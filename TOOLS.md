# PyMake Tools  

## FixSource  

*In China field, other country PyPi is slowly, so you will need change PyPi to domestic field.*  
*It can change PyPi to tsinghua tuna, it is a system setting.*  

## FixDependent  

*This is a tool to add package for PyMake programs.*  
*PyEdit need PyQt5, so FixDependent will install it.*  
*Window program need install third-party package PyQt5.*   
*Now, FixDependent install PyQt5 only.*  

## FixPip  

*FixPip do a fix job for fixing pip for a python instance.*  
*It is using PyPi for downloading pip package.*  
*It can select pip version using pip=={pip-version} in command-line.*  
*It is a mod from get-pip.py, remove pkgutil, add local pip certifi file cacert.pem, support macOS/Windows/Ubuntu, and so on.*  
*Defaut, FixPip will install pip 19.1.1, latest wheel, latest setuptools.*  
*Maybe, when you upgrade pip, it crashed, oh no, it is a hard thing on Ubuntu.*   
*Maybe, you need root authority on \*Unix system.*  

## PyEdit  

*This is a tools helping user to edit your environment files.*  
*It provide system environ showing, local environ showing, custom environ editing, separate environ editing,*
*command editing, path-assemblage editing, user env source changing, exec root changing, and readme.*  
*That is a full-functional editing tool for user source files.*  

#### Screen Shotcut  

![PyEdit](./screenshot/pyedit.png)  


## PyDependent  

*This a tool for installing dependent packages for ensure Python, using ensure PyPi.*  
*User can use this tool only on window, wont saving anything.*  
*User can save his Python and PyPi settings to UserSource Directory, but wont effect on system setting, next startup, saved settings can effect on window.*  
*User can save settings for current user on system, other Python instance will also using these setting.*  

#### Screen Shotcut  

![PyDependent](./screenshot/pydep/c1.png)  
![PyDependent](./screenshot/pydep/c3.png)  
![PyDependent](./screenshot/pydep/c4.png)  
![PyDependent](./screenshot/pydep/c5.png)  
![PyDependent](./screenshot/pydep/c6.png)  
![PyDependent](./screenshot/pydep/c8.png)  
![PyDependent](./screenshot/pydep/c10.png)  

## filetransporter  

*This a third-party tool for translating file and folder in lan field network.*  
*This tool use http protocol to translate files and folders.*  
*Only one process can be executing when thanslate job starting.*  

