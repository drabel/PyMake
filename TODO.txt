√ 1. pyvcenv.bat
  2. pymake type 命令支持type外部脚本。
      用pymake see 命令？
√ 3. vc env内部separate env应该放在最后添加。
  4. vc 命令使用pymakevcdict，不应该直接读取pymake-vc-command.json。
  5. vcvarsall，vcvarsallparam，全都是小写的问题。
  6. env的路径查找顺序是从下往上的，能正常使用，要不要改？
  7. python3.10多处功能测试有问题。
  8. pymake-gui... 当中全部使用弹出命令行窗口。
  9. 检查对环境目录空格的支持情况。
  10. Windows下，把默认环境目录迁移到 Documents/My Environment
  11. 把PyMake安装为Python模块，支持Install快捷方式，支持把快捷方式路径加入环境路径。
        PyMake提供独立的安装包？
  