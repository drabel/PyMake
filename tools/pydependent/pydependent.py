# -*- coding: utf-8 -*-
# !/usr/bin/env python

from __future__ import unicode_literals

import os
import re
import sys
import uuid
import shutil
import time
import json
import copy
import types

# ===============================================================================================
# NOTICE:
# __file__ only can be used at startup position! [before os.chdir()]
# behind os.chdir(), __file__ relative to os.getcwd(), it maybe an error value!
# Dont use it later!
#
# THAT:
# __file__ maybe a relative path to os.getcwd(), it is its defination from python.
# It is a const value in its life term.
# Only at startup position, user can get its correct abs path.
# behind os.chdir(), user cannot get its correct abs path.
# Dont use it later!
# --------------------

# ===============================================================================================
# relative to project root. [relative to pymake.py]
# --------------------
pymakerelativepath = '../..'

# ===============================================================================================
# load module helper
# --------------------

def load_rpath_for_local(path=''):
    # add rpath. [HEADER ROOT] [LIBRARY ROOT] [MODULE ROOT]
    # find module when execute.
    # 1, realpath, fake_file
    # 2, __file__
    # 3, sys.argv[0]
    basicpath = os.path.split(os.path.realpath(__file__))[0]
    paths = []
    if (isinstance(path, str)):
        paths.append(path)
    if (isinstance(path, list)):
        paths.extend(path)
    syspath1 = []
    for key in sys.path:
        temppath = key.replace('\\', '/')
        syspath1.append(temppath)
    for path in paths:
        module_path = os.path.join(basicpath, path)
        module_path = os.path.realpath(module_path)
        temppath = module_path.replace('\\', '/')
        if(syspath1.__contains__(temppath)):
            continue
        sys.path.insert(0, module_path)
    return

load_rpath_for_local(pymakerelativepath)
# print(sys.path)

# ===============================================================================================
# import module
# --------------------
# common
from pycore.pycore import *

# ===============================================================================================
# PYMAKE INITIAL CODE
# --------------------
def main_function():
    d = {
        "tips": {
            "variable.tips": [
                "QQt",
                "LibQQt",
                "${root.build}/${qqt.proj.name}/${qt.sys.mac}/${build.release}",
                "${root.src}/${qqt.proj.name}",
                "${qqt.proj.name}.pro",
                "${qqt.build.path}/src/bin",
                "${root.prod}/QQt",
                "qqtframe",
                "${qqt.build.path}/examples/${qqtframe.prod.name}/bin",
                "${qqt.build.path}/src/bin/QQt.framework",
                "QQt.framework/Versions/1/QQt",
                "${root.tool}/Source/qt5",
                "${root.build}/qt5",
                "androiddeployqt",
                "macdeployqt",
                "DownloadQueue",
                "/Users/abel/Develop/c1-webrc/DownloadQueue/DownloadQueue.pro",
                "${root.build}/${app.name}",
                "macdeployqt ${app.path.build}/${app.name}.app",
                "${prod.name}.app",
                "${mac.app}/Contents",
                "${mac.app.content}/MacOS",
                "${mac.app.content}/Frameworks",
                "${mac.app.content}/Resources",
                "${mac.app.content}/PlugIns",
                "${prod.name}.framework",
                "${prod.name}.framework/Versions",
                "${prod.name}.framework/Resources",
                "macdeployqt ${}"
            ],
            "command.tips": [
                "I'm not similar to these command, so list them here, rather than forgotten them",
                "cl-command, sys-command",
                "replace? no, append? easy!",
                "help you to remeber these command.",
                "mkdir -p ${qqt.build.path}",
                "cd ${build-path}",
                "cmake -G\"Unix Makefiles\" -DCMAKE_INSTALL_PREFIX=${prod-root} ${source-path}",
                "cmake -GXCode -DCMAKE_INSTALL_PREFIX=${prod-root} ${source-path}",
                "rm -f CMakeCache.txt",
                "qmake ${source-path}/${qmake-file} -spec $QTSPEC CONFIG+=$QTCONFIG && make qmake_all",
                "make -j4",
                "make clean in ${build-path}",
                "make install",
                "${deployqt} ${bin-path}/${app-bundle} -verbose=1",
                "${deployqt} -dmg",
                "${deployqt} --help",
                "cp -fr ${lib-dep} ${lib-native}",
                "install_name_tool -change ${lib-dep-name} @rpath/${lib-dep-name} ${app-native}/${prod-name} ",
                "install_name_tool -change $LibDep @rpath/$LibDep ${app-native}/${prod-name} ",
                "${source-path}/configure -prefix ${install-path} -hostprefix ${install-path} -xplatform android-g++ -release -nomake tests -nomake examples -android-ndk $ANDROID_NDK_ROOT -android-sdk $ANDROID_SDK_ROOT -android-ndk-host $ANDROID_NDK_HOST -android-toolchain-version $ANDROID_NDK_TOOLCHAIN_VERSION -skip qtwebkit-examples -no-warnings-are-errors",
                "${qt5.path.source}/configure -prefix ${qt5.path.install} -hostprefix ${qt5.path.install} -xplatform android-g++ -release -nomake tests -nomake examples -android-ndk $ANDROID_NDK_ROOT -android-sdk $ANDROID_SDK_ROOT -android-ndk-host $ANDROID_NDK_HOST -android-toolchain-version $ANDROID_NDK_TOOLCHAIN_VERSION -skip qtwebkit-examples -no-warnings-are-errors"
            ],
        },
        "path-assemblage": {
            "root": "/Users/abel/Develop",
            "root.src": "${root}/a0-develop",
            "root.prod": "${root}/d0-product",
            "root.sdk": "${root}/d1-sdks",
            "root.tool": "${root}/b0-toolskits",
            "root.build": "${root}/c0-buildstation",
            "root.test": "${root}/f0-test",
            "root.webrc": "${root}/e0-webrc",
            "cc": "${root.tool}/a0-compiler",
            "pymake": "${cc}/PyMake",
            "wincc": "${root.tool}/macCompilers",
            "cmake.bin": "${wincc}/CMake.app/Contents/bin",
            "qt": "${root.tool}/macLibraries/Qt",
            "qt4.version": "4.8.6",
            "qt4.clang": "${qt}/${qt4.version}/clang_64",
            "qt4.clang.bin": "${qt4.clang}/bin",
            "qt5.version": "5.9.2",
            "qt5.ios": "${qt}/${qt5.version}/ios",
            "qt5.ios.bin": "${qt}/${qt5.version}/ios/bin",
            "qt5.clang": "${qt}/${qt5.version}/clang_64",
            "qt5.clang.bin": "${qt}/${qt5.version}/clang_64/bin",
            "qt5.android_arm": "${qt}/${qt5.version}/android_armv7",
            "qt5.android_arm.bin": "${qt}/${qt5.version}/android_armv7/bin",
            "qt5.android_x86": "${qt}/${qt5.version}/android_x86",
            "qt5.android_x86.bin": "${qt}/${qt5.version}/android_x86/bin",
            "android": "${root.tool}/macAndroidLibraries",
            "android.sdk": "${android}/android-sdk-macosx",
            "android.ndk": "${android}/android-ndk-r13b",
            "android.ant": "${android}/apache-ant-1.10.1",
            "android.java": "${android}/java-macosx/Java/JavaVirtualMachines",
            "sdk.plat.tool": "${android.sdk}/platform-tools",
            "sdk.build.tool": "${android.sdk}/build-tools",
            "sdk.tool": "${android.sdk}/tools",
            "java1.7.home": "${android.java}/jdk1.7.0_79.jdk/Contents/Home",
            "java1.8.home": "${android.java}/jdk1.8.0_111.jdk/Contents/Home",
            "java1.9.home": "${android.java}/jdk9.jdk/Contents/Home",
            "java1.7.bin": "${java1.7.home}/bin",
            "java1.8.bin": "${java1.8.home}/bin",
            "java1.9.bin": "${java1.9.home}/bin",
            "ant.bin": "${android.ant}/bin",
            "ndk.arm": "${android.ndk}/toolchains/arm-linux-androideabi-4.9/prebuilt/darwin-x86_64",
            "ndk.x86": "${android.ndk}/toolchains/x86-4.9/prebuilt/darwin-x86_64",
            "ndk.x86_64": "${android.ndk}/toolchains/x86_64-4.9/prebuilt/darwin-x86_64",
            "ndk.arm.bin": "${ndk.arm}/bin",
            "ndk.x86.bin": "${ndk.x86}/bin",
            "ndk.x86_64.bin": "${ndk.x86_64}/bin",
            "ios.simulator.sysroot": "/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator.sdk",
            "xcode.bin": "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin",
            "mac.sysroot": "/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk"
        },
        "environ": {
            "android.mobile": {
                "path+": [
                    "${qt5.android_arm.bin}",
                    "${java1.8.bin}",
                    "${android.sdk}",
                    "${sdk.plat.tool}",
                    "${sdk.build.tool}",
                    "${sdk.tool}",
                    "${ant.bin}",
                    "${ndk.arm.bin}"
                ],
                "makex": "make",
                "CMAKEGENERATOR": "Unix Makefiles",
                "ANDROID_API_VERSION": "android-23",
                "ANDROID_SDK_ROOT": "${android.sdk}",
                "ANDROID_NDK_ROOT": "${android.ndk}",
                "ANDROID_NDK_HOST": "darwin-x86_64",
                "ANDROID_NDK_TOOLCHAIN_PREFIX": "arm-linux-androideabi",
                "ANDROID_NDK_TOOLCHAIN_VERSION": "4.9",
                "ANDROID_NDK_PLATFORM": "android-23",
                "NDK_TOOLCHAIN_PATH": "${ndk.arm.bin}",
                "NDK_TOOLS_PREFIX": "arm-linux-androideabi",
                "QTVERSION": "${qt5.version}",
                "QTDIR": "${qt5.android_arm}",
                "QTSPEC": "-spec android-g++",
                "QTCONFIG": "",
                "JAVA_HOME": "${java1.8.home}",
                "CLASSPATH": ".:${JAVA_HOME}/lib/dt.jar:${JAVA_HOME}/lib/tools.jar",
                "PYMAKE_MYNAME": "T.D.R",
                "a_special_var_const": "hello world",
                "QKIT": "ANDROID",
                "QSYS": "Android"
            },
            "android.x86": {
                "path+": [
                    "${qt5.android_x86.bin}",
                    "${java1.8.bin}",
                    "${android.sdk}",
                    "${sdk.plat.tool}",
                    "${sdk.build.tool}",
                    "${sdk.tool}",
                    "${ant.bin}",
                    "${ndk.x86.bin}"
                ],
                "makex": "make",
                "CMAKEGENERATOR": "Unix Makefiles",
                "CLICOLOR": "1",
                "ANDROID_API_VERSION": "android-23",
                "ANDROID_HOME": "${android.sdk}",
                "ANDROID_SDK_ROOT": "${android.sdk}",
                "ANDROID_NDK_PLATFORM": "android-23",
                "ANDROID_NDK_ROOT": "${android.ndk}",
                "ANDROID_NDK_HOST": "darwin-x86_64",
                "ANDROID_NDK_TOOLCHAIN_PREFIX": "x86",
                "ANDROID_NDK_TOOLCHAIN_VERSION": "4.9",
                "ANDROID_NDK_TOOLS_PREFIX": "i686-linux-android",
                "NDK_TOOLCHAIN_PATH": "${ndk.x86.bin}",
                "NDK_TOOLS_PREFIX": "i686-linux-android",
                "PYMAKE_MYNAME": "T.D.R",
                "a_special_var_const": "hello world",
                "QTDIR": "${qt5.android_x86}",
                "QTSPEC": "-spec android-g++",
                "QTCONFIG": "",
                "JAVA_HOME": "${java1.8.home}",
                "CLASSPATH": ".:${JAVA_HOME}/lib/dt.jar:${JAVA_HOME}/lib/tools.jar",
                "QTVERSION": "${qt5.version}",
                "QKIT": "ANDROIDX86",
                "QSYS": "AndroidX86"
            },
            "qt4": {
                "path+": [
                    "${qt4.clang.bin}"
                ],
                "makex": "make",
                "CMAKEGENERATOR": "Unix Makefiles",
                "QTVERSION": "${qt4.version}",
                "QTDIR": "${qt4.clang}",
                "QTSPEC": "-spec macx-llvm",
                "QTCONFIG": "CONFIG+=x86_64",
                "QKIT": "macOS",
                "QSYS": "macOS"
            },
            "ios": {
                "path+": [
                    "${qt5.ios.bin}"
                ],
                "makex": "make",
                "CMAKEGENERATOR": "Unix Makefiles",
                "QTVERSION": "${qt5.version}",
                "QTDIR": "${qt5.ios}",
                "QTSPEC": "-spec macx-ios-clang",
                "QTCONFIG": "CONFIG+=iphoneos CONFIG+=device -after QMAKE_MAC_XCODE_SETTINGS+=qteam qteam.name=DEVELOPMENT_TEAM qteam.value=4EGMLT3G6T",
                "QKIT": "iOS",
                "QSYS": "iOS"
            },
            "iossimulator": {
                "path+": [
                    "${qt5.ios.bin}"
                ],
                "makex": "make",
                "CMAKEGENERATOR": "Unix Makefiles",
                "QTVERSION": "${qt5.version}",
                "QTDIR": "${qt5.ios}",
                "QTSPEC": "-spec macx-ios-clang",
                "QTCONFIG": "CONFIG+=iphonesimulator CONFIG+=simulator",
                "QKIT": "iOSSimulator",
                "QSYS": "iOSSimulator"
            },
            "macos": {
                "path+": [
                    "${qt5.clang.bin}"
                ],
                "makex": "make",
                "CMAKEGENERATOR": "Unix Makefiles",
                "QTVERSION": "${qt5.version}",
                "QTDIR": "${qt5.clang}",
                "QTSPEC": "-spec macx-clang",
                "QTCONFIG": "CONFIG+=x86_64",
                "QKIT": "macOS",
                "QSYS": "macOS"
            },
            "current": "macos"
        },
        "command": {
            "test": [
                "echo $(pwd)"
            ],
            "test.2": [
                "#echo $*",
                "param=$*",
                "ping 127.0.0.1 $param"
            ],
            "test.3": [
                "#echo $*",
                "ping $*"
            ],
            "test.4": [
                "echo param1: $1",
                "echo param2: $2",
                "echo param3: $3",
                "echo param4: $4"
            ],
            "test.5": [
                "exit 0"
            ],
            "test.6": [
                "echo 中文"
            ],
            "test.7": [
                "echo $PATH"
            ],
            "open.env": [
                "open $(pymake source)",
                "open $(pymake source root)/custom.path+.ini",
                "open $(pymake source root)/custom.var+.ini"
            ],
            "qt": [
                "open \"/Applications/Qt Creator.app\""
            ],
            "cmake-app": [
                "open ${wincc}/CMake.app"
            ],
            "prod": [
                "open /Users/abel/Develop/d0-product/ProductExecTool/macOS/ProductExecTool_debug.app"
            ],
            "libtool": [
                "open /Users/abel/Develop/d0-product/AddLibraryTool/macOS/AddLibraryTool_debug.app"
            ],
            "android.sdk": [
                "/Users/abel/Develop/b0-toolskits/macAndroidLibraries/android-sdk-macosx/tools/android"
            ],
            "open-dir": [
                "open $1"
            ],
            "qtdir": [
                "echo $QTDIR"
            ],
            "build.info": [
                "while [ 1 ]",
                "do",
                "src_path=$(pwd)",
                "profilename=$1",
                "if [ \"$profilename\" == \"\" ]; then",
                "   echo please input a project name",
                "   break",
                "fi",
                "if [ -f \"$1.pro\" ]; then",
                "   echo $1.pro existed.",
                "else",
                "   echo has $1.pro? please add here command to restrict.",
                "   break",
                "fi",
                "src=$src_path/$profilename.pro",
                "build=${root.build}/$profilename/$QSYS/$QTVERSION/Debug",
                "prod=${root.prod}/$profilename/$QSYS",
                "sdk=${root.sdk}/$profilename/$QSYS",
                "echo src file: $src",
                "echo src path: $src_path",
                "echo sdk   at: $sdk",
                "echo prod  at: $prod",
                "echo build at: $build",
                "echo build inf $QTSPEC $QTCONFIG",
                "endtime=`date +'%Y-%m-%d %H:%M:%S'`",
                "echo 现在时间：$endtime",
                "break",
                "done"
            ],
            "build.qmake": [
                "while [ 1 ]",
                "do",
                "src_path=$(pwd)",
                "profilename=$1",
                "if [ \"$profilename\" == \"\" ]; then",
                "   echo please input a project name",
                "   break",
                "fi",
                "if [ -f \"$1.pro\" ]; then",
                "   echo $1.pro existed.",
                "else",
                "   echo has $1.pro? please add here command to restrict.",
                "   break",
                "fi",
                "src=$src_path/$profilename.pro",
                "build=${root.build}/$profilename/$QSYS/$QTVERSION/Debug",
                "prod=${root.prod}/$profilename/$QSYS",
                "sdk=${root.sdk}/$profilename/$QSYS",
                "mkdir -p $build",
                "cd $build",
                "echo src file: $src",
                "echo src path: $src_path",
                "echo sdk   at: $sdk",
                "echo prod  at: $prod",
                "echo build at: $build",
                "echo $QTSPEC $QTCONFIG",
                "#starttime=`date +'%Y-%m-%d %H:%M:%S'`",
                "qmake $src $QTSPEC CONFIG+=debug CONFIG+=qml_debug $QTCONFIG && make qmake_all",
                "make -j4",
                "echo build inf $QTSPEC $QTCONFIG",
                "echo src file: $src",
                "echo src path: $src_path",
                "echo build at: $build",
                "echo sdk   at: $sdk",
                "echo prod  at: $prod",
                "#endtime=`date +'%Y-%m-%d %H:%M:%S'`",
                "#start_seconds=$(date --date=\"$starttime\" +%s);",
                "#end_seconds=$(date --date=\"$endtime\" +%s);",
                "#echo \"本次运行时间： \"$((end_seconds-start_seconds))\"s\"",
                "break",
                "done"
            ],
            "build.qmake.all": [
                "while [ 1 ]",
                "do",
                "src_path=$(pwd)",
                "profilename=$1",
                "if [ \"$profilename\" == \"\" ]; then",
                "   echo please input a project name",
                "   break",
                "fi",
                "if [ -f \"$1.pro\" ]; then",
                "   echo $1.pro existed.",
                "else",
                "   echo has $1.pro? please add here command to restrict.",
                "   break",
                "fi",
                "echo You must use pymake system ccvp here command to execute this command!",
                "envlist=(\"macos\" \"iossimulator\" \"android.mobile\" \"android.x86\")",
                "length=${#envlist[@]}",
                "cmd0=${envlist[0]}",
                "#for envname in $envlist 这个是个bug",
                "for envname in ${envlist[@]}",
                "do",
                "   #echo $envname",
                "   #echo pymake use $envname ccvp here build.qmake $profilename",
                "   #export aiya=$envname",
                "   #echo $aiya",
                "   #osascript -e \"",
                "   #    set a to \\\"$aiya\\\"",
                "   #    tell application \\\"Terminal\\\" to do script \\\"echo - $a - \\\" & a & - \\\"$aiya\\\"",
                "   #\"",
                "   #osascript",
                "   #set a to POSIX file \"$envname\"",
                "   #env",
                "   #osascript -e 'tell application \"Terminal\" to do script \"echo $aiya\"'",
                "   osascript -e \"",
                "       tell application \\\"Terminal\\\" to do script \\\"cd $src_path && echo $profilename - $envname && pymake use $envname ccvp here build.qmake $profilename\\\"",
                "   \"",
                "done",
                "break",
                "done"
            ],
            "build.cmake.gui": [
                "while [ 1 ]",
                "do",
                "src_path=$(pwd)",
                "profilename=$1",
                "if [ \"$profilename\" == \"\" ]; then",
                "   echo please input a project name",
                "   break",
                "fi",
                "if [ -f \"CMakeLists.txt\" ]; then",
                "   echo CMakeLists.txt existed.",
                "else",
                "   echo has CMakeLists.txt? please add here command to restrict.",
                "   break",
                "fi",
                "src=$src_path/CMakeLists.txt",
                "build=${root.build}/$profilename/$QSYS/$QTVERSION/Debug",
                "prod=${root.prod}/$profilename/$QSYS",
                "sdk=${root.sdk}/$profilename/$QSYS",
                "mkdir -p $build",
                "cd $build",
                "echo src file: $src",
                "echo src path: $src_path",
                "echo sdk   at: $sdk",
                "echo prod  at: $prod",
                "echo build at: $build",
                "echo $QTSPEC $QTCONFIG",
                "open ${wincc}/CMake.app",
                "errorlevel=$?",
                "if [ $errorlevel -ne 0 ]; then",
                "   echo cmake exit code: $errorlevel",
                "   break",
                "fi",
                "echo build inf $QTSPEC $QTCONFIG",
                "echo src file: $src",
                "echo src path: $src_path",
                "echo build at: $build",
                "echo sdk   at: $sdk",
                "echo prod  at: $prod",
                "break",
                "done"
            ],
            "build.cmake.sdk": [
                "while [ 1 ]",
                "do",
                "src_path=$(pwd)",
                "profilename=$1",
                "if [ \"$profilename\" == \"\" ]; then",
                "   echo please input a project name",
                "   break",
                "fi",
                "if [ -f \"CMakeLists.txt\" ]; then",
                "   echo CMakeLists.txt existed.",
                "else",
                "   echo has CMakeLists.txt? please add here command to restrict.",
                "   break",
                "fi",
                "src=$src_path/CMakeLists.txt",
                "build=${root.build}/$profilename/$QSYS/$QTVERSION/Debug",
                "prod=${root.prod}/$profilename/$QSYS",
                "sdk=${root.sdk}/$profilename/$QSYS",
                "mkdir -p $build",
                "cd $build",
                "echo src file: $src",
                "echo src path: $src_path",
                "echo sdk   at: $sdk",
                "echo prod  at: $prod",
                "echo build at: $build",
                "echo $QTSPEC $QTCONFIG",
                "cmake $src_path -G\"$CMAKEGENERATOR\" -DCMAKE_INSTALL_PREFIX=${root.sdk}/$profilename/$QSYS -DCMAKE_BUILD_TYPE=Debug",
                "errorlevel=$?",
                "if [ $errorlevel -ne 0 ]; then",
                "   echo cmake exit code: $errorlevel",
                "   break",
                "fi",
                "echo build inf $QTSPEC $QTCONFIG",
                "echo src file: $src",
                "echo src path: $src_path",
                "echo build at: $build",
                "echo sdk   at: $sdk",
                "echo prod  at: $prod",
                "break",
                "done"
            ],
            "build.cmake.app": [
                "while [ 1 ]",
                "do",
                "src_path=$(pwd)",
                "profilename=$1",
                "if [ \"$profilename\" == \"\" ]; then",
                "   echo please input a project name",
                "   break",
                "fi",
                "if [ -f \"CMakeLists.txt\" ]; then",
                "   echo CMakeLists.txt existed.",
                "else",
                "   echo has CMakeLists.txt? please add here command to restrict.",
                "   break",
                "fi",
                "src=$src_path/CMakeLists.txt",
                "build=${root.build}/$profilename/$QSYS/$QTVERSION/Debug",
                "prod=${root.prod}/$profilename/$QSYS",
                "sdk=${root.sdk}/$profilename/$QSYS",
                "mkdir -p $build",
                "cd $build",
                "echo src file: $src",
                "echo src path: $src_path",
                "echo sdk   at: $sdk",
                "echo prod  at: $prod",
                "echo build at: $build",
                "echo $QTSPEC $QTCONFIG",
                "cmake $src_path -G\"$CMAKEGENERATOR\" -DCMAKE_INSTALL_PREFIX=${root.prod}/$profilename/$QSYS -DCMAKE_BUILD_TYPE=Debug",
                "errorlevel=$?",
                "if [ $errorlevel -ne 0 ]; then",
                "   echo cmake exit code: $errorlevel",
                "   break",
                "fi",
                "echo build inf $QTSPEC $QTCONFIG",
                "echo src file: $src",
                "echo src path: $src_path",
                "echo build at: $build",
                "echo sdk   at: $sdk",
                "echo prod  at: $prod",
                "break",
                "done"
            ],
            "build.make": [
                "while [ 1 ]",
                "do",
                "src_path=$(pwd)",
                "profilename=$1",
                "if [ \"$profilename\" == \"\" ]; then",
                "   echo please input a project name",
                "   break",
                "fi",
                "src=$src_path/$profilename.pro",
                "build=${root.build}/$profilename/$QSYS/$QTVERSION/Debug",
                "prod=${root.prod}/$profilename/$QSYS",
                "sdk=${root.sdk}/$profilename/$QSYS",
                "#mkdir -p $build",
                "cd $build",
                "echo src file: $src",
                "echo src path: $src_path",
                "echo sdk   at: $sdk",
                "echo prod  at: $prod",
                "echo build at: $build",
                "echo $QTSPEC $QTCONFIG",
                "#qmake $src $QTSPEC CONFIG+=debug CONFIG+=qml_debug $QTCONFIG && $makex qmake_all",
                "$makex",
                "echo build inf $QTSPEC $QTCONFIG",
                "echo src file: $src",
                "echo src path: $src_path",
                "echo build at: $build",
                "echo sdk   at: $sdk",
                "echo prod  at: $prod",
                "break",
                "done"
            ],
            "build.make.install": [
                "while [ 1 ]",
                "do",
                "src_path=$(pwd)",
                "profilename=$1",
                "if [ \"$profilename\" == \"\" ]; then",
                "   echo please input a project name",
                "   break",
                "fi",
                "src=$src_path/$profilename.pro",
                "build=${root.build}/$profilename/$QSYS/$QTVERSION/Debug",
                "prod=${root.prod}/$profilename/$QSYS",
                "sdk=${root.sdk}/$profilename/$QSYS",
                "#mkdir -p $build",
                "cd $build",
                "echo src file: $src",
                "echo src path: $src_path",
                "echo sdk   at: $sdk",
                "echo prod  at: $prod",
                "echo build at: $build",
                "echo $QTSPEC $QTCONFIG",
                "#qmake $src $QTSPEC CONFIG+=debug CONFIG+=qml_debug $QTCONFIG && $makex qmake_all",
                "$makex install",
                "echo build inf $QTSPEC $QTCONFIG",
                "echo src file: $src",
                "echo src path: $src_path",
                "echo build at: $build",
                "echo sdk   at: $sdk",
                "echo prod  at: $prod",
                "break",
                "done"
            ],
            "build.make.clean": [
                "while [ 1 ]",
                "do",
                "src_path=$(pwd)",
                "profilename=$1",
                "if [ \"$profilename\" == \"\" ]; then",
                "   echo please input a project name",
                "   break",
                "fi",
                "src=$src_path/$profilename.pro",
                "build=${root.build}/$profilename/$QSYS/$QTVERSION/Debug",
                "prod=${root.prod}/$profilename/$QSYS",
                "sdk=${root.sdk}/$profilename/$QSYS",
                "#mkdir -p $build",
                "cd $build",
                "echo src file: $src",
                "echo src path: $src_path",
                "echo sdk   at: $sdk",
                "echo prod  at: $prod",
                "echo build at: $build",
                "echo $QTSPEC $QTCONFIG",
                "#qmake $src $QTSPEC CONFIG+=debug CONFIG+=qml_debug $QTCONFIG && $makex qmake_all",
                "$makex clean",
                "echo build inf $QTSPEC $QTCONFIG",
                "echo src file: $src",
                "echo src path: $src_path",
                "echo build at: $build",
                "echo sdk   at: $sdk",
                "echo prod  at: $prod",
                "break",
                "done"
            ],
            "build.go": [
                "while [ 1 ]",
                "do",
                "src_path=$(pwd)",
                "profilename=$1",
                "if [ \"$profilename\" == \"\" ]; then",
                "   echo please input a project name",
                "   break",
                "fi",
                "if [ -f \"$1.go\" ]; then",
                "   echo $1.go existed.",
                "else",
                "   echo has $1.go? please add here command to restrict.",
                "   break",
                "fi",
                "src=$src_path/$profilename.go",
                "build=${root.build}/$profilename/$QSYS/$QTVERSION/Debug",
                "prod=${root.prod}/$profilename/$QSYS",
                "sdk=${root.sdk}/$profilename/$QSYS",
                "mkdir -p $build",
                "cd $build",
                "echo src file: $src",
                "echo src path: $src_path",
                "echo sdk   at: $sdk",
                "echo prod  at: $prod",
                "echo build at: $build",
                "echo $QTSPEC $QTCONFIG",
                "#qmake $src $QTSPEC CONFIG+=debug CONFIG+=qml_debug $QTCONFIG && make qmake_all",
                "#make -j4",
                "go build $src",
                "echo build inf $QTSPEC $QTCONFIG",
                "echo src file: $src",
                "echo src path: $src_path",
                "echo build at: $build",
                "echo sdk   at: $sdk",
                "echo prod  at: $prod",
                "break",
                "done"
            ],
            "build.configure": [
                "while [ 1 ]",
                "do",
                "src_path=$(pwd)",
                "profilename=$1",
                "if [ \"$profilename\" == \"\" ]; then",
                "   echo please input a project name",
                "   break",
                "fi",
                "if [ -f \"configure\" ]; then",
                "   echo configure existed.",
                "else",
                "   echo has configure? please add here command to restrict.",
                "   break",
                "fi",
                "src=$src_path/configure",
                "build=${root.build}/$profilename/$QSYS/$QTVERSION/Debug",
                "prod=${root.prod}/$profilename/$QSYS",
                "sdk=${root.sdk}/$profilename/$QSYS",
                "mkdir -p $build",
                "cd $build",
                "echo src file: $src",
                "echo src path: $src_path",
                "echo sdk   at: $sdk",
                "echo prod  at: $prod",
                "echo build at: $build",
                "echo $QTSPEC $QTCONFIG",
                "#qmake $src $QTSPEC CONFIG+=debug CONFIG+=qml_debug $QTCONFIG && make qmake_all",
                "#make -j4",
                "configure \"$@\"",
                "echo build inf $QTSPEC $QTCONFIG",
                "echo src file: $src",
                "echo src path: $src_path",
                "echo build at: $build",
                "echo sdk   at: $sdk",
                "echo prod  at: $prod",
                "break",
                "done"
            ],
            "build.autogen": [
                "while [ 1 ]",
                "do",
                "src_path=$(pwd)",
                "profilename=$1",
                "if [ \"$profilename\" == \"\" ]; then",
                "   echo please input a project name",
                "   break",
                "fi",
                "if [ -f \"autogen.sh\" ]; then",
                "   echo autogen.sh existed.",
                "else",
                "   echo has autogen.sh? please add here command to restrict.",
                "   break",
                "fi",
                "src=$src_path/autogen.sh",
                "build=${root.build}/$profilename/$QSYS/$QTVERSION/Debug",
                "prod=${root.prod}/$profilename/$QSYS",
                "sdk=${root.sdk}/$profilename/$QSYS",
                "mkdir -p $build",
                "cd $build",
                "echo src file: $src",
                "echo src path: $src_path",
                "echo sdk   at: $sdk",
                "echo prod  at: $prod",
                "echo build at: $build",
                "echo $QTSPEC $QTCONFIG",
                "#qmake $src $QTSPEC CONFIG+=debug CONFIG+=qml_debug $QTCONFIG && make qmake_all",
                "#make -j4",
                "autogen.sh \"$@\"",
                "echo build inf $QTSPEC $QTCONFIG",
                "echo src file: $src",
                "echo src path: $src_path",
                "echo build at: $build",
                "echo sdk   at: $sdk",
                "echo prod  at: $prod",
                "break",
                "done"
            ],
            "Qt.move": [
                "while [ 1 ]",
                "do",
                "srcdir=$1",
                "srcver=$2",
                "if [ \"$1\" = \"\" ]; then",
                "   echo please input old qtdir.",
                "   break",
                "fi",
                "if [ \"$2\" = \"\" ]; then",
                "   echo please input old qtversion.",
                "   break",
                "fi",
                "mv -f $srcdir/$srcver/* Qt2/$srcver/",
                "mv -f $srcdir/Docs/* Qt2/Docs/",
                "mv -f $srcdir/Examples/* Qt2/Examples/",
                "mv -f $srcdir/*.zip Qt2/",
                "break",
                "done"
            ],
            "qt4.build": [
                "src=${root.tool}/z0-Source/qt",
                "build=${root.build}/qt",
                "install=${root.tool}/macLibraries/Qt/4.8/gcc_64",
                "mkdir -p $build",
                "cd $build",
                "echo build $(pwd)",
                "CXXFLAGS=-stdlib=libc++",
                "${src}/configure -prefix ${install}",
                "make -j4",
                "make install"
            ],
            "qtsoap.build": [
                "src=/Users/abel/Develop/c1-webrc/qt-solutions/qtsoap",
                "build=${root.build}/qtsoap",
                "install=/Users/abel/Develop/d1-product/QtSoap",
                "cd $build",
                "${src}/configure -library"
            ],
            "qqt.build.all": [
                "osascript -e 'tell application \"Terminal\" to do script \"echo hello\"'"
            ],
            "qqt.daily": [
                "src_path=/Users/abel/Develop/a0-develop/LibQQt",
                "src=$src_path/QQt.pro",
                "build=/Users/abel/Develop/c0-buildstation/QQt/$QSYS/$QTVERSION/Debug",
                "mkdir -p $build",
                "cd $build",
                "rm -rf src examples",
                "qmake $src $QTSPEC $QTCONFIG CONFIG+=debug CONFIG+=qml_debug && make qmake_all",
                "make -j4"
            ],
            "qqt.daily.release": [
                "src_path=/Users/abel/Develop/a0-develop/LibQQt",
                "src=$src_path/QQt.pro",
                "build=/Users/abel/Develop/c0-buildstation/QQt/$QSYS/$QTVERSION/Release",
                "mkdir -p $build",
                "cd $build",
                "rm -rf src examples",
                "qmake $src $QTSPEC $QTCONFIG CONFIG+=release && make qmake_all",
                "make -j4"
            ],
            "qqt.build.release": [
                "src_path=/Users/abel/Develop/a0-develop/LibQQt",
                "src=$src_path/QQt.pro",
                "build=/Users/abel/Develop/c0-buildstation/QQt/$QSYS/$QTVERSION/Release",
                "mkdir -p $build",
                "cd $build",
                "qmake $src $QTSPEC $QTCONFIG CONFIG+=release && make qmake_all",
                "make -j4"
            ],
            "qqt.build": [
                "src_path=${root.src}/LibQQt",
                "src=${root.src}/LibQQt/QQt.pro",
                "build=${root.build}/QQt/$QSYS/$QTVERSION/Debug",
                "mkdir -p $build",
                "cd $build",
                "echo build $(pwd)",
                "qmake ${src} $QTSPEC CONFIG+=debug CONFIG+=qml_debug $QTCONFIG && make qmake_all",
                "make -j4"
            ],
            "qqt.clean": [
                "src=${root.src}/LibQQt/QQt.pro",
                "build=${root.build}/QQt/$QSYS/$QTVERSION/Debug",
                "cd $build",
                "make clean"
            ],
            "qqt.clean.release": [
                "src=/Users/abel/Develop/a0-develop/LibQQt/QQt.pro",
                "build=/Users/abel/Develop/c0-buildstation/QQt/$QSYS/$QTVERSION/Release",
                "cd $build",
                "make clean"
            ],
            "qt.check": [
                "src=${root.tool}/z0-Source/qt-everywhere-opensource-src-4.8.7",
                "build=${root.build}/qt4.7",
                "install=${root.tool}/macLibraries/Qt/4.8.7/gcc_64",
                "mkdir -p $build",
                "cd $build",
                "echo build $(pwd)",
                "${src}/configure --help"
            ],
            "qt.build": [
                "src=/Users/abel/Develop/b0-toolskits/z0-Source/qt",
                "build=/Users/abel/Develop/c0-buildstation/qt",
                "install=/Users/abel/Develop/b0-toolskits/macLibraries/Qt/4.8/gcc_64",
                "mkdir -p $build",
                "cd $build",
                "echo build $(pwd)",
                "CXXFLAGS=-stdlib=libc++",
                "${src}/configure -prefix ${install}",
                "make -j4",
                "make install"
            ],
            "qt4.7.build": [
                "src=${root.tool}/z0-Source/qt-everywhere-opensource-src-4.8.7",
                "build=${root.build}/qt4.7",
                "install=${root.tool}/macLibraries/Qt/4.8.7/gcc_64",
                "mkdir -p $build",
                "cd $build",
                "echo build $(pwd)",
                "${src}/configure -prefix ${install}",
                "make -j4",
                "make install"
            ],
            "soap.build": [
                "src=/Users/abel/Develop/c1-webrc/qt-solutions/qtsoap",
                "build=/Users/abel/Develop/c0-buildstation/qtsoap",
                "install=/Users/abel/Develop/d1-product/QtSoap",
                "cd $build",
                "${src}/configure -library"
            ],
            "qqt.push": [
                "src=${root.src}/LibQQt",
                "cd $src",
                "git push",
                "git push --tag"
            ],
            "qqt.pull": [
                "src=${root.src}/LibQQt",
                "cd $src",
                "git pull"
            ],
            "qqt.cloc": [
                "src=${root.src}/LibQQt",
                "cd $src",
                "perl ${pymake}/demo/cloc-1.74.pl  .",
                "date"
            ],
            "light.cloc": [
                "src=${root.src}/LightUnderWater/App",
                "cd $src",
                "perl ${pymake}/demo/cloc-1.74.pl  .",
                "date"
            ]
        }
    }

    # record current directory [pwd, execute path]
    startupworkdirectory = os.getcwd()
    # print( "pymake start up work directory:", startupworkdirectory )
    # here work root

    # use __file__ , but ... [relative path]
    # record pymake file directory, program file path.
    pymakefile = os.path.realpath(__file__)
    pymakefileroot = os.path.dirname(os.path.realpath(__file__))
    pymakefilename = os.path.basename(os.path.realpath(__file__))
    # print( "pymake file     :", pymakefile )
    # print( "pymake file root:", pymakefileroot )
    # print( "pymake file name:", pymakefilename )

    # record pymake project root, project files root.
    # important relative path. [relative to pymake.py] [relative to project root] [relative to caller file]
    pymakeprojectroot = os.path.join(pymakefileroot, pymakerelativepath)
    pymakeprojectroot = os.path.realpath(pymakeprojectroot)
    # print( "pymake project root:", pymakeprojectroot )

    # record pymake user source root [env, *.json] [ + auto create ]
    pymakedefaultsourceroot = pymakeprojectroot + os.path.sep + 'UserSource'
    if (not os.path.exists(pymakedefaultsourceroot)):
        os.makedirs(pymakedefaultsourceroot)
    # print( "pymake default source root:", pymakedefaultsourceroot )

    # record default user source config file name
    pymakedefaultsourcefile = 'pymake.json'
    # print( "pymake default source file:", pymakedefaultsourcefile )

    # print( "pymake default source config:", os.path.join(pymakedefaultsourceroot, pymakedefaultsourcefile) )
    # print( "pymake default source root  :", pymakedefaultsourceroot )
    # print( "pymake default source file  :", pymakedefaultsourcefile )

    # record pymake user shell root [ dynamic work path ]
    # default work directory
    pymakedefaultshellroot = pymakedefaultsourceroot + os.path.sep + 'UserShell'
    if (not os.path.exists(pymakedefaultshellroot)):
        os.makedirs(pymakedefaultshellroot)
    # print( "pymake default shell path:", pymakedefaultshellroot )


    # ===============================================================================================
    # PYMAKE PROGRAM CONFIGURE INITIAL CODE
    # --------------------
    """
    [pymake]

    [source]
    root = $pymakedefaultsourceroot
    config = $pymakedefaultsourcefile
    """
    userroot = getuserroot()
    configroot = getconfigroot()
    plat = getplatform()
    # record pymake configure directory. [ in user config path ]
    pymakeconfigureroot = userroot + os.path.sep + '.pymake'
    if (not os.path.exists(pymakeconfigureroot)):
        os.makedirs(pymakeconfigureroot)
    # initial pymake.ini
    pymakeconfigureini = pymakeconfigureroot + os.path.sep + 'pymake.ini'
    # add green ability
    # pymakegreenconfigureroot = pymakeprojectroot
    # pymakegreenconfigureini = os.path.join(pymakegreenconfigureroot, 'pymake.ini')
    # if (os.path.exists(pymakegreenconfigureini)):
    #     pymakeconfigureroot = pymakegreenconfigureroot
    #     pymakeconfigureini = pymakegreenconfigureini
    # print(pymakeconfigureroot)
    # print(pymakeconfigureini)
    # return
    conf = MyConfigParser()
    conf.read(pymakeconfigureini)
    if (not conf.has_section('pymake')):
        conf.add_section('pymake')
        conf.write(open(pymakeconfigureini, 'w'))
    if (not conf.has_section('source')):
        conf.add_section('source')
        conf.write(open(pymakeconfigureini, 'w'))
    if (not conf.has_option('source', 'root')):
        conf.set('source', 'root', pymakedefaultsourceroot)
        conf.write(open(pymakeconfigureini, 'w'))
    if (not conf.has_option('source', 'config')):
        conf.set('source', 'config', pymakedefaultsourcefile)
        conf.write(open(pymakeconfigureini, 'w'))

    # ===============================================================================================
    # PYMAKE ARGS COMMAND INITIAL CODE
    # --------------------
    args = {}
    # args = docopt(__doc__, version='pymake8.py v8.0')
    # print(args)

    # ===============================================================================================
    # PYMAKE INITIALIZE COMMAND INITIAL CODE
    # --------------------

    # initialize

    # ===============================================================================================
    # PYMAKE SOURCE ROOT INITIAL CODE
    # --------------------

    # init pymake.json in sourceroot [ + program create ]
    # record user source root directory
    sourceroot = conf.get('source', 'root')
    # record source config file name
    sourcefile = conf.get('source', 'config')
    # record source config file
    sourceconfigfile = sourceroot + os.path.sep + sourcefile
    # print("root: %s, config: %s" % (sourceroot, sourcefile))
    # print("use source config: %s" % (sourceconfigfile) )
    # record default source config file
    defaultsourceconfigfile = sourceroot + os.path.sep + pymakedefaultsourcefile
    # print ("root: %s, default config: %s" % (sourceroot, pymakedefaultsourcefile))
    # print("default source config: %s" % (defaultsourceconfigfile) )

    # print("user default source config: %s" % (defaultsourceconfigfile) )
    # print("user default source root  : %s" % (sourceroot))
    # print("user default source file  : %s" % (pymakedefaultsourcefile) )

    # check source root
    # prepare to user source root
    if (not os.path.exists(sourceroot)):
        os.makedirs(sourceroot)
    os.chdir(sourceroot)

    # check default source file [default:d]
    if (os.path.exists(sourceroot)):
        if (os.path.abspath(sourceroot) != os.path.abspath(pymakeconfigureroot)
            and os.path.abspath(sourceroot) != os.path.abspath(pymakeprojectroot)
            and os.path.abspath(sourceroot) != os.path.abspath(pymakefileroot)):
            if (not os.path.exists(defaultsourceconfigfile)):
                writeJsonData(defaultsourceconfigfile, d)

    # check source file [default:d]
    if (os.path.exists(sourceroot)):
        if (os.path.abspath(sourceroot) != os.path.abspath(pymakeconfigureroot)
            and os.path.abspath(sourceroot) != os.path.abspath(pymakeprojectroot)
            and os.path.abspath(sourceroot) != os.path.abspath(pymakefileroot)):
            if (not os.path.exists(sourceconfigfile)):
                writeJsonData(sourceconfigfile, d)

    def open_file(file0):
        plat = getplatform()
        cmd0 = ''
        if (plat == "Windows"):
            if (file0.__contains__(' ')):
                cmd0 = 'start "" ' + '"%s"' % file0
            else:
                cmd0 = "start " + file0
        elif (plat == "Darwin"):
            if (file0.__contains__(' ')):
                cmd0 = 'open ' + '"%s"' % file0
            else:
                cmd0 = "open " + file0
        else:
            if (file0.__contains__(' ')):
                cmd0 = 'xdg-open ' + '"%s" ' % file0 + ">/dev/null 2>&1"
            else:
                cmd0 = "xdg-open " + '%s ' % file0 + ">/dev/null 2>&1"
        return cmd0

    # record source config file postfix
    pymakesourcesuffix = '.json'

    # ===============================================================================================
    # PYMAKE SOURCE COMMAND INITIAL CODE
    # --------------------

    # source


    # ===============================================================================================
    # PYMAKE SOURCE ROOT INITIAL 2 CODE
    # --------------------

    # check source root directory
    if (os.path.exists(sourceroot) is False):
        print("You have changed sourceroot manually, please change it using source command")
        return

    # check source root .json file
    if (os.path.abspath(sourceroot) == os.path.abspath(pymakeconfigureroot)
            or os.path.abspath(sourceroot) == os.path.abspath(pymakeprojectroot)
            or os.path.abspath(sourceroot) == os.path.abspath(pymakefileroot)):
        print("I checked you use pymakeconfigureroot or pymakeprojectroot to be sourceroot, I suggest you use source command changing one.")
        print("This progrom can store building env and building command forever, please repleace source root then using it.")
        return
    elif (os.path.abspath(sourceroot) != os.path.abspath(pymakeconfigureroot)):
        if (not os.path.exists(defaultsourceconfigfile)):
            writeJsonData(defaultsourceconfigfile, d)
            print("initialize pymake.json in source root %s." % sourceroot)
        if (not os.path.exists(sourceconfigfile)):
            print("source config file %s is not existed." % sourceconfigfile)
            print("You can use source command to fix it.")
            return

    # ===============================================================================================
    # PYMAKE INITIALIZE COMMAND INITIAL CODE
    # --------------------

    # initialize


    # ===============================================================================================
    # PYMAKE SHELL ROOT INITIAL CODE
    # --------------------

    # record user shell root directory [default]
    defaultshellroot = sourceroot + os.path.sep + "UserShell"
    # print("execute directory: %s" % (defaultshellroot) )
    if (not os.path.exists(defaultshellroot)):
        os.makedirs(defaultshellroot)

    # support pymake default shell root, pymake custom shell root, pymake current shell root.
    pymakeexecini = sourceroot + os.path.sep + 'exec.ini'
    execconf = MyConfigParser()
    execconf.read(pymakeexecini)
    if (not execconf.has_section('exec')):
        execconf.add_section('exec')
        execconf.write(open(pymakeexecini, 'w'))
    if (not execconf.has_section('work')):
        execconf.add_section('work')
        execconf.write(open(pymakeexecini, 'w'))
    if (not execconf.has_section('root')):
        execconf.add_section('root')
        execconf.write(open(pymakeexecini, 'w'))
    if (not execconf.has_option('work', 'where')):
        execconf.set('work', 'where', 'default')
        execconf.write(open(pymakeexecini, 'w'))
    if (not execconf.has_option('root', 'default')):
        execconf.set('root', 'default', defaultshellroot)
        execconf.write(open(pymakeexecini, 'w'))
    if (not execconf.has_option('root', 'there')):
        execconf.set('root', 'there', defaultshellroot)
        execconf.write(open(pymakeexecini, 'w'))
    if (not execconf.has_option('root', 'here')):
        execconf.set('root', 'here', '')
        execconf.write(open(pymakeexecini, 'w'))

    # record pymake work root type [ where is the work root ? default, there, here ]
    workroottype = execconf.get('work', 'where')
    # record pymake custom shell root [ user custom work path ]
    customshellroot = execconf.get('root', 'there')
    if (not os.path.exists(customshellroot)):
        os.makedirs(customshellroot)
    # custom work root

    # record user shell root directory [starting] [1/3 maybe startup directory]
    shellroot = defaultshellroot
    if (workroottype == 'default'):
        shellroot = defaultshellroot
    elif (workroottype == 'here'):
        shellroot = startupworkdirectory
    elif (workroottype == 'there'):
        shellroot = customshellroot

    # if(workroottype == 'default'):
    #    print("WORK STARTING : %s" % (defaultshellroot))
    # elif (workroottype == 'here'):
    #    print("WORK STARTING : %s" % (startupworkdirectory))
    # elif (workroottype == 'there'):
    #    print("WORK STARTING : %s" % (customshellroot))
    # print("execute root type: %s" % (workroottype) )
    # print("execute directory: %s" % (shellroot) )

    # I set this,
    # pymake execute user bat/sh in shellroot,
    # user can use here param to restrict exec action.
    # cd user shell root [ default shell execute path ]
    # prepare to user shell root
    if (not os.path.exists(shellroot)):
        os.makedirs(shellroot)
    os.chdir(shellroot)
    # print( "pymake user shell root:", pymakeshellroot )

    # ===============================================================================================
    # PYMAKE BACKUP COMMAND INITIAL CODE
    # --------------------

    # backup

    # recovery

    # ===============================================================================================
    # PYMAKE PORT INITIAL CODE
    # --------------------

    # port translate function
    portdefaultsourceconfig = pymakedefaultsourcefile
    portdefaulttargetconfig = 'temporary-target.json'
    portiniconfig = 'port.ini'
    portinifile = os.path.join(sourceroot, "port.ini")

    def init_portconf():
        portconf = MyConfigParser()
        portconf.read(portinifile)
        if (not portconf.has_section('port')):
            portconf.add_section('port')
            portconf.write(open(portinifile, 'w'))
        if (not portconf.has_option('port', 'sourceroot')):
            portconf.set('port', 'sourceroot', sourceroot)
            portconf.write(open(portinifile, 'w'))
        if (not portconf.has_option('port', 'sourceconfig')):
            portconf.set('port', 'sourceconfig', portdefaultsourceconfig)
            portconf.write(open(portinifile, 'w'))
        if (not portconf.has_option('port', 'targetroot')):
            portconf.set('port', 'targetroot', sourceroot)
            portconf.write(open(portinifile, 'w'))
        if (not portconf.has_option('port', 'targetconfig')):
            portconf.set('port', 'targetconfig', portdefaulttargetconfig)
            portconf.write(open(portinifile, 'w'))
        # print (portinifile)
        # print ("sourceroot  :", portconf['port']['sourceroot'])
        # print ("sourceconfig:", portconf['port']['sourceconfig'])
        # print ("targetroot  :", portconf['port']['targetroot'])
        # print ("targetconfig:", portconf['port']['targetconfig'])
        return portconf, portinifile

    def init_portconfig(portconf=MyConfigParser()):
        portsourceconfigfile = os.path.join(portconf['port']['sourceroot'], portconf['port']['sourceconfig'])
        porttargetconfigfile = os.path.join(portconf['port']['targetroot'], portconf['port']['targetconfig'])
        d_temp = {
            "path-assemblage": {
            },
            "environ": {
                "default": {
                    "path+": [
                    ]
                },
                "current": "default"
            },
            "command": {
            }
        }
        if (not os.path.exists(portsourceconfigfile)):
            writeJsonData(portsourceconfigfile, d_temp)
        if (not os.path.exists(porttargetconfigfile)):
            writeJsonData(porttargetconfigfile, d_temp)

        srcsize = os.path.getsize(portsourceconfigfile)
        tarsize = os.path.getsize(porttargetconfigfile)
        if (srcsize < 166):
            writeJsonData(portsourceconfigfile, d_temp)
        if (tarsize < 166):
            writeJsonData(porttargetconfigfile, d_temp)

        portconfig = readJsonData(portsourceconfigfile)
        porttargetconfig = readJsonData(porttargetconfigfile)

        # hard
        if (portconfig.__contains__("path-assemblage") is False):
            portconfig['path-assemblage'] = {}
            writeJsonData(portsourceconfigfile, portconfig)
        if (portconfig.__contains__("environ") is False):
            portconfig['environ'] = {}
            writeJsonData(portsourceconfigfile, portconfig)
        if (portconfig.__contains__("command") is False):
            portconfig['command'] = {}
            writeJsonData(portsourceconfigfile, portconfig)

        # soft
        # if(portconfig['environ'].__contains__("default") is False):
        #    portconfig['environ']['default']={"path+":[]}
        #    writeJsonData(portsourceconfigfile, portconfig)
        # if(portconfig['environ']['default'].__contains__("path+") is False):
        #    portconfig['environ']['default']['path+']=[]
        #    writeJsonData(portsourceconfigfile, portconfig)
        # if(portconfig['environ'].__contains__("current") is False):
        #    portconfig['environ']['current']='default'
        #    writeJsonData(portsourceconfigfile, portconfig)

        # hard
        if (porttargetconfig.__contains__("path-assemblage") is False):
            porttargetconfig['path-assemblage'] = {}
            writeJsonData(porttargetconfigfile, porttargetconfig)
        if (porttargetconfig.__contains__("environ") is False):
            porttargetconfig['environ'] = {}
            writeJsonData(porttargetconfigfile, porttargetconfig)
        if (porttargetconfig.__contains__("command") is False):
            porttargetconfig['command'] = {}
            writeJsonData(porttargetconfigfile, porttargetconfig)

        # soft
        order_of_keys = porttargetconfig['environ'].keys()
        list_of_tuples = [key for key in order_of_keys]
        if (list_of_tuples.__len__() < 2):
            if (porttargetconfig['environ'].__contains__("default") is False):
                porttargetconfig['environ']['default'] = {"path+": []}
                writeJsonData(porttargetconfigfile, porttargetconfig)
            if (porttargetconfig['environ']['default'].__contains__("path+") is False):
                porttargetconfig['environ']['default']['path+'] = []
                writeJsonData(porttargetconfigfile, porttargetconfig)
            if (porttargetconfig['environ'].__contains__("current") is False):
                porttargetconfig['environ']['current'] = 'default'
                writeJsonData(porttargetconfigfile, porttargetconfig)

        # move 'current' to be last key
        order_of_keys = porttargetconfig['environ'].keys()
        list_of_tuples = [key for key in order_of_keys]
        # print(order_of_keys)
        # print(list_of_tuples)
        # print(list_of_tuples[-1])
        if (list_of_tuples[-1] != 'current'):
            # print(".....")
            current_var = porttargetconfig['environ']['current']
            porttargetconfig['environ'].__delitem__('current')
            porttargetconfig['environ']['current'] = current_var
            writeJsonData(porttargetconfigfile, porttargetconfig)
        return portconfig, porttargetconfig

    # initialize when need port function? no case.
    portconf, temp_var = init_portconf()
    init_portconfig(portconf)

    # port translate

    # set this command here .
    # program

    # ===============================================================================================
    # PYMAKE DEBUG INITIAL CODE
    # --------------------

    # debug.
    debugini = sourceroot + os.path.sep + "debug.ini"
    debugconf = MyConfigParser()
    debugconf.read(debugini)
    if (not debugconf.has_section('debug')):
        debugconf.add_section('debug')
        debugconf.write(open(debugini, 'w'))

    if (not debugconf.has_option('debug', 'switch')):
        debugconf.set('debug', 'switch', '0')
        debugconf.write(open(debugini, 'w'))

    debugswitch = debugconf['debug']['switch']
    if (debugswitch != '0' and debugswitch != '1'):
        debugswitch = '0'
        debugconf.set('debug', 'switch', debugswitch)
        debugconf.write(open(debugini, 'w'))

    # debug

    # ===============================================================================================
    # PYMAKE SOURCE CONFIG INITIAL CODE
    # --------------------

    config = readJsonData(sourceconfigfile)

    # print(config)

    def check_config():
        # hard
        if (config.__contains__("path-assemblage") is False):
            config['path-assemblage'] = {}
            writeJsonData(sourceconfigfile, config)
        if (config.__contains__("environ") is False):
            config['environ'] = {}
            writeJsonData(sourceconfigfile, config)
        if (config.__contains__("command") is False):
            config['command'] = {}
            writeJsonData(sourceconfigfile, config)

        # soft
        # if(portconfig['environ'].__contains__("default") is False):
        #    portconfig['environ']['default']={"path+":[]}
        #    writeJsonData(portsourceconfigfile, portconfig)
        # if(portconfig['environ']['default'].__contains__("path+") is False):
        #    portconfig['environ']['default']['path+']=[]
        #    writeJsonData(portsourceconfigfile, portconfig)
        # if(portconfig['environ'].__contains__("current") is False):
        #    portconfig['environ']['current']='default'
        #    writeJsonData(portsourceconfigfile, portconfig)

        # soft
        order_of_keys = config['environ'].keys()
        list_of_tuples = [key for key in order_of_keys]
        if (list_of_tuples.__len__() < 2):
            if (config['environ'].__contains__("default") is False):
                config['environ']['default'] = {"path+": []}
                writeJsonData(sourceconfigfile, config)
            if (config['environ']['default'].__contains__("path+") is False):
                config['environ']['default']['path+'] = []
                writeJsonData(sourceconfigfile, config)
            if (config['environ'].__contains__("current") is False):
                config['environ']['current'] = 'default'
                writeJsonData(sourceconfigfile, config)

        # move 'current' to be last key
        order_of_keys = config['environ'].keys()
        list_of_tuples = [key for key in order_of_keys]
        # print(order_of_keys)
        # print(list_of_tuples)
        # print(list_of_tuples[-1])
        if (list_of_tuples[-1] != 'current'):
            # print(".....")
            current_var = config['environ']['current']
            config['environ'].__delitem__('current')
            config['environ']['current'] = current_var
            writeJsonData(sourceconfigfile, config)
        return

    check_config()

    # ===============================================================================================
    # PYMAKE IMPORT COMMAND INITIAL CODE
    # --------------------

    # import command

    # set

    # set

    # get

    # ===============================================================================================
    # PYMAKE SOURCE RAW CONFIG INITIAL CODE
    # --------------------

    # record system environ
    pymakesystemenviron = copy.deepcopy(os.environ)

    ### config -> raw config
    rawconfig = copy.deepcopy(config)
    # print ( config )
    # print ( rawconfig )

    # replace path
    for (key, value) in rawconfig["path-assemblage"].items():
        # print (key) #...

        startpos = 0
        while (True):
            # print (startpos)

            index = value.find('${', startpos)
            if (index == -1):
                break

            index2 = value.find('}', index)
            startpos = index2

            key_replace = value[index:index2 + 1]
            # print ( key0 ) #${...}
            key_from = key_replace.split('{')[1].split('}')[0].strip()
            # print ( key1 ) #...

            for (find_key, find_value) in rawconfig["path-assemblage"].items():
                if (key == find_key):
                    break
                if (find_key == key_from):
                    rawconfig["path-assemblage"][key] = rawconfig["path-assemblage"][key].replace(
                        key_replace, rawconfig["path-assemblage"][key_from])
                    # print("xxx %s" % rawconfig["path-assemblage"][key])
                    break

                    # NO! ignore [in command, has various interpretations]
                    # for (find_key, find_value) in pymakesystemenviron.items():
                    #    if (key == find_key):
                    #        break
                    #    if (str(find_key).lower() == 'path'):
                    #        continue
                    #    if (find_key == key_from):
                    #        rawconfig["path-assemblage"][key] = rawconfig["path-assemblage"][key].replace(
                    #            key_replace, pymakesystemenviron[key_from])
                    #        # print("xxx %s" % rawconfig["path-assemblage"][key])
                    #        break

        # fix windows platform path sep
        # but no nessesary
        plat = getplatform()
        if (plat == "Windows"):
            ""
            # rawconfig["path-assemblage"][key] = rawconfig["path-assemblage"][key].replace('/', os.path.sep)

    # replace env
    # from path var env
    for current_var in rawconfig["environ"].keys():
        if (current_var == "current"):
            continue
        # replace path+
        step = 0
        for value in rawconfig["environ"][current_var]['path+']:
            startpos = 0
            while (True):
                # print (startpos)
                # print (value)

                index = value.find('${', startpos)
                if (index == -1):
                    break

                index2 = value.find('}', index)
                startpos = index2

                key_replace = value[index:index2 + 1]
                # print ( key0 ) #${...}
                key_from = key_replace.split('{')[1].split('}')[0].strip()
                # print ( key1 ) #...

                for (find_key, find_value) in rawconfig["path-assemblage"].items():
                    if (find_key == key_from):
                        rawconfig["environ"][current_var]['path+'][step] = rawconfig["environ"][current_var]['path+'][step].replace(
                            key_replace, rawconfig["path-assemblage"][key_from])
                        break
            step += 1
        for (key, value) in rawconfig["environ"][current_var].items():
            # print (key) #...
            if (key == "path+"):
                continue

            startpos = 0
            while (True):
                # print (startpos)

                index = value.find('${', startpos)
                if (index == -1):
                    break

                index2 = value.find('}', index)
                startpos = index2

                key_replace = value[index:index2 + 1]
                # print ( key0 ) #${...}
                key_from = key_replace.split('{')[1].split('}')[0].strip()
                # print ( key1 ) #...

                for (find_key, find_value) in rawconfig["path-assemblage"].items():
                    if (find_key == key_from):
                        rawconfig["environ"][current_var][key] = rawconfig["environ"][current_var][key].replace(
                            key_replace, rawconfig["path-assemblage"][key_from])
                        break

                for (find_key, find_value) in rawconfig["environ"][current_var].items():
                    if (key == find_key):
                        break
                    if (find_key == key_from):
                        rawconfig["environ"][current_var][key] = rawconfig["environ"][current_var][key].replace(
                            key_replace, rawconfig["environ"][current_var][key_from])
                        break

    # replace cmd
    # from path env
    for (cmd, stream) in rawconfig["command"].items():
        # print (key) #...

        step = 0
        for value in stream:
            startpos = 0
            while (True):
                # print (startpos)
                # print (value)

                index = value.find('${', startpos)
                if (index == -1):
                    break

                index2 = value.find('}', index)
                startpos = index2

                key_replace = value[index:index2 + 1]
                # print ( key0 ) #${...}
                key_from = key_replace.split('{')[1].split('}')[0].strip()
                # print ( key1 ) #...

                for (find_key, find_value) in rawconfig["path-assemblage"].items():
                    if (find_key == key_from):
                        rawconfig['command'][cmd][step] = rawconfig['command'][cmd][step].replace(
                            key_replace, rawconfig["path-assemblage"][key_from])
                        break

                current_env_var = rawconfig["environ"]["current"]
                for (find_key, find_value) in rawconfig["environ"][current_env_var].items():
                    if (find_key == key_from):
                        rawconfig['command'][cmd][step] = rawconfig['command'][cmd][step].replace(
                            key_replace, rawconfig["environ"][current_env_var][key_from])
                        break
            step += 1

    # raw path function, parse custom path tuple
    def raw_path(pathgroup0):
        pathgroup = copy.deepcopy(pathgroup0)

        # replace path
        for (key, value) in enumerate(pathgroup):
            # print (key) #...

            startpos = 0
            while (True):
                # print (startpos)

                index = value.find('${', startpos)
                if (index == -1):
                    break

                index2 = value.find('}', index)
                startpos = index2

                key_replace = value[index:index2 + 1]
                # print ( key0 ) #${...}
                key_from = key_replace.split('{')[1].split('}')[0].strip()
                # print ( key1 ) #...

                for (find_key, find_value) in rawconfig["path-assemblage"].items():
                    if (key == find_key):
                        break
                    if (find_key == key_from):
                        pathgroup[key] = pathgroup[key].replace(key_replace, rawconfig["path-assemblage"][key_from])
                        # print("xxx %s" % pathgroup[key])
                        break
        return pathgroup

    # custom command function
    # custom command stream from rawconfig
    def raw_command(env_name=None):
        command_dict = copy.deepcopy(config['command'])

        # replace cmd
        # from path env
        for (cmd, stream) in command_dict.items():
            # print (key) #...

            step = 0
            for value in stream:
                startpos = 0
                while (True):
                    # print (startpos)
                    # print (value)

                    index = value.find('${', startpos)
                    if (index == -1):
                        break

                    index2 = value.find('}', index)
                    startpos = index2

                    key_replace = value[index:index2 + 1]
                    # print ( key0 ) #${...}
                    key_from = key_replace.split('{')[1].split('}')[0].strip()
                    # print ( key1 ) #...

                    for (find_key, find_value) in rawconfig["path-assemblage"].items():
                        if (find_key == key_from):
                            command_dict[cmd][step] = command_dict[cmd][step].replace(
                                key_replace, rawconfig["path-assemblage"][key_from])
                            break

                    current_env_var = env_name
                    if (env_name is None):
                        current_env_var = config["environ"]["current"]
                    for (find_key, find_value) in rawconfig["environ"][current_env_var].items():
                        if (find_key == key_from):
                            command_dict[cmd][step] = command_dict[cmd][step].replace(
                                key_replace, rawconfig["environ"][current_env_var][key_from])
                            break
                step += 1
        return command_dict

    # custom string dict -> raw dict
    def raw_string(pathgroup0, env_name=None):
        pathgroup = {k: v for k, v in pathgroup0.items()}
        # print(pathgroup)

        dict0 = {k: v for k, v in rawconfig['path-assemblage'].items()}
        dict1 = {}
        current_env = env_name
        if (current_env == "current"):
            current_env = rawconfig['environ']['current']
        if (env_name is not None):
            dict1 = {k: v for k, v in rawconfig['environ'][current_env].items()}
        dict2 = {k: v for k, v in os.environ.items()}

        # replace path
        for (key, value) in pathgroup.items():
            # print (key, value) #...

            if (instanceof(value) != 'str'):
                continue

            if (dict0.__contains__(value) is True):
                pathgroup[key] = dict0[value]
                # print(pathgroup[key])
                continue

            if (dict2.__contains__(value) is True):
                pathgroup[key] = dict2[value]
                continue

            if (dict1.__contains__(value) is True):
                pathgroup[key] = dict1[value]
                continue

            startpos = 0
            while (True):
                # print (startpos)

                index = value.find('${', startpos)
                if (index == -1):
                    break

                index2 = value.find('}', index)
                startpos = index2

                key_replace = value[index:index2 + 1]
                # print ( key0 ) #${...}
                key_from = key_replace.split('{')[1].split('}')[0].strip()
                # print ( key1 ) #...

                for (find_key, find_value) in dict0.items():
                    # print("%-30s, %-30s, %-30s, path-assemblage" % (key, key_from, find_key))
                    if (key == find_key):
                        break
                    # if (find_key == 'path+'):
                    #    continue
                    # if (find_key == "path"):
                    #    continue
                    if (find_key == key_from):
                        pathgroup[key] = pathgroup[key].replace(key_replace, dict0[key_from])
                        # print("path-assemblage %s" % pathgroup[key])
                        break

                for (find_key, find_value) in dict2.items():
                    # print("%-30s, %-30s, %-30s, system env" % (key, key_from, find_key))
                    if (key == find_key):
                        break
                    # if (find_key == 'path+'):
                    #    continue
                    if (find_key == "path"):
                        continue
                    if (find_key == key_from):
                        pathgroup[key] = pathgroup[key].replace(key_replace, dict2[key_from])
                        # print("system env %s" % pathgroup[key])
                        break

                for (find_key, find_value) in dict1.items():
                    # print("%-30s, %-30s, %-30s, separate env" % (key, key_from, find_key))
                    if (key == find_key):
                        break
                    if (find_key == 'path+'):
                        continue
                    # if (find_key == "path"):
                    #    continue
                    if (find_key == key_from):
                        pathgroup[key] = pathgroup[key].replace(key_replace, dict1[key_from])
                        # print("separate env %s" % pathgroup[key])
                        break

        return pathgroup

    # custom string dict -> raw dict [ignore case]
    def raw_string1(pathgroup0, env_name=None):
        # pathgroup = {k: v.lower() for k, v in pathgroup0.items()}
        pathgroup1 = {k: v for k, v in pathgroup0.items()}
        # print(pathgroup)

        dict0 = {k.lower(): v for k, v in rawconfig['path-assemblage'].items()}
        dict1 = {}
        current_env = env_name
        if (current_env == "current"):
            current_env = rawconfig['environ']['current']
        if (env_name is not None):
            dict1 = {k.lower(): v for k, v in rawconfig['environ'][current_env].items()}
        dict2 = {k.lower(): v for k, v in os.environ.items()}

        # replace path
        for (key, value) in pathgroup0.items():
            # print (key, value) #...

            if (instanceof(value) != 'str'):
                continue

            if (dict0.__contains__(value.lower()) is True):
                pathgroup1[key] = dict0[value.lower()]
                # print(pathgroup1[key])
                continue

            if (dict2.__contains__(value.lower()) is True):
                pathgroup1[key] = dict2[value.lower()]
                continue

            if (dict1.__contains__(value.lower()) is True):
                pathgroup1[key] = dict1[value.lower()]
                continue

            startpos = 0
            while (True):
                # print (startpos)

                index = value.find('${', startpos)
                if (index == -1):
                    break

                index2 = value.find('}', index)
                startpos = index2

                key_replace = value[index:index2 + 1]
                # print ( key0 ) #${...}
                key_from = key_replace.split('{')[1].split('}')[0].strip()
                # print ( key1 ) #...

                for (find_key, find_value) in dict0.items():
                    # print("%-30s, %-30s, %-30s, path-assemblage" % (key, key_from, find_key))
                    if (key.lower() == find_key):
                        break
                    # if (find_key == 'path+'):
                    #    continue
                    # if (find_key == "path"):
                    #    continue
                    if (find_key == key_from.lower()):
                        pathgroup1[key] = pathgroup1[key].replace(key_replace, key_replace.lower())
                        pathgroup1[key] = pathgroup1[key].replace(key_replace.lower(), dict0[key_from.lower()])
                        # print("path-assemblage %s" % pathgroup1[key])
                        break

                for (find_key, find_value) in dict2.items():
                    # print("%-30s, %-30s, %-30s, system env" % (key, key_from, find_key))
                    if (key.lower() == find_key):
                        break
                    # if (find_key == 'path+'):
                    #    continue
                    if (find_key == "path"):
                        continue
                    if (find_key == key_from.lower()):
                        pathgroup1[key] = pathgroup1[key].replace(key_replace, key_replace.lower())
                        pathgroup1[key] = pathgroup1[key].replace(key_replace.lower(), dict2[key_from.lower()])
                        # print("system env %s" % pathgroup1[key])
                        break

                for (find_key, find_value) in dict1.items():
                    # print("%-30s, %-30s, %-30s, separate env" % (key, key_from, find_key))
                    if (key.lower() == find_key):
                        break
                    if (find_key == 'path+'):
                        continue
                    # if (find_key == "path"):
                    #    continue
                    if (find_key == key_from.lower()):
                        pathgroup1[key] = pathgroup1[key].replace(key_replace, key_replace.lower())
                        pathgroup1[key] = pathgroup1[key].replace(key_replace.lower(), dict1[key_from.lower()])
                        # print("separate env %s" % pathgroup1[key])
                        break

        return pathgroup1

    # which command [internal]
    def which_command(env_name=None, name='', postfix=[]):
        if (name is None or name == ''):
            return None

        # get python command.
        pycmd = name

        # get path ext
        pathext = []
        pathext.append('')
        pathext.extend(postfix)
        plat = getplatform()
        if (plat == "Windows"):
            pathext.extend(os.environ['PATHEXT'].split(os.path.pathsep))
        else:
            pathext.extend(['.sh', '.out', '.cmd'])

        # find in current path
        specialpath = [
            startupworkdirectory,
            os.getcwd()
        ]
        list0 = copy.deepcopy(specialpath)
        list0.reverse()
        for path0a in list0:
            for path0 in path0a.split(os.path.pathsep):
                path0 = path0.strip()
                # print(path0)
                path1 = ''
                for pext0 in pathext:
                    path1 = path0 + os.path.sep + pycmd + pext0
                    if (os.path.isfile(path1)):
                        if (plat == "Windows"):
                            return path1.replace('/', '\\')
                        else:
                            return path1.replace('\\', '/')

        if (env_name is not None):
            if (rawconfig['environ'].__contains__(env_name) is False):
                print("Fault Error! .json file is broken, env %s is losing!" % env_name)
                return None

            # find in separate env
            list0 = copy.deepcopy(rawconfig['environ'][env_name]['path+'])
            list0.reverse()
            for path0a in list0:
                for path0 in path0a.split(os.path.pathsep):
                    path0 = path0.strip()
                    # print(path0)
                    path1 = ''
                    for pext0 in pathext:
                        path1 = path0 + os.path.sep + pycmd + pext0
                        if (os.path.isfile(path1)):
                            if (plat == "Windows"):
                                return path1.replace('/', '\\')
                            else:
                                return path1.replace('\\', '/')

        # find in env. [custom+, local+, system]
        env = os.environ
        # for pathA in env['PATH'].split(os.path.pathsep):
        #    print(pathA)
        list0 = copy.deepcopy(env['PATH'].split(os.path.pathsep))
        # list0.reverse()
        for path0a in list0:
            for path0 in path0a.split(os.path.pathsep):
                path0 = path0.strip()
                # print(path0)
                path1 = ''
                for pext0 in pathext:
                    path1 = path0 + os.path.sep + pycmd + pext0
                    # print("[%s]" % path1)
                    if (os.path.isfile(path1)):
                        if (plat == "Windows"):
                            return path1.replace('/', '\\')
                        else:
                            return path1.replace('\\', '/')

        return None

    # which file [internal]
    def which_file(env_name=None, name='', postfix=[]):
        if (name is None or name == ''):
            return None

        # get python command.
        pycmd = name

        # get path ext
        pathext = []
        pathext.append('')
        pathext.extend(postfix)
        plat = getplatform()
        if (plat == "Windows"):
            pathext.extend([])
        else:
            pathext.extend([])

        # find in current path
        specialpath = [
            startupworkdirectory,
            os.getcwd()
        ]
        list0 = copy.deepcopy(specialpath)
        list0.reverse()
        for path0a in list0:
            for path0 in path0a.split(os.path.pathsep):
                path0 = path0.strip()
                # print(path0)
                path1 = ''
                for pext0 in pathext:
                    path1 = path0 + os.path.sep + pycmd + pext0
                    if (os.path.isfile(path1)):
                        if (plat == "Windows"):
                            return path1.replace('/', '\\')
                        else:
                            return path1.replace('\\', '/')

        if (env_name is not None):
            if (rawconfig['environ'].__contains__(env_name) is False):
                print("Fault Error! .json file is broken, env %s is losing!" % env_name)
                return None

            # find in separate env
            list0 = copy.deepcopy(rawconfig['environ'][env_name]['path+'])
            list0.reverse()
            for path0a in list0:
                for path0 in path0a.split(os.path.pathsep):
                    path0 = path0.strip()
                    # print(path0)
                    path1 = ''
                    for pext0 in pathext:
                        path1 = path0 + os.path.sep + pycmd + pext0
                        if (os.path.isfile(path1)):
                            if (plat == "Windows"):
                                return path1.replace('/', '\\')
                            else:
                                return path1.replace('\\', '/')

        # find in env. [custom+, local+, system]
        env = os.environ
        # for pathA in env['PATH'].split(os.path.pathsep):
        #    print(pathA)
        list0 = copy.deepcopy(env['PATH'].split(os.path.pathsep))
        # list0.reverse()
        for path0a in list0:
            for path0 in path0a.split(os.path.pathsep):
                path0 = path0.strip()
                # print(path0)
                path1 = ''
                for pext0 in pathext:
                    path1 = path0 + os.path.sep + pycmd + pext0
                    # print("[%s]" % path1)
                    if (os.path.isfile(path1)):
                        if (plat == "Windows"):
                            return path1.replace('/', '\\')
                        else:
                            return path1.replace('\\', '/')

        return None

    # pymake expand command-line.
    # current_var = args['<env-name>']
    # args = raw_string(args, current_var)
    # print(args)
    # for (k, v) in args.items():
    #    if(isinstance(v, str)):
    #        print(k, v)
    # return

    # system command function
    # system command stream from rawconfig path-assemblage
    def raw_command_system():
        command_dict = copy.deepcopy(config['command'])

        # replace cmd
        # from path env
        for (cmd, stream) in command_dict.items():
            # print (key) #...

            step = 0
            for value in stream:
                startpos = 0
                while (True):
                    # print (startpos)
                    # print (value)

                    index = value.find('${', startpos)
                    if (index == -1):
                        break

                    index2 = value.find('}', index)
                    startpos = index2

                    key_replace = value[index:index2 + 1]
                    # print ( key0 ) #${...}
                    key_from = key_replace.split('{')[1].split('}')[0].strip()
                    # print ( key1 ) #...

                    for (find_key, find_value) in rawconfig["path-assemblage"].items():
                        if (find_key == key_from):
                            command_dict[cmd][step] = command_dict[cmd][step].replace(
                                key_replace, rawconfig["path-assemblage"][key_from])
                            break

                step += 1
        return command_dict

    # .bat .sh, windows, unix, system
    def createCmdList06(env_name=None, local=True, list0=[], params0=[]):
        cmd_list = []

        name = uuid.uuid4().__str__()
        name = name.split('-')[0]
        # print (name)

        plat = getplatform()
        if (plat == "Windows"):
            cmd_status = "echo pymake-command-status:%ERRORLEVEL%"
            cmd_sep = '&'
            cmd_codec = "ansi"
            if (getplatform_release() == "XP"):
                cmd_codec = None
            # but windows, it is \r\n, python helpping me?
            cmd_return = "\n"
            cmd_exit = 'exit /b %ERRORLEVEL%'
            cmd_suffix = ".bat"
            cmd_header = "@echo off"
            cmd_call = "call "
            # window close echo, close promot
            cmd_list.append(cmd_header)
            # os.system("type env_effect.bat > cmd_exec.bat")
            if (env_name != None):
                cmd_list.append("call %s_effect.bat" % name)
        else:
            cmd_status = "echo pymake-command-status:$?"
            cmd_sep = ';'
            cmd_suffix = ".sh"
            cmd_exit = 'exit $?'
            cmd_codec = "utf8"
            cmd_return = "\n"
            cmd_header = "#!/usr/bin/env bash"
            cmd_call = "./"
            cmd_list.append(cmd_header)
            if (env_name != None):
                cmd_list.append("source %s_effect.sh" % name)

        params_string = ""
        for param in params0:
            if (str(param).__contains__(' ')):
                params_string += '"' + param + '"' + ' '
            else:
                params_string += param + ' '
        # print(params_string)

        if (local is True):
            for cmd in list0:
                cmd_list.append(cmd)
        else:
            for cmd in list0:
                if (str(cmd).__contains__(' ')):
                    pycmd = which_command(env_name, str(cmd))
                    # print(pycmd)
                    if (pycmd is not None and os.path.isfile(pycmd)):
                        cmd_list.append('"' + cmd + '"' + ' ' + params_string)
                    else:
                        cmd_list.append(cmd + ' ' + params_string)
                else:
                    cmd_list.append(cmd + ' ' + params_string)

        # append exit 0
        cmd_list.append(cmd_exit)
        # print( cmd_list )

        cmd_execute = name + "_exec" + cmd_suffix
        with open(cmd_execute, "w", encoding=cmd_codec) as f:
            for line in cmd_list:
                f.write(line + cmd_return)
        # print(cmd_execute)

        if (debugswitch == '1'):
            print("IN: execute file: %s" % cmd_execute)
            for cmd in cmd_list:
                print(cmd)
            print("---------------------------")

        if (plat == "Windows"):
            ""
        else:
            os.system("chmod +x " + cmd_execute)

        cmd_list.clear()
        if (plat == "Windows"):
            cmd_list.append(cmd_header + ' ' + cmd_sep + ' ' + cmd_status)
            # cmd_list.append("call " + cmd_execute + cmd_sep + ' ' + cmd_status)
            cmd_list.append("call " + cmd_execute + ' ' + params_string + cmd_sep + ' ' + cmd_status)
        else:
            # cmd_list.append(cmd_header + ' ' + cmd_sep + ' ' + cmd_status)
            # cmd_list.append("./" + cmd_execute + cmd_sep + ' ' + cmd_status)
            cmd_list.append("./" + cmd_execute + ' ' + params_string + cmd_sep + ' ' + cmd_status)

        cmd_list.append(cmd_exit)

        # print (cmd_list)
        if (debugswitch == '1'):
            print("CMD: call execute file: %s" % cmd_execute)
            for cmd in cmd_list:
                print(cmd)
            print("---------------------------")

        return cmd_list, name

    # system export function
    def system_env_export(env_name=None, file_name=None):
        if (env_name == None):
            return env_name, '', ''

        # select env
        current_var = rawconfig['environ']['current']
        if (env_name is not None):
            current_var = env_name
        dict0 = copy.deepcopy(rawconfig['environ'][current_var])

        plat = getplatform()
        if (plat == "Windows"):
            cmd_suffix = ".bat"
            cmd_codec = "ansi"
            if (getplatform_release() == "XP"):
                cmd_codec = None
            cmd_return = "\n"
            cmd_header = "@echo off" + cmd_return
            env_set = 'set '
        else:
            cmd_suffix = ".sh"
            cmd_codec = "utf8"
            cmd_return = "\n"
            cmd_header = "#!/usr/bin/env bash" + cmd_return
            env_set = 'export '

        # export effect env
        cmd_effect = 'env'
        if (file_name is not None):
            cmd_effect = file_name
        cmd_effect += '_effect' + cmd_suffix

        # export path
        lines = ""
        for (key) in dict0["path+"]:
            if (plat == "Windows"):
                lines += (env_set + 'PATH=' + key + os.path.pathsep + '%PATH%' + cmd_return)
            else:
                lines += (env_set + 'PATH="' + key + '"' + os.path.pathsep + '$PATH' + cmd_return)

        # export var
        for (key, value) in dict0.items():
            if (key == 'path+'):
                continue
            if (plat == "Windows"):
                lines += (env_set + key + '=' + value + cmd_return)
            else:
                lines += (env_set + key + '=\"' + value + '\"' + cmd_return)

        with open(cmd_effect, 'w', encoding=cmd_codec) as f:
            f.write(cmd_header)
            f.write(lines)

        # export unset env
        cmd_unset = 'env'
        if (file_name is not None):
            cmd_unset = file_name
        cmd_unset += '_unset' + cmd_suffix

        # export unset path
        lines = ""
        for (key) in dict0["path+"]:
            if (plat == "Windows"):
                lines += (env_set + 'PATH=%PATH:' + key + ';=%' + cmd_return)
            else:
                lines += (env_set + 'PATH=$(' + 'echo ${PATH//' + key.replace('/', '\/') + ':/})' + cmd_return)

        # export unset var
        for (key, value) in dict0.items():
            if (key == 'path+'):
                continue
            if (plat == "Windows"):
                lines += ('set ' + key + '=' + cmd_return)
            else:
                lines += ('unset ' + key + cmd_return)
        with open(cmd_unset, 'w', encoding=cmd_codec) as f:
            f.write(cmd_header)
            f.write(lines)

        if (plat == "Windows"):
            ""
        else:
            os.system("chmod +x " + cmd_effect)
            os.system("chmod +x " + cmd_unset)

        # return file name
        return current_var, cmd_effect, cmd_unset

    # ===============================================================================================
    # PYMAKE SYSTEM COMMAND INITIAL CODE
    # --------------------

    # print(args)
    # system ccvp

    # outport command

    # get open path's cmd list
    def open_command(pathlist0, env_name=None):
        pathgroup0 = {}
        for (k, v) in enumerate(pathlist0):
            pathgroup0[str('-pymake-open-p%d' % k)] = str(v)

        cmd_list = []
        pathgroup1 = {}
        # if(args['-i'] or args['--ignorecase'] is True):
        #    pathgroup1 = raw_string1(pathgroup0, env_name)
        # else:
        #    pathgroup1 = raw_string(pathgroup0, env_name)
        pathgroup1 = raw_string1(pathgroup0, env_name)

        plat = getplatform()
        for (k, v) in pathgroup1.items():
            path0 = str(v)
            pathslist = path0.split(os.path.pathsep)
            while (pathslist.__contains__('')):
                pathslist.remove('')
            # print(pathslist)
            for path0 in pathslist:
                cmd0 = ''
                if (plat == "Windows"):
                    if (path0.__contains__(' ')):
                        cmd0 = 'start "" ' + '"%s"' % path0
                    else:
                        cmd0 = "start " + path0
                elif (plat == "Darwin"):
                    if (path0.__contains__(' ')):
                        cmd0 = 'open ' + '"%s"' % path0
                    else:
                        cmd0 = "open " + path0
                else:
                    if (path0.__contains__(' ')):
                        cmd0 = 'xdg-open ' + '"%s" ' % path0 + ">/dev/null 2>&1"
                    else:
                        cmd0 = "xdg-open " + '%s ' % path0 + ">/dev/null 2>&1"
                cmd_list.append(cmd0)

        return cmd_list

    # print(args)
    # open command

    # ===============================================================================================
    # PYMAKE LOCAL ENV INITIAL CODE
    # --------------------

    # pymake local const variable.
    localini = sourceroot + os.path.sep + "local.ini"
    localconf = MyConfigParser()
    localconf.read(localini)
    if (not localconf.has_section('local')):
        localconf.add_section('local')
        localconf.write(open(localini, 'w'))
    if (not localconf.has_section('path+')):
        localconf.add_section('path+')
        localconf.write(open(localini, 'w'))
    if (not localconf.has_section('variable')):
        localconf.add_section('variable')
        localconf.write(open(localini, 'w'))

    # status readonly
    # if( not localconf.has_option('local', 'status') ):
    #    localconf.set('local', 'status', 'readonly')
    #    localconf.write(open(localini, 'w'))

    # localswitch = localconf['local']['status']
    # if(localswitch != 'readonly'):
    #    localswitch = 'readonly'
    #    localconf.set('local', 'status', localswitch)
    #    localconf.write(open(localini, 'w'))

    # switch [1, default]
    if (not localconf.has_option('local', 'switch')):
        localconf.set('local', 'switch', '1')
        localconf.write(open(localini, 'w'))

    localswitch = localconf['local']['switch']
    if (localswitch != '0' and localswitch != '1'):
        localswitch = '1'
        localconf.set('local', 'switch', localswitch)
        localconf.write(open(localini, 'w'))

    localenv = {}
    localenv['path+'] = []

    # set into env [no effect to system environ]
    while (True):
        if (int(localswitch) == 0):
            break

        env = os.environ

        localenv['PYMAKEDEFAULTSOURCEROOT'] = pymakedefaultsourceroot
        localenv['PYMAKEDEFAULTSOURCECONFIG'] = pymakedefaultsourcefile

        localenv['PYMAKESOURCEFILE'] = sourceconfigfile
        localenv['PYMAKESOURCEROOT'] = sourceroot
        localenv['PYMAKESOURCECONFIG'] = sourcefile

        localenv['PYMAKEDEFAULTWORKROOT'] = defaultshellroot
        localenv['PYMAKETHEREWORKROOT'] = customshellroot
        localenv['PYMAKEHEREWORKROOT'] = startupworkdirectory
        localenv['PYMAKEWORKROOT'] = shellroot
        # localenv['PYMAKEWORKROOTTYPE'] = workroottype

        if ((args.__contains__('here') or args.__contains__('hh')) and (args['here'] or args['hh'] is True)):
            localenv['PYMAKEWORKROOT'] = startupworkdirectory
        elif ((args.__contains__('there') or args.__contains__('tt')) and (args['there'] or args['tt'] is True)):
            localenv['PYMAKEWORKROOT'] = customshellroot
        elif ((args.__contains__('default') or args.__contains__('dd')) and (args['default'] or args['dd'] is True)):
            localenv['PYMAKEWORKROOT'] = defaultshellroot

        if (args.__contains__('--workroot') and args['--workroot'] is not None):
            if (os.path.isdir(args['--workroot'])
                and os.path.isabs(args['--workroot'])):
                localenv['PYMAKEWORKROOT'] = args['--workroot']
                # os.chdir(args['--workroot'])
            else:
                print('please input a legal work root.')
                return

        localenv['PYMAKEPROGRAM'] = pymakefile
        localenv['PYMAKEPROGRAMROOT'] = pymakefileroot
        localenv['PYMAKEPROGRAMFILE'] = pymakefilename

        localenv['PYMAKEPROGRAMCONFIGURE'] = os.path.realpath(pymakeconfigureini)
        localenv['PYMAKEPROGRAMCONFIGUREROOT'] = os.path.split(os.path.realpath(pymakeconfigureini))[0]
        localenv['PYMAKEPROGRAMCONFIGUREFILE'] = os.path.split(os.path.realpath(pymakeconfigureini))[1]

        if (getplatform() == 'Windows'):
            localenv['PYMAKEINSTALLROOT'] = env['windir']
        else:
            localenv['PYMAKEINSTALLROOT'] = '/usr/local/bin'

        localenv['path+'].append(localenv['PYMAKEPROGRAMROOT'])
        localenv['path+'].append(localenv['PYMAKESOURCEROOT'])
        localenv['path+'].append(localenv['PYMAKEDEFAULTWORKROOT'])
        # if(localenv['path+'].__contains__(localenv['PYMAKETHEREWORKROOT']) is False):
        localenv['path+'].append(localenv['PYMAKETHEREWORKROOT'])
        # if(localenv['path+'].__contains__(localenv['PYMAKEHEREWORKROOT']) is False):
        localenv['path+'].append(localenv['PYMAKEHEREWORKROOT'])
        # if(localenv['path+'].__contains__(localenv['PYMAKEWORKROOT']) is False):
        localenv['path+'].append(localenv['PYMAKEWORKROOT'])

        # store to file
        for (key, value) in enumerate(localenv["path+"]):
            localconf.set('path+', str("%d" % key), value)

        for (key, value) in localenv.items():
            if (key == 'path+'):
                continue
            localconf.set('variable', key, value)

        localconf.write(open(localini, 'w'))

        # set into env
        for (key) in localenv["path+"]:
            env["PATH"] = key + os.path.pathsep + env["PATH"]

        for (key, value) in localenv.items():
            if (key == 'path+'):
                continue
            env[key] = value

        # print(json.dumps(localenv, indent=4, sort_keys=False, ensure_ascii=False))
        break

    # local command

    # ===============================================================================================
    # PYMAKE CUSTOM ENV INITIAL CODE
    # --------------------

    # initial custom environ module
    pymakecustomini = sourceroot + os.path.sep + "custom.ini"
    customconf = MyConfigParser()
    customconf.read(pymakecustomini)
    if (not customconf.has_section('custom')):
        customconf.add_section('custom')
        customconf.write(open(pymakecustomini, 'w'))
    if (not customconf.has_option('custom', 'switch')):
        customconf.set('custom', 'switch', '1')
        customconf.write(open(pymakecustomini, 'w'))

    switch0 = customconf['custom']['switch']
    if (switch0 != '0' and switch0 != '1'):
        switch0 = '1'
        customconf.set('custom', 'switch', switch0)
        customconf.write(open(pymakecustomini, 'w'))

    custompathfile = sourceroot + os.path.sep + "custom.path+.ini"
    customenvfile = sourceroot + os.path.sep + "custom.var+.ini"

    storecustompaths = []
    storecustomvars = []

    envcustomlistpaths = []
    envcustomlistvars = {}

    envcustomlistrawpaths = []
    envcustomlistrawvars = {}

    plat = getplatform()
    cmd_codec = "utf8"
    cmd_return = "\n"
    if (plat == "Windows"):
        cmd_codec = "ansi"
        if (getplatform_release() == "XP"):
            cmd_codec = None
        # but windows, it is \r\n, python helpping me?
        cmd_return = "\n"
    else:
        cmd_codec = "utf8"
        cmd_return = "\n"

    # custom environ
    # user can use custom environ to effect pymake basic environment.
    # it will effect every executing environment.
    while (True):

        if (int(switch0) == 0):
            break

        # print("open custom environ.")

        # set custom path+ to env.
        # print(sourceroot)
        # print(shellroot)
        # init file
        # custompathfile = sourceroot + os.path.sep + "custom.path+.ini"
        if (os.path.exists(custompathfile) is False):
            with open(custompathfile, 'w', encoding=cmd_codec) as f:
                ''

        # read all
        custompaths = []
        with open(custompathfile, 'r', encoding=cmd_codec) as f:
            for l in f.readlines():
                # important format
                # l = l.strip()
                while (l.endswith('\r') or l.endswith('\n') or l.endswith('\r\n')):
                    l = l.rstrip('\r\n')
                    l = l.rstrip('\n')
                    l = l.rstrip('\r')
                # if(l == ''):
                #    continue
                custompaths.append(l)
        # while(custompaths.__contains__('') is True):
        # custompaths.remove('')
        # print(custompaths)
        # print(os.linesep)
        # for l in custompaths:
        #    print("AAAA:" + l)

        # write back

        # strip
        storecustompaths = copy.deepcopy(custompaths)
        for (i, l) in enumerate(storecustompaths):
            # import format
            l = l.strip()
            storecustompaths[i] = l

        # added by local, ignore
        '''
        # default [ fixed ]
        # add pymake default source root to environ.
        if (storecustompaths.__contains__(pymakedefaultsourceroot) is False):
            storecustompaths.append(pymakedefaultsourceroot)
        # add pymake default shell root to environ.
        if (storecustompaths.__contains__(pymakedefaultshellroot) is False):
            storecustompaths.append(pymakedefaultshellroot)
        # default [ movable, follow user source root ]
        # add user source root to environ.
        if (sourceroot != pymakedefaultsourceroot and storecustompaths.__contains__(sourceroot) is False):
            storecustompaths.append(sourceroot)
        # add user shell root to environ.
        if (customshellroot != defaultshellroot and storecustompaths.__contains__(customshellroot) is False):
            storecustompaths.append(customshellroot)
        if (startupworkdirectory != defaultshellroot and storecustompaths.__contains__(startupworkdirectory) is False):
            storecustompaths.append(startupworkdirectory)
        if (shellroot != defaultshellroot and storecustompaths.__contains__(shellroot) is False):
            storecustompaths.append(shellroot)
        '''

        # clean repeat path [for store]
        clean_list = []
        temp_list = []
        for l in storecustompaths:
            if (l == ''):
                continue
            if (os.path.isabs(l) is False):
                continue
            if (temp_list.__contains__(str(l).replace('\\', '/').lower())):
                clean_list.append(l)
                continue
            else:
                temp_list.append(str(l).replace('\\', '/').lower())
        # print(clean_list)
        storecustompaths.reverse()
        for l in clean_list:
            if (storecustompaths.__contains__(l) is True):
                storecustompaths.remove(l)
        storecustompaths.reverse()

        if (custompaths != storecustompaths):
            with open(custompathfile, 'w', encoding=cmd_codec) as f:
                for l in storecustompaths:
                    f.write(l + cmd_return)

        # set into env

        # raw
        envcustompaths = copy.deepcopy(storecustompaths)
        envcustomrawpaths = raw_path(envcustompaths)
        # print(envcustompaths)
        # print(envcustomrawpaths)

        # envcustomlistpaths
        for (key, l) in zip(envcustompaths, envcustomrawpaths):
            if (l == ''):
                continue
            # print(os.path.isabs(l), l)
            if (os.path.isabs(l) is False):
                continue
            envcustomlistpaths.append(key)

        # clean illgal path
        clean_list = []
        for l in envcustomrawpaths:
            if (l == ''):
                clean_list.append(l)
                continue
            if (os.path.isabs(l) is False):
                clean_list.append(l)
                continue
        # print(clean_list)

        for l in clean_list:
            if (envcustomrawpaths.__contains__(l) is True):
                envcustomrawpaths.remove(l)

        env = os.environ
        for l in envcustomrawpaths:
            env["PATH"] = l + os.path.pathsep + env["PATH"]

        for l in envcustomrawpaths:
            envcustomlistrawpaths.append(l)

        # set custom env+ to env.
        # customenvfile = sourceroot + os.path.sep + "custom.var+.ini"
        # print(customenvfile)
        # init
        if (os.path.exists(customenvfile) is False):
            with open(customenvfile, 'w', encoding=cmd_codec) as f:
                ''

        # read all
        customenvs = []
        with open(customenvfile, 'r', encoding=cmd_codec) as f:
            for l in f.readlines():
                # important format
                # l = l.strip()
                while (l.endswith('\r') or l.endswith('\n') or l.endswith('\r\n')):
                    l = l.rstrip('\r\n')
                    l = l.rstrip('\n')
                    l = l.rstrip('\r')
                # if(l == ''):
                #    continue
                customenvs.append(l)

        # write back

        # strip
        storecustomvars = copy.deepcopy(customenvs)
        for (i, l) in enumerate(storecustomvars):
            # important format
            l = l.strip()
            storecustomvars[i] = l

        avarkeyvalue = "PYMAKEAUTHOR=T.D.R."
        if (storecustomvars.__contains__(avarkeyvalue) is False):
            storecustomvars.append(avarkeyvalue)

        # clean repeat var [for store]
        clean_list = []
        temp_list = []
        for l in storecustomvars:
            if (l == ''):
                continue
            if (str(l).__contains__('=') is False):
                continue
            if (temp_list.__contains__(str(l).split('=')[0].strip().lower())):
                clean_list.append(l)
                continue
            else:
                temp_list.append(str(l).split('=')[0].strip().lower())
        # print(clean_list)
        storecustomvars.reverse()
        for l in clean_list:
            if (storecustomvars.__contains__(l) is True):
                storecustomvars.remove(l)
        storecustomvars.reverse()

        if (storecustomvars != customenvs):
            with open(customenvfile, 'w', encoding=cmd_codec) as f:
                for l in storecustomvars:
                    f.write(l + cmd_return)

        # set into env

        # raw
        envcustomvars = copy.deepcopy(storecustomvars)
        envcustomrawvars = raw_path(envcustomvars)
        # print(envcustomvars)
        # print(envcustomrawvars)
        for (key0, l) in zip(envcustomvars, envcustomrawvars):
            if (l == ''):
                continue
            if (str(l).__contains__('=') is False):
                continue
            key = str(key0).split('=')[0].strip()
            value = '='.join(str(key0).split('=')[1:]).strip()
            envcustomlistvars[key] = value

        # clean illgal var
        clean_list = []
        for l in envcustomrawvars:
            if (l == ''):
                clean_list.append(l)
                continue
            if (str(l).__contains__('=') is False):
                clean_list.append(l)
                continue
        # print(clean_list)

        for l in clean_list:
            if (envcustomrawvars.__contains__(l) is True):
                envcustomrawvars.remove(l)

        env = os.environ
        for l in envcustomrawvars:
            key = str(l).split('=')[0].strip()
            value = '='.join(str(l).split('=')[1:]).strip()
            env[key] = value

        for l in envcustomrawvars:
            key = str(l).split('=')[0].strip()
            value = '='.join(str(l).split('=')[1:]).strip()
            envcustomlistrawvars[key] = value

        break

    # ===============================================================================================
    # PYMAKE VC MODULE INITIAL CODE
    # --------------------

    # initial vc module
    # record vc shell root.
    vcroot = sourceroot + os.path.sep + "VCShell"
    if (not os.path.exists(vcroot)):
        os.mkdir(vcroot)

    d_vc = {
        'environ': {

        }
    }

    pymakevcconfigfile = vcroot + os.path.sep + 'pymake-vc-command.json'
    # print(pymakevcconfigfile)
    if (not os.path.exists(pymakevcconfigfile)):
        writeJsonData(pymakevcconfigfile, d_vc)

    pymakevcdict = readJsonData(pymakevcconfigfile)
    if (pymakevcdict.__contains__('environ') is False):
        # print('Fault error: %s is broken, please delete it manully.' % pymakevcconfigfile)
        # return
        pymakevcdict['environ'] = {}
        writeJsonData(pymakevcconfigfile, pymakevcdict)
    # print(pymakevcdict)

    # check command

    def vc_createCmdList08(shellenvname=None, env_name=None, local=True, list0=[], params0=[]):

        cmd_list = []

        if (env_name is None):
            env_name = rawconfig['environ']['current']
        # print(env_name)

        name = uuid.uuid4().__str__()
        name = name.split('-')[0]
        # print (name)

        plat = getplatform()
        if (plat == "Windows"):
            cmd_status = "echo pymake-command-status:%ERRORLEVEL%"
            cmd_sep = '&'
            cmd_codec = "ansi"
            if (getplatform_release() == "XP"):
                cmd_codec = None
            # but windows, it is \r\n, python helpping me?
            cmd_return = "\n"
            cmd_exit = 'exit /b %ERRORLEVEL%'
            cmd_suffix = ".bat"
            cmd_header = "@echo off"
            cmd_call = "call "
            # window close echo, close promot
            cmd_list.append(cmd_header)
            # os.system("type env_effect.bat > cmd_exec.bat")
            cmd_list.append("call %s_effect.bat" % name)
            if (shellenvname is not None):
                cmd_list.append('call \"%s_effect%s\"' % (shellenvname, cmd_suffix))
        else:
            cmd_status = "echo pymake-command-status:$?"
            cmd_sep = ';'
            cmd_suffix = ".sh"
            cmd_exit = 'exit $?'
            cmd_codec = "utf8"
            cmd_return = "\n"
            cmd_header = "#!/usr/bin/env bash"
            cmd_call = "./"
            cmd_list.append(cmd_header)
            cmd_list.append("source %s_effect.sh" % name)
            if (shellenvname is not None):
                cmd_list.append('source \"%s_effect%s\"' % (shellenvname, cmd_suffix))

        # print(params0)
        params_string = ""
        for param in params0:
            # print(param)
            if (str(param).__contains__(' ')):
                params_string += '"' + param + '"' + ' '
            else:
                params_string += param + ' '
        # print(params_string)

        if (local is True):
            for cmd in list0:
                cmd_list.append(cmd)
        else:
            for cmd in list0:
                if (str(cmd).__contains__(' ')):
                    pycmd = which_command(env_name, str(cmd))
                    # print(str(cmd))
                    # print(Fore.RED + "which command:", pycmd)
                    if (pycmd is not None and os.path.isfile(pycmd)):
                        cmd_list.append('"' + cmd + '"' + ' ' + params_string)
                    else:
                        cmd_list.append(cmd + ' ' + params_string)
                else:
                    cmd_list.append(cmd + ' ' + params_string)

        # append exit 0
        cmd_list.append(cmd_exit)
        # print( cmd_list )

        cmd_execute = name + "_exec" + cmd_suffix
        with open(cmd_execute, "w", encoding=cmd_codec) as f:
            for line in cmd_list:
                f.write(line + cmd_return)
        # print(cmd_execute)

        if (debugswitch == '1'):
            print("IN: execute file: %s" % cmd_execute)
            for cmd in cmd_list:
                print(cmd)
            print("---------------------------")

        if (plat == "Windows"):
            ""
        else:
            os.system("chmod +x " + cmd_execute)

        cmd_list.clear()
        if (plat == "Windows"):
            cmd_list.append(cmd_header + ' ' + cmd_sep + ' ' + cmd_status)
            # cmd_list.append("call " + cmd_execute + cmd_sep + ' ' + cmd_status)
            cmd_list.append("call " + cmd_execute + ' ' + params_string + cmd_sep + ' ' + cmd_status)
        else:
            # cmd_list.append(cmd_header + ' ' + cmd_sep + ' ' + cmd_status)
            # cmd_list.append("./" + cmd_execute + cmd_sep + ' ' + cmd_status)
            cmd_list.append("./" + cmd_execute + ' ' + params_string + cmd_sep + ' ' + cmd_status)

        cmd_list.append(cmd_exit)

        # print (cmd_list)
        if (debugswitch == '1'):
            print("CMD: call execute file: %s" % cmd_execute)
            for cmd in cmd_list:
                print(cmd)
            print("---------------------------")

        return cmd_list, name

    def vc_powershell_createCmdList08(shellenvname=None, env_name=None, local=True, list0=[], params0=[]):

        cmd_list = []

        if (env_name is None):
            env_name = rawconfig['environ']['current']

        name = uuid.uuid4().__str__()
        name = name.split('-')[0]
        # print (name)

        # plat = getplatform()

        cmd_status = "echo pymake-command-status:$LASTEXITCODE"
        cmd_sep = ';'
        cmd_suffix = ".ps1"
        cmd_suffix_powershell = cmd_suffix
        cmd_exit = 'exit $LASTEXITCODE'
        cmd_codec = 'ansi'
        if (getplatform_release() == "XP"):
            cmd_codec = None
        # but windows, it is \r\n, python helpping me?
        cmd_return = "\n"
        cmd_header = "#!/usr/bin/env bash"
        cmd_call = "./"
        # cmd_list.append(cmd_header)
        cmd_list.append("./%s_effect%s" % (name, cmd_suffix))

        if (shellenvname is not None):
            cmd_list.append('. \"%s_effect%s\"' % (shellenvname, cmd_suffix))

        params_string = ""
        for param in params0:
            if (str(param).__contains__(' ')):
                params_string += '"' + param + '"' + ' '
            else:
                params_string += param + ' '
        # print(params_string)

        if (local is True):
            for cmd in list0:
                cmd_list.append(cmd)
        else:
            # actually now has only one command.
            for cmd in list0:
                # warning: now pymake is in user setted workroot.
                if (str(cmd).endswith(cmd_suffix_powershell)):
                    cmd_suffix_powershell = ''

                powershellexecfile = ""
                while (True):
                    # find in current path [+workroot]
                    powershellexecfile = os.getcwd() + os.path.sep + cmd + cmd_suffix_powershell
                    if (os.path.exists(powershellexecfile)):
                        break
                    powershellexecfile = startupworkdirectory + os.path.sep + cmd + cmd_suffix_powershell
                    if (os.path.exists(powershellexecfile)):
                        break

                    # find in .json environ
                    separateenvlistpath = os.path.pathsep.join(rawconfig['environ'][env_name]['path+'])
                    separateenvlistpath = separateenvlistpath.split(os.path.pathsep)
                    separateenvlistpath.reverse()
                    # for path0 in separateenvlistpath:
                    #    print(path0)
                    find_flag = 0
                    for path0 in separateenvlistpath:
                        powershellexecfile = path0 + os.path.sep + cmd + cmd_suffix_powershell
                        if (os.path.exists(powershellexecfile)):
                            find_flag = 1
                            break
                    if (find_flag == 1):
                        break

                    # find in basic environ [+custom]
                    env = os.environ
                    find_flag = 0
                    for path0 in env["PATH"].split(os.path.pathsep):
                        powershellexecfile = path0 + os.path.sep + cmd + cmd_suffix_powershell
                        if (os.path.exists(powershellexecfile)):
                            find_flag = 1
                            break
                    if (find_flag == 1):
                        break

                    # none? a powershell command, or powershell command-line.
                    powershellexecfile = cmd
                    break

                # print(powershellexecfile)
                # wow
                # cmd_list.append(powershellexecfile + ' ' + params_string)
                if (str(powershellexecfile).__contains__(' ')):
                    if (os.path.isfile(powershellexecfile)):
                        powershellexecfile = powershellexecfile.replace('/', os.path.sep)
                        powershellexecfile = powershellexecfile.replace('\\', os.path.sep)
                        cmd_list.append(". \"%s\" @args" % powershellexecfile)
                    else:
                        cmd_list.append("%s" % powershellexecfile)
                else:
                    if (os.path.isfile(powershellexecfile)):
                        powershellexecfile = powershellexecfile.replace('/', os.path.sep)
                        powershellexecfile = powershellexecfile.replace('\\', os.path.sep)
                        cmd_list.append(". %s @args" % powershellexecfile)
                    else:
                        cmd_list.append("%s" % powershellexecfile)

        # append exit 0
        cmd_list.append(cmd_exit)
        # print( cmd_list )

        cmd_execute = "" + name + "_exec" + cmd_suffix
        with open(cmd_execute, "w", encoding=cmd_codec) as f:
            for line in cmd_list:
                f.write(line + cmd_return)
        # print(cmd_execute)

        if (debugswitch == '1'):
            print("IN: execute file: %s" % cmd_execute)
            for cmd in cmd_list:
                print(cmd)
            print("---------------------------")

        # if (plat == "Windows"):
        #    ""
        # else:
        #    os.system("chmod +x " + cmd_execute)

        cmd_list.clear()
        cmd_list.append(cmd_call + cmd_execute + ' ' + params_string + cmd_sep + ' ' + cmd_status)
        cmd_list.append(cmd_exit)

        # print (cmd_list)
        if (debugswitch == '1'):
            print("CMD: call execute file: %s" % cmd_execute)
            for cmd in cmd_list:
                print(cmd)
            print("---------------------------")

        return cmd_list, name

    def vc_python_createCmdList08(shellenvname=None, env_name=None, local=True, list0=[], params0=[]):
        cmd_list = []

        if (env_name is None):
            env_name = rawconfig['environ']['current']

        name = uuid.uuid4().__str__()
        name = name.split('-')[0]
        # print (name)

        plat = getplatform()
        if (plat == "Windows"):
            cmd_status = "echo pymake-command-status:%ERRORLEVEL%"
            cmd_sep = '&'
            cmd_codec = "ansi"
            if (getplatform_release() == "XP"):
                cmd_codec = None
            # but windows, it is \r\n, python helpping me?
            cmd_return = "\n"
            cmd_exit = 'exit /b %ERRORLEVEL%'
            cmd_suffix = ".bat"
            cmd_header = "@echo off"
            cmd_call = "call "
            # window close echo, close promot
            cmd_list.append(cmd_header)
            # os.system("type env_effect.bat > cmd_exec.bat")
            cmd_list.append("call %s_effect.bat" % name)
            if (shellenvname is not None):
                cmd_list.append('call \"%s_effect%s\"' % (shellenvname, cmd_suffix))
        else:
            cmd_status = "echo pymake-command-status:$?"
            cmd_sep = ';'
            cmd_suffix = ".sh"
            cmd_exit = 'exit $?'
            cmd_codec = "utf8"
            cmd_return = "\n"
            cmd_header = "#!/usr/bin/env bash"
            cmd_call = "./"
            cmd_list.append(cmd_header)
            cmd_list.append("source %s_effect.sh" % name)
            if (shellenvname is not None):
                cmd_list.append('source \"%s_effect%s\"' % (shellenvname, cmd_suffix))

        cmd_suffix_python = '.py'
        cmd_codec_python = "utf8"
        cmd_return_python = "\n"

        params_string = ""
        for param in params0:
            if (str(param).__contains__(' ')):
                params_string += '"' + param + '"' + ' '
            else:
                params_string += param + ' '
        # print(params_string)

        pythonexecfile = ''
        if (local is True):
            # fixed
            pythonexecfile = name + '_exec' + cmd_suffix_python
            with open(pythonexecfile, 'w', encoding=cmd_codec_python) as f:
                for cmd in list0:
                    f.write(cmd + cmd_return_python)
                    # print(1, pythonexecfile)
        else:
            # actually now has only one command.
            for cmd in list0:
                # warning: now pymake is in user setted workroot.
                if (str(cmd).endswith(cmd_suffix_python)):
                    cmd_suffix_python = ''

                pythonexecfile = ""
                while (True):
                    # find in current path [+workroot]
                    pythonexecfile = os.getcwd() + os.path.sep + cmd + cmd_suffix_python
                    # print(2, pythonexecfile)
                    if (os.path.exists(pythonexecfile)):
                        break
                    pythonexecfile = startupworkdirectory + os.path.sep + cmd + cmd_suffix_python
                    # print(2, pythonexecfile)
                    if (os.path.exists(pythonexecfile)):
                        break

                    # find in .json environ
                    separateenvlistpath = os.path.pathsep.join(rawconfig['environ'][env_name]['path+'])
                    separateenvlistpath = separateenvlistpath.split(os.path.pathsep)
                    separateenvlistpath.reverse()
                    # for path0 in separateenvlistpath:
                    #    print(path0)
                    find_flag = 0
                    for path0 in separateenvlistpath:
                        pythonexecfile = path0 + os.path.sep + cmd + cmd_suffix_python
                        # print(2, pythonexecfile)
                        if (os.path.exists(pythonexecfile)):
                            find_flag = 1
                            break
                    if (find_flag == 1):
                        break

                    # find in basic environ [custom+]
                    env = os.environ
                    find_flag = 0
                    for path0 in env["PATH"].split(os.path.pathsep):
                        pythonexecfile = path0 + os.path.sep + cmd + cmd_suffix_python
                        # print(2, pythonexecfile)
                        if (os.path.exists(pythonexecfile)):
                            find_flag = 1
                            break
                    if (find_flag == 1):
                        break

                    # none? a python command, or python command-line.
                    pythonexecfile = cmd
                    break

                    # print(2, pythonexecfile)
                    # cmd_list.append(pythonexecfile + ' ' + params_string)

        # print params.
        # print(3, pythonexecfile)

        # get python command.
        pycmd = ''
        if (plat == "Windows"):
            pycmd = 'python.exe'
        else:
            pycmd = 'python3'

        pycmd = which_command(env_name, pycmd)

        # print(pycmd)
        if (plat == "Windows"):
            if (pycmd is None):
                pycmd = 'py'
            elif (pycmd != 'py'):
                pycmd = 'python'
        else:
            pycmd = 'python3'

        if (os.path.isfile(pythonexecfile)):
            if (plat == "Windows"):
                cmd_list.append("call %s \"%s\" %s" % (pycmd, pythonexecfile, '%*'))
            else:
                cmd_list.append("%s \"%s\" %s" % (pycmd, pythonexecfile, '"$@"'))
        else:
            if (plat == "Windows"):
                cmd_list.append("call %s -c \"%s\" %s" % (pycmd, pythonexecfile, '%*'))
            else:
                cmd_list.append("%s -c \"%s\" %s" % (pycmd, pythonexecfile, '"$@"'))

        # append exit 0
        cmd_list.append(cmd_exit)

        cmd_execute = name + "_exec" + cmd_suffix
        with open(cmd_execute, "w", encoding=cmd_codec) as f:
            for line in cmd_list:
                f.write(line + cmd_return)
        # print(cmd_execute)

        if (debugswitch == '1'):
            print("IN: execute file: %s" % cmd_execute)
            for cmd in cmd_list:
                print(cmd)
            print("---------------------------")

        if (plat == "Windows"):
            ""
        else:
            os.system("chmod +x " + cmd_execute)

        cmd_list.clear()
        if (plat == "Windows"):
            cmd_list.append(cmd_header + ' ' + cmd_sep + ' ' + cmd_status)
            # cmd_list.append("call " + cmd_execute + cmd_sep + ' ' + cmd_status)
            cmd_list.append("call " + cmd_execute + ' ' + params_string + cmd_sep + ' ' + cmd_status)
        else:
            # cmd_list.append(cmd_header + ' ' + cmd_sep + ' ' + cmd_status)
            # cmd_list.append("./" + cmd_execute + cmd_sep + ' ' + cmd_status)
            cmd_list.append("./" + cmd_execute + ' ' + params_string + cmd_sep + ' ' + cmd_status)

        cmd_list.append(cmd_exit)

        if (debugswitch == '1'):
            print("CMD: call execute file: %s" % cmd_execute)
            for cmd in cmd_list:
                print(cmd)
            print("---------------------------")

        return cmd_list, name

    def vc_language_createCmdList08(shellenvname=None, suffix=None, encoding=None, env_name=None, local=True, list0=[], params0=[]):
        cmd_list = []

        name = uuid.uuid4().__str__()
        name = name.split('-')[0]
        # print (name)

        plat = getplatform()
        if (plat == "Windows"):
            cmd_status = "echo pymake-command-status:%ERRORLEVEL%"
            cmd_sep = '&'
            cmd_codec = "ansi"
            if (getplatform_release() == "XP"):
                cmd_codec = None
            # but windows, it is \r\n, python helpping me?
            cmd_return = "\n"
            cmd_exit = 'exit /b %ERRORLEVEL%'
            cmd_suffix = ".bat"
            cmd_header = "@echo off"
            cmd_call = "call "
            # window close echo, close promot
            cmd_list.append(cmd_header)
            # os.system("type env_effect.bat > cmd_exec.bat")
            cmd_list.append("call %s_effect.bat" % name)
            if (shellenvname is not None):
                cmd_list.append('call \"%s_effect%s\"' % (shellenvname, cmd_suffix))
        else:
            cmd_status = "echo pymake-command-status:$?"
            cmd_sep = ';'
            cmd_suffix = ".sh"
            cmd_exit = 'exit $?'
            cmd_codec = "utf8"
            cmd_return = "\n"
            cmd_header = "#!/usr/bin/env bash"
            cmd_call = "./"
            cmd_list.append(cmd_header)
            cmd_list.append("source %s_effect.sh" % name)
            if (shellenvname is not None):
                cmd_list.append('source \"%s_effect%s\"' % (shellenvname, cmd_suffix))

        if (env_name is None):
            env_name = rawconfig['environ']['current']
        if (env_name == "current"):
            env_name = rawconfig['environ']['current']

        cmd_suffix_language = cmd_suffix
        cmd_codec_language = cmd_codec
        cmd_return_language = cmd_return

        if (suffix is not None):
            cmd_suffix_language = suffix
        if (encoding is not None):
            cmd_codec_language = encoding

        list1 = []
        # for current_var in str(args['<command-param>']).split():
        #    list1.append(current_var)
        if (params0.__len__() > 0):
            current_var = params0[0]
            list1.append(current_var)
            params0.pop(0)
        # print(list1)

        params_string = ""
        for param in params0:
            if (str(param).__contains__(' ')):
                params_string += '"' + param + '"' + ' '
            else:
                params_string += param + ' '
        # print(params_string)

        local2 = True
        if (list1.__len__() > 0):
            current_var = list1[0]
            if (current_var in rawconfig['command']):
                local2 = True
            else:
                local2 = False

        languageparams = ''
        # actually only one param.
        if (local2 is True):
            for param1 in list1:
                languageparams += param1
        else:
            for param1 in list1:
                # warning: now pymake is in user setted workroot.

                languageparams = ""
                while (True):
                    # find in current path [+--workroot]
                    languageparams = os.getcwd() + os.path.sep + param1
                    # print(2, languageparams)
                    if (os.path.exists(languageparams)):
                        break
                    languageparams = startupworkdirectory + os.path.sep + param1
                    # print(2, languageparams)
                    if (os.path.exists(languageparams)):
                        break

                    # find in .json environ
                    separateenvlistpath = os.path.pathsep.join(rawconfig['environ'][env_name]['path+'])
                    separateenvlistpath = separateenvlistpath.split(os.path.pathsep)
                    separateenvlistpath.reverse()
                    # for path0 in separateenvlistpath:
                    #    print(path0)
                    find_flag = 0
                    for path0 in separateenvlistpath:
                        languageparams = path0 + os.path.sep + param1
                        # print(2, languageparams)
                        if (os.path.exists(languageparams)):
                            find_flag = 1
                            break
                    if (find_flag == 1):
                        break

                    # find in basic environ [custom+]
                    env = os.environ
                    find_flag = 0
                    for path0 in env["PATH"].split(os.path.pathsep):
                        languageparams = path0 + os.path.sep + param1
                        # print(2, languageparams)
                        if (os.path.exists(languageparams)):
                            find_flag = 1
                            break
                    if (find_flag == 1):
                        break

                    # none? a language command, or language command-line.
                    languageparams = param1
                    break

                    # print(2, languageparams)
                    # cmd_list.append(languageparams + ' ' + params_string)

        if (list1.__len__() > 0):
            ''
            if (local2 is True):
                ''
                current_var = env_name
                local_command = raw_command(current_var)
                dict0 = copy.deepcopy(local_command)

                inside_name = name + '_2'
                languageparams = inside_name + '_exec' + cmd_suffix_language

                with open(languageparams, 'w', encoding=cmd_codec_language) as f:
                    for cmd1 in list1:
                        for cmd in dict0[cmd1]:
                            f.write(cmd + cmd_return_language)

                            # print(1, cmd_suffix_language)
                            # print(1, cmd_return_language)
                            # print(1, cmd_codec_language)
                            # print(1, languageparams)
            else:
                ''

        # print(3, languageparams)
        if (list1.__len__() > 0):
            if (local2 is True):
                params_string = languageparams + ' ' + params_string
            else:
                if (str(languageparams).__contains__(' ')):
                    params_string = '"' + languageparams + '"' + ' ' + params_string
                else:
                    params_string = languageparams + ' ' + params_string
        # print(params_string)

        languageexecfile = ''
        if (local is True):
            # fixed
            # inside_name = name
            # inside_name = hex( int( inside_name, 16 ) + 1).split('x')[1]
            inside_name = name + '_1'
            # print(inside_name)
            languageexecfile = inside_name + '_exec' + cmd_suffix
            with open(languageexecfile, 'w', encoding=cmd_codec) as f:
                for cmd in list0:
                    f.write(cmd + cmd_return)
                    # print(1, languageexecfile)
        else:
            languageexecfile = ''
            # actually now has only one command.
            for cmd in list0:
                # actually this is a command.
                if (str(cmd).__contains__(' ')):
                    pycmd = which_command(env_name, str(cmd))
                    # print(cmd)
                    # print(pycmd)
                    if (pycmd is not None and os.path.isfile(pycmd)):
                        languageexecfile = '"' + cmd + '"'
                    else:
                        languageexecfile = cmd
                else:
                    languageexecfile = cmd

        # print(3, languageexecfile)
        if (plat == "Windows"):
            cmd_list.append("call %s %s" % (languageexecfile, '%*'))
        else:
            if (languageexecfile.endswith('.sh')):
                cmd_list.append("sh %s %s" % (languageexecfile, '"$@"'))
            else:
                cmd_list.append("%s %s" % (languageexecfile, '"$@"'))

        # append exit 0
        cmd_list.append(cmd_exit)

        cmd_execute = name + "_exec" + cmd_suffix
        with open(cmd_execute, "w", encoding=cmd_codec) as f:
            for line in cmd_list:
                f.write(line + cmd_return)

        if (debugswitch == '1'):
            print("IN: execute file: %s" % cmd_execute)
            for cmd in cmd_list:
                print(cmd)
            print("---------------------------")

        if (plat == "Windows"):
            ""
        else:
            os.system("chmod +x " + cmd_execute)

        cmd_list.clear()
        if (plat == "Windows"):
            cmd_list.append(cmd_header + ' ' + cmd_sep + ' ' + cmd_status)
            # cmd_list.append("call " + cmd_execute + cmd_sep + ' ' + cmd_status)
            cmd_list.append("call " + cmd_execute + ' ' + params_string + cmd_sep + ' ' + cmd_status)
        else:
            # cmd_list.append(cmd_header + ' ' + cmd_sep + ' ' + cmd_status)
            # cmd_list.append("./" + cmd_execute + cmd_sep + ' ' + cmd_status)
            cmd_list.append("./" + cmd_execute + ' ' + params_string + cmd_sep + ' ' + cmd_status)

        cmd_list.append(cmd_exit)

        if (debugswitch == '1'):
            print("CMD: call execute file: %s" % cmd_execute)
            for cmd in cmd_list:
                print(cmd)
            print("---------------------------")

        return cmd_list, name, cmd_suffix, cmd_suffix_language

    # vc json export function
    def vc_json_export(dict0=None, env_name=None, file_name=None):
        if (dict0 is None):
            return "", "", ""

        # select env
        current_var = rawconfig['environ']['current']
        if (env_name is not None):
            current_var = env_name

        cmd_suffix = ".json"
        cmd_codec = 'utf8'
        if (getplatform_release() == "XP"):
            cmd_codec = None
        # but windows, it is \r\n, python helpping me?
        cmd_return = "\n"
        cmd_header = "#!/usr/bin/env bash" + cmd_return
        env_set = ''

        # export effect env
        cmd_effect = 'vcenv'
        if (file_name is not None):
            cmd_effect = file_name
        cmd_effect += cmd_suffix

        if (os.path.exists(cmd_effect) is False):
            tempdict = {
                'environ': {

                }
            }
            writeJsonData(cmd_effect, tempdict)

        vcdict = readJsonData(cmd_effect)
        if (vcdict.__contains__('environ') is False):
            vcdict['environ'] = {}
        vcdict['environ'][current_var] = dict0
        writeJsonData(cmd_effect, vcdict)

        return cmd_effect

    # vc powershell export function
    def vc_powershell_export(dict0=None, env_name=None, file_name=None):
        if (dict0 is None):
            return "", "", ""

        # select env
        current_var = rawconfig['environ']['current']
        if (env_name is not None):
            current_var = env_name

        cmd_suffix = ".ps1"
        cmd_codec = 'ansi'
        if (getplatform_release() == "XP"):
            cmd_codec = None
        # but windows, it is \r\n, python helpping me?
        cmd_return = "\n"
        cmd_header = "#!/usr/bin/env bash" + cmd_return
        env_set = ''

        # export effect env
        cmd_effect = 'env'
        if (file_name is not None):
            cmd_effect = "" + file_name
        cmd_effect += '_effect' + cmd_suffix

        # export path
        lines = ""
        for (key) in dict0["path+"]:
            lines += ("if ( !$env:Path.Contains(\"%s;\" ) ) { $env:Path = $env:Path.Insert(0, \"%s;\") }" % (key, key)) + cmd_return

        # export var
        for (key, value) in dict0.items():
            if (key == 'path+'):
                continue
            lines += ("${env:" + key + '} = \'' + value + '\'' + cmd_return)

        # print(lines.split('\n'))
        with open(cmd_effect, 'w', encoding=cmd_codec) as f:
            # f.write(cmd_header)
            f.write(lines)

        # export unset env
        cmd_unset = 'env'
        if (file_name is not None):
            cmd_unset = "" + file_name
        cmd_unset += '_unset' + cmd_suffix

        # export unset path
        lines = ""
        for (key) in dict0["path+"]:
            # lines += ("$env:Path = $env:Path.Replace(\"%s;\", \"\")" % key) + cmd_return
            lines += ("if ( $env:Path.Contains(\"%s;\" ) ) { $env:Path = $env:Path.Replace(\"%s;\", \"\") }" % (key, key)) + cmd_return
        # export unset var
        for (key, value) in dict0.items():
            if (key == 'path+'):
                continue
            lines += ("${env:%s} = \"\"" % key) + cmd_return

        with open(cmd_unset, 'w', encoding=cmd_codec) as f:
            # f.write(cmd_header)
            f.write(lines)

        return cmd_effect, cmd_unset

    def vc_powershell_export2(dict0=None, env_name=None, file_name=None):
        if (dict0 is None):
            return "", "", ""

        # select env
        current_var = rawconfig['environ']['current']
        if (env_name is not None):
            current_var = env_name

        cmd_suffix = ".ps1"
        cmd_codec = 'ansi'
        if (getplatform_release() == "XP"):
            cmd_codec = None
        # but windows, it is \r\n, python helpping me?
        cmd_return = "\n"
        cmd_header = "#!/usr/bin/env bash" + cmd_return
        env_set = ''

        # export effect env
        cmd_effect = 'env'
        if (file_name is not None):
            cmd_effect = "" + file_name
        cmd_effect += '_effect' + cmd_suffix

        lines = ""
        # +system
        if (args['-s'] or args['--system'] is True):
            # export path
            # print(envcustomlistrawpaths)
            for (key) in pymakesystemenviron['PATH'].split(os.path.pathsep):
                lines += ("if ( !$env:Path.Contains(\"%s;\" ) ) { $env:Path = $env:Path.Insert(0, \"%s;\") }" % (key, key)) + cmd_return

            # export var
            for (key, value) in pymakesystemenviron.items():
                if (key == 'path+'):
                    continue
                if (str(key).lower() == "path"):
                    continue
                lines += ("${env:" + key + '} = \'' + value + '\'' + cmd_return)
        else:
            ''

        # +local
        if (args['-l'] or args['--local'] is True):
            # export path
            for (key) in localenv['path+']:
                lines += ("if ( !$env:Path.Contains(\"%s;\" ) ) { $env:Path = $env:Path.Insert(0, \"%s;\") }" % (key, key)) + cmd_return

            # export var
            for (key, value) in localenv.items():
                if (key == 'path+'):
                    continue
                lines += ("${env:" + key + '} = \'' + value + '\'' + cmd_return)
        else:
            ''

        # +custom
        if (args['-c'] or args['--custom'] is True):
            # export path
            # print(envcustomlistrawpaths)
            for (key) in envcustomlistrawpaths:
                lines += ("if ( !$env:Path.Contains(\"%s;\" ) ) { $env:Path = $env:Path.Insert(0, \"%s;\") }" % (key, key)) + cmd_return

            # export var
            for (key, value) in envcustomlistrawvars.items():
                if (key == 'path+'):
                    continue
                lines += ("${env:" + key + '} = \'' + value + '\'' + cmd_return)
        else:
            ''

        # export path
        for (key) in dict0["path+"]:
            lines += ("if ( !$env:Path.Contains(\"%s;\" ) ) { $env:Path = $env:Path.Insert(0, \"%s;\") }" % (key, key)) + cmd_return

        # export var
        for (key, value) in dict0.items():
            if (key == 'path+'):
                continue
            lines += ("${env:" + key + '} = \'' + value + '\'' + cmd_return)

        # print(lines.split('\n'))
        with open(cmd_effect, 'w', encoding=cmd_codec) as f:
            # f.write(cmd_header)
            f.write(lines)

        # export unset env
        cmd_unset = 'env'
        if (file_name is not None):
            cmd_unset = "" + file_name
        cmd_unset += '_unset' + cmd_suffix

        lines = ""
        # +system
        if (args['-s'] or args['--system'] is True):
            # export unset path
            for (key) in pymakesystemenviron['PATH'].split(os.path.pathsep):
                lines += ("if ( $env:Path.Contains(\"%s;\" ) ) { $env:Path = $env:Path.Replace(\"%s;\", \"\") }" % (key, key)) + cmd_return

            # export unset var
            for (key, value) in pymakesystemenviron.items():
                if (key == 'path+'):
                    continue
                if (str(key).lower() == "path"):
                    continue
                lines += ("${env:%s} = \"\"" % key) + cmd_return
        else:
            ''

        # +local
        if (args['-l'] or args['--local'] is True):
            # export unset path
            for (key) in localenv['path+']:
                lines += ("if ( $env:Path.Contains(\"%s;\" ) ) { $env:Path = $env:Path.Replace(\"%s;\", \"\") }" % (key, key)) + cmd_return

            # export unset var
            for (key, value) in localenv.items():
                if (key == 'path+'):
                    continue
                lines += ("${env:%s} = \"\"" % key) + cmd_return
        else:
            ''

        # +custom
        if (args['-c'] or args['--custom'] is True):
            # export unset path
            for (key) in envcustomlistrawpaths:
                lines += ("if ( $env:Path.Contains(\"%s;\" ) ) { $env:Path = $env:Path.Replace(\"%s;\", \"\") }" % (key, key)) + cmd_return

            # export unset var
            for (key, value) in envcustomlistrawvars.items():
                if (key == 'path+'):
                    continue
                lines += ("${env:%s} = \"\"" % key) + cmd_return
        else:
            ''

        # export unset path
        for (key) in dict0["path+"]:
            # lines += ("$env:Path = $env:Path.Replace(\"%s;\", \"\")" % key) + cmd_return
            lines += ("if ( $env:Path.Contains(\"%s;\" ) ) { $env:Path = $env:Path.Replace(\"%s;\", \"\") }" % (key, key)) + cmd_return

        # export unset var
        for (key, value) in dict0.items():
            if (key == 'path+'):
                continue
            lines += ("${env:%s} = \"\"" % key) + cmd_return

        with open(cmd_unset, 'w', encoding=cmd_codec) as f:
            # f.write(cmd_header)
            f.write(lines)

        return cmd_effect, cmd_unset

    # vc export function
    # dict0: VC effect env
    # env_name: separate env
    # file_name: script name
    def vc_export(dict0=None, env_name=None, file_name=None):
        if (dict0 is None):
            return "", "", ""

        # select env
        current_var = rawconfig['environ']['current']
        if (env_name is not None):
            current_var = env_name

        plat = getplatform()
        if (plat == "Windows"):
            cmd_suffix = ".bat"
            cmd_codec = "ansi"
            if (getplatform_release() == "XP"):
                cmd_codec = None
            cmd_return = "\n"
            cmd_header = "@echo off" + cmd_return
            env_set = 'set '
        else:
            cmd_suffix = ".sh"
            cmd_codec = "utf8"
            cmd_return = "\n"
            cmd_header = "#!/usr/bin/env bash" + cmd_return
            env_set = 'export '

        # export effect env
        cmd_effect = 'env'
        if (file_name is not None):
            cmd_effect = file_name
        cmd_effect += '_effect' + cmd_suffix

        # export path
        lines = ""
        for (key) in dict0["path+"]:
            if (plat == "Windows"):
                lines += (env_set + 'PATH=' + key + os.path.pathsep + '%PATH%' + cmd_return)
            else:
                lines += (env_set + 'PATH="' + key + '"' + os.path.pathsep + '$PATH' + cmd_return)

        # export var
        for (key, value) in dict0.items():
            if (key == 'path+'):
                continue
            if (plat == "Windows"):
                lines += (env_set + key + '=' + value + cmd_return)
            else:
                lines += (env_set + key + '=\"' + value + '\"' + cmd_return)

        with open(cmd_effect, 'w', encoding=cmd_codec) as f:
            f.write(cmd_header)
            f.write(lines)

        # export unset env
        cmd_unset = 'env'
        if (file_name is not None):
            cmd_unset = file_name
        cmd_unset += '_unset' + cmd_suffix

        # export unset path
        lines = ""
        for (key) in dict0["path+"]:
            if (plat == "Windows"):
                lines += (env_set + 'PATH=%PATH:' + key + ';=%' + cmd_return)
            else:
                lines += (env_set + 'PATH=$(' + 'echo ${PATH//' + key.replace('/', '\/') + ':/})' + cmd_return)

        # export unset var
        for (key, value) in dict0.items():
            if (key == 'path+'):
                continue
            if (plat == "Windows"):
                lines += ('set ' + key + '=' + cmd_return)
            else:
                lines += ('unset ' + key + cmd_return)

        with open(cmd_unset, 'w', encoding=cmd_codec) as f:
            f.write(cmd_header)
            f.write(lines)

        if (plat == "Windows"):
            ""
        else:
            os.system("chmod +x " + cmd_effect)
            os.system("chmod +x " + cmd_unset)

        # return file name
        return cmd_effect, cmd_unset

    def vc_export2(dict0=None, env_name=None, file_name=None):
        if (dict0 is None):
            return "", "", ""

        # select env
        current_var = rawconfig['environ']['current']
        if (env_name is not None):
            current_var = env_name

        plat = getplatform()
        if (plat == "Windows"):
            cmd_suffix = ".bat"
            cmd_codec = "ansi"
            if (getplatform_release() == "XP"):
                cmd_codec = None
            cmd_return = "\n"
            cmd_header = "@echo off" + cmd_return
            env_set = 'set '
        else:
            cmd_suffix = ".sh"
            cmd_codec = "utf8"
            cmd_return = "\n"
            cmd_header = "#!/usr/bin/env bash" + cmd_return
            env_set = 'export '

        # export effect env
        cmd_effect = 'env'
        if (file_name is not None):
            cmd_effect = file_name
        cmd_effect += '_effect' + cmd_suffix

        lines = ""

        # +system
        if (args['-s'] or args['--system'] is True):
            # export path
            # print(envcustomlistrawpaths)
            for (key) in pymakesystemenviron['PATH'].split(os.path.pathsep):
                if (plat == "Windows"):
                    lines += (env_set + 'PATH=' + key + os.path.pathsep + '%PATH%' + cmd_return)
                else:
                    lines += (env_set + 'PATH="' + key + '"' + os.path.pathsep + '$PATH' + cmd_return)

            # export var
            for (key, value) in pymakesystemenviron.items():
                if (key == 'path+'):
                    continue
                if (str(key).lower() == "path"):
                    continue
                if (plat == "Windows"):
                    lines += (env_set + key + '=' + value + cmd_return)
                else:
                    lines += (env_set + key + '=\"' + value + '\"' + cmd_return)
        else:
            ''

        # +local
        if (args['-l'] or args['--local'] is True):
            # export path
            for (key) in localenv['path+']:
                if (plat == "Windows"):
                    lines += (env_set + 'PATH=' + key + os.path.pathsep + '%PATH%' + cmd_return)
                else:
                    lines += (env_set + 'PATH="' + key + '"' + os.path.pathsep + '$PATH' + cmd_return)

            # export var
            for (key, value) in localenv.items():
                if (key == 'path+'):
                    continue
                if (plat == "Windows"):
                    lines += (env_set + key + '=' + value + cmd_return)
                else:
                    lines += (env_set + key + '=\"' + value + '\"' + cmd_return)
        else:
            ''

        # +custom
        if (args['-c'] or args['--custom'] is True):
            # export path
            # print(envcustomlistrawpaths)
            for (key) in envcustomlistrawpaths:
                if (plat == "Windows"):
                    lines += (env_set + 'PATH=' + key + os.path.pathsep + '%PATH%' + cmd_return)
                else:
                    lines += (env_set + 'PATH="' + key + '"' + os.path.pathsep + '$PATH' + cmd_return)

            # export var
            for (key, value) in envcustomlistrawvars.items():
                if (key == 'path+'):
                    continue
                if (plat == "Windows"):
                    lines += (env_set + key + '=' + value + cmd_return)
                else:
                    lines += (env_set + key + '=\"' + value + '\"' + cmd_return)
        else:
            ''

        # export path
        for (key) in dict0["path+"]:
            if (plat == "Windows"):
                lines += (env_set + 'PATH=' + key + os.path.pathsep + '%PATH%' + cmd_return)
            else:
                lines += (env_set + 'PATH="' + key + '"' + os.path.pathsep + '$PATH' + cmd_return)

        # export var
        for (key, value) in dict0.items():
            if (key == 'path+'):
                continue
            if (plat == "Windows"):
                lines += (env_set + key + '=' + value + cmd_return)
            else:
                lines += (env_set + key + '=\"' + value + '\"' + cmd_return)

        with open(cmd_effect, 'w', encoding=cmd_codec) as f:
            f.write(cmd_header)
            f.write(lines)

        # export unset env
        cmd_unset = 'env'
        if (file_name is not None):
            cmd_unset = file_name
        cmd_unset += '_unset' + cmd_suffix

        lines = ""

        # +system
        if (args['-s'] or args['--system'] is True):
            # export unset path
            for (key) in pymakesystemenviron['PATH'].split(os.path.pathsep):
                if (plat == "Windows"):
                    lines += (env_set + 'PATH=%PATH:' + key + ';=%' + cmd_return)
                else:
                    lines += (env_set + 'PATH=$(' + 'echo ${PATH//' + key.replace('/', '\/') + ':/})' + cmd_return)

            # export unset var
            for (key, value) in pymakesystemenviron.items():
                if (key == 'path+'):
                    continue
                if (str(key).lower() == "path"):
                    continue
                if (plat == "Windows"):
                    lines += ('set ' + key + '=' + cmd_return)
                else:
                    lines += ('unset ' + key + cmd_return)
        else:
            ''

        # +local
        if (args['-l'] or args['--local'] is True):
            # export unset path
            for (key) in localenv['path+']:
                if (plat == "Windows"):
                    lines += (env_set + 'PATH=%PATH:' + key + ';=%' + cmd_return)
                else:
                    lines += (env_set + 'PATH=$(' + 'echo ${PATH//' + key.replace('/', '\/') + ':/})' + cmd_return)

            # export unset var
            for (key, value) in localenv.items():
                if (key == 'path+'):
                    continue
                if (plat == "Windows"):
                    lines += ('set ' + key + '=' + cmd_return)
                else:
                    lines += ('unset ' + key + cmd_return)
        else:
            ''

        # +custom
        if (args['-c'] or args['--custom'] is True):
            # export unset path
            for (key) in envcustomlistrawpaths:
                if (plat == "Windows"):
                    lines += (env_set + 'PATH=%PATH:' + key + ';=%' + cmd_return)
                else:
                    lines += (env_set + 'PATH=$(' + 'echo ${PATH//' + key.replace('/', '\/') + ':/})' + cmd_return)

            # export unset var
            for (key, value) in envcustomlistrawvars.items():
                if (key == 'path+'):
                    continue
                if (plat == "Windows"):
                    lines += ('set ' + key + '=' + cmd_return)
                else:
                    lines += ('unset ' + key + cmd_return)
        else:
            ''

        # export unset path
        for (key) in dict0["path+"]:
            if (plat == "Windows"):
                lines += (env_set + 'PATH=%PATH:' + key + ';=%' + cmd_return)
            else:
                lines += (env_set + 'PATH=$(' + 'echo ${PATH//' + key.replace('/', '\/') + ':/})' + cmd_return)

        # export unset var
        for (key, value) in dict0.items():
            if (key == 'path+'):
                continue
            if (plat == "Windows"):
                lines += ('set ' + key + '=' + cmd_return)
            else:
                lines += ('unset ' + key + cmd_return)

        with open(cmd_unset, 'w', encoding=cmd_codec) as f:
            f.write(cmd_header)
            f.write(lines)

        if (plat == "Windows"):
            ""
        else:
            os.system("chmod +x " + cmd_effect)
            os.system("chmod +x " + cmd_unset)

        # return file name
        return cmd_effect, cmd_unset

    # vc settings function
    def vc_settings(env_name=None):
        current_env = env_name
        if (env_name is None):
            current_env = rawconfig['environ']['current']

        print("source file: %s" % sourceconfigfile)
        vcvarslist = [
            'vcvarsall-1998',
            'vcvarsall-2003',
            'vcvarsall-2005',
            'vcvarsall-2008',
            'vcvarsall-2010',
            'vcvarsall-2012',
            'vcvarsall-2013',
            'vcvarsall-2015',
            'vcvarsall-2017',
            'vcvarsall-2019',
            'vcvarsall-20XX'
        ]

        '''
        vcvarslist = []
        for key, vcconfig in rawconfig['path-assemblage'].items():
            if(str(key).startswith('vcvarsall')):
                vcvarslist.append(key)
        '''

        print('path-assemblage:')
        for key in vcvarslist:
            if (rawconfig['path-assemblage'].__contains__(key) is False):
                rawconfig['path-assemblage'][key] = "<CAN SET>"
            print('  "%s": "%s"' % (key, rawconfig['path-assemblage'][key]))

        current_vcvarsall = 'vcvarsall'
        current_vcvarsallparam = 'vcvarsallparam'
        print('system env:')
        if (pymakesystemenviron.__contains__(current_vcvarsall) is False):
            pymakesystemenviron[current_vcvarsall] = "<CAN SET>"
        print('  "%s"     : "%s"' % (current_vcvarsall, pymakesystemenviron[current_vcvarsall]))
        if (pymakesystemenviron.__contains__(current_vcvarsallparam) is False):
            pymakesystemenviron[current_vcvarsallparam] = "<CAN SET>"
        print('  "%s": "%s"' % (current_vcvarsallparam, pymakesystemenviron[current_vcvarsallparam]))

        print('custom env:')
        if (envcustomlistrawvars.__contains__(current_vcvarsall) is False):
            envcustomlistrawvars[current_vcvarsall] = "<CAN SET>"
        print('  "%s"     : "%s"' % (current_vcvarsall, envcustomlistrawvars[current_vcvarsall]))
        if (envcustomlistrawvars.__contains__(current_vcvarsallparam) is False):
            envcustomlistrawvars[current_vcvarsallparam] = "<CAN SET>"
        print('  "%s": "%s"' % (current_vcvarsallparam, envcustomlistrawvars[current_vcvarsallparam]))

        print('env %s:' % current_env)
        if (rawconfig['environ'][current_env].__contains__(current_vcvarsall) is False):
            rawconfig['environ'][current_env][current_vcvarsall] = "<CAN SET>"
        print('  "%s"     : "%s"' % (current_vcvarsall, rawconfig['environ'][current_env][current_vcvarsall]))
        if (rawconfig['environ'][current_env].__contains__(current_vcvarsallparam) is False):
            rawconfig['environ'][current_env][current_vcvarsallparam] = "<CAN SET>"
        print('  "%s": "%s"' % (current_vcvarsallparam, rawconfig['environ'][current_env][current_vcvarsallparam]))
        return

    # vc settings2 function
    def vc_settings2(env_name = None):
        print("source root: %s" % sourceroot)
        current_vcvarsall = 'vcvarsall'
        current_vcvarsallparam = 'vcvarsallparam'
        print('system env:')
        if (pymakesystemenviron.__contains__(current_vcvarsall) is False):
            pymakesystemenviron[current_vcvarsall] = "<CAN SET>"
        print('  "%s"     : "%s"' % (current_vcvarsall, pymakesystemenviron[current_vcvarsall]))
        if (pymakesystemenviron.__contains__(current_vcvarsallparam) is False):
            pymakesystemenviron[current_vcvarsallparam] = "<CAN SET>"
        print('  "%s": "%s"' % (current_vcvarsallparam, pymakesystemenviron[current_vcvarsallparam]))

        print('custom env:')
        if (envcustomlistrawvars.__contains__(current_vcvarsall) is False):
            envcustomlistrawvars[current_vcvarsall] = "<CAN SET>"
        print('  "%s"     : "%s"' % (current_vcvarsall, envcustomlistrawvars[current_vcvarsall]))
        if (envcustomlistrawvars.__contains__(current_vcvarsallparam) is False):
            envcustomlistrawvars[current_vcvarsallparam] = "<CAN SET>"
        print('  "%s": "%s"' % (current_vcvarsallparam, envcustomlistrawvars[current_vcvarsallparam]))

        vcvarslist = []
        for key, vcconfig in rawconfig['path-assemblage'].items():
            if(str(key).startswith('vcvarsall')):
                vcvarslist.append(key)
            if(str(key).startswith('vcbuildtools')):
                vcvarslist.append(key)
        vcvarslist.append('vcvarsall-20XX')

        print("source file: %s" % sourceconfigfile)
        print('path-assemblage:')
        for key in vcvarslist:
            if (rawconfig['path-assemblage'].__contains__(key) is False):
                rawconfig['path-assemblage'][key] = "<CAN SET>"
            print('  %-24s: %s' % (key, rawconfig['path-assemblage'][key]))

        print('environ:')
        print('  %-30s %-30s %-30s %s' % ('[env]', '[status]', '[vcvarsall]', '[vcvarsallparam]'))
        set_content = '<WAIT SET>'
        set_content2 = '<CAN SET>'
        set_content3 = '<INSTALLED>'
        set_content4 = '<NEED INSTALL>'
        for (k, v) in rawconfig['environ'].items():
            current_env = k
            if (current_env == 'current'):
                continue
            status = set_content
            vcvarsall = set_content2
            vcvarsallparam = set_content2
            if (rawconfig['environ'].__contains__(current_env) is True):
                if (rawconfig['environ'][current_env].__contains__(current_vcvarsall) is True):
                    if(os.path.exists(rawconfig['environ'][current_env][current_vcvarsall])):
                        status = set_content3
                    else:
                        status = set_content4
            if (rawconfig['environ'][current_env].__contains__(current_vcvarsall) is True):
                vcvarsall = rawconfig['environ'][current_env][current_vcvarsall]
            if (rawconfig['environ'][current_env].__contains__(current_vcvarsallparam) is True):
                vcvarsallparam = rawconfig['environ'][current_env][current_vcvarsallparam]
            if (current_env == rawconfig['environ']['current']):
                print(Fore.LIGHTMAGENTA_EX + '  %-30s %-30s %-30s %s' % (current_env, status, vcvarsall, vcvarsallparam))
                continue
            print('  %-30s %-30s %-30s %s' % (current_env, status, vcvarsall, vcvarsallparam))
        return

    # vc command

    # vc init command

    # ===============================================================================================
    # PYMAKE LANGUAGE CCVP COMMAND INITIAL CODE
    # --------------------

    # export2 command

    # powershell export env function
    def powershell_environ_export(env_name=None, file_name=None):
        # select env
        current_var = rawconfig['environ']['current']
        if (env_name is not None):
            current_var = env_name
        dict0 = copy.deepcopy(rawconfig['environ'][current_var])

        cmd_suffix = ".ps1"
        cmd_codec = 'ansi'
        if (getplatform_release() == "XP"):
            cmd_codec = None
        # but windows, it is \r\n, python helpping me?
        cmd_return = "\n"
        cmd_header = "#!/usr/bin/env bash" + cmd_return
        env_set = ''

        # export effect env
        cmd_effect = 'env'
        if (file_name is not None):
            cmd_effect = "" + file_name
        cmd_effect += '_effect' + cmd_suffix

        # export path
        lines = ""
        for (key) in dict0["path+"]:
            lines += ("if ( !$env:Path.Contains(\"%s;\" ) ) { $env:Path = $env:Path.Insert(0, \"%s;\") }" % (key, key)) + cmd_return

        # export var
        for (key, value) in dict0.items():
            if (key == 'path+'):
                continue
            lines += ("${env:" + key + '} = \'' + value + '\'' + cmd_return)

        # print(lines.split('\n'))
        with open(cmd_effect, 'w', encoding=cmd_codec) as f:
            # f.write(cmd_header)
            f.write(lines)

        # export unset env
        cmd_unset = 'env'
        if (file_name is not None):
            cmd_unset = "" + file_name
        cmd_unset += '_unset' + cmd_suffix

        # export unset path
        lines = ""
        for (key) in dict0["path+"]:
            # lines += ("$env:Path = $env:Path.Replace(\"%s;\", \"\")" % key) + cmd_return
            lines += ("if ( $env:Path.Contains(\"%s;\" ) ) { $env:Path = $env:Path.Replace(\"%s;\", \"\") }" % (key, key)) + cmd_return
        # export unset var
        for (key, value) in dict0.items():
            if (key == 'path+'):
                continue
            lines += ("${env:%s} = \"\"" % key) + cmd_return

        with open(cmd_unset, 'w', encoding=cmd_codec) as f:
            # f.write(cmd_header)
            f.write(lines)

        return current_var, cmd_effect, cmd_unset

    # powershell [windows, unix]
    def createCmdList03(env_name=None, local=True, list0=[], params0=[]):

        cmd_list = []

        if (env_name is None):
            env_name = rawconfig['environ']['current']

        name = uuid.uuid4().__str__()
        name = name.split('-')[0]
        # print (name)

        # plat = getplatform()

        cmd_status = "echo pymake-command-status:$LASTEXITCODE"
        cmd_sep = ';'
        cmd_suffix = ".ps1"
        cmd_suffix_powershell = cmd_suffix
        cmd_exit = 'exit $LASTEXITCODE'
        cmd_codec = 'ansi'
        if (getplatform_release() == "XP"):
            cmd_codec = None
        # but windows, it is \r\n, python helpping me?
        cmd_return = "\n"
        cmd_header = "#!/usr/bin/env bash"
        cmd_call = "./"
        # cmd_list.append(cmd_header)
        cmd_list.append("./%s_effect%s" % (name, cmd_suffix))

        params_string = ""
        for param in params0:
            if (str(param).__contains__(' ')):
                params_string += '"' + param + '"' + ' '
            else:
                params_string += param + ' '
        # print(params_string)

        if (local is True):
            for cmd in list0:
                cmd_list.append(cmd)
        else:
            # actually now has only one command.
            for cmd in list0:
                # warning: now pymake is in user setted workroot.
                if (str(cmd).endswith(cmd_suffix_powershell)):
                    cmd_suffix_powershell = ''

                powershellexecfile = ""
                while (True):
                    # find in current path [+workroot]
                    powershellexecfile = os.getcwd() + os.path.sep + cmd + cmd_suffix_powershell
                    if (os.path.exists(powershellexecfile)):
                        break
                    powershellexecfile = startupworkdirectory + os.path.sep + cmd + cmd_suffix_powershell
                    if (os.path.exists(powershellexecfile)):
                        break

                    # find in .json environ
                    separateenvlistpath = os.path.pathsep.join(rawconfig['environ'][env_name]['path+'])
                    separateenvlistpath = separateenvlistpath.split(os.path.pathsep)
                    separateenvlistpath.reverse()
                    # for path0 in separateenvlistpath:
                    #    print(path0)
                    find_flag = 0
                    for path0 in separateenvlistpath:
                        powershellexecfile = path0 + os.path.sep + cmd + cmd_suffix_powershell
                        if (os.path.exists(powershellexecfile)):
                            find_flag = 1
                            break
                    if (find_flag == 1):
                        break

                    # find in basic environ [+custom]
                    env = os.environ
                    find_flag = 0
                    for path0 in env["PATH"].split(os.path.pathsep):
                        powershellexecfile = path0 + os.path.sep + cmd + cmd_suffix_powershell
                        if (os.path.exists(powershellexecfile)):
                            find_flag = 1
                            break
                    if (find_flag == 1):
                        break

                    # none? a powershell command, or powershell command-line.
                    powershellexecfile = cmd
                    break

                # print(powershellexecfile)
                # wow
                # cmd_list.append(powershellexecfile + ' ' + params_string)
                if (str(powershellexecfile).__contains__(' ')):
                    if (os.path.isfile(powershellexecfile)):
                        powershellexecfile = powershellexecfile.replace('/', os.path.sep)
                        powershellexecfile = powershellexecfile.replace('\\', os.path.sep)
                        cmd_list.append(". \"%s\" @args" % powershellexecfile)
                    else:
                        cmd_list.append("%s" % powershellexecfile)
                else:
                    if (os.path.isfile(powershellexecfile)):
                        powershellexecfile = powershellexecfile.replace('/', os.path.sep)
                        powershellexecfile = powershellexecfile.replace('\\', os.path.sep)
                        cmd_list.append(". %s @args" % powershellexecfile)
                    else:
                        cmd_list.append("%s" % powershellexecfile)

        # append exit 0
        cmd_list.append(cmd_exit)
        # print( cmd_list )

        cmd_execute = "" + name + "_exec" + cmd_suffix
        with open(cmd_execute, "w", encoding=cmd_codec) as f:
            for line in cmd_list:
                f.write(line + cmd_return)
        # print(cmd_execute)

        if (debugswitch == '1'):
            print("IN: execute file: %s" % cmd_execute)
            for cmd in cmd_list:
                print(cmd)
            print("---------------------------")

        # if (plat == "Windows"):
        #    ""
        # else:
        #    os.system("chmod +x " + cmd_execute)

        cmd_list.clear()
        cmd_list.append(cmd_call + cmd_execute + ' ' + params_string + cmd_sep + ' ' + cmd_status)
        cmd_list.append(cmd_exit)

        # print (cmd_list)
        if (debugswitch == '1'):
            print("CMD: call execute file: %s" % cmd_execute)
            for cmd in cmd_list:
                print(cmd)
            print("---------------------------")

        return cmd_list, name

    # print(args)
    # print(args['powershell'])
    # powershell is using basic, .json, and custom environ data.
    # powershell command

    # python export function
    def python_env_export(env_name=None, file_name=None):
        # select env
        current_var = rawconfig['environ']['current']
        if (env_name is not None):
            current_var = env_name
        dict0 = copy.deepcopy(rawconfig['environ'][current_var])

        plat = getplatform()
        if (plat == "Windows"):
            cmd_suffix = ".bat"
            cmd_codec = "ansi"
            if (getplatform_release() == "XP"):
                cmd_codec = None
            cmd_return = "\n"
            cmd_header = "@echo off" + cmd_return
            env_set = 'set '
        else:
            cmd_suffix = ".sh"
            cmd_codec = "utf8"
            cmd_return = "\n"
            cmd_header = "#!/usr/bin/env bash" + cmd_return
            env_set = 'export '

        # export effect env
        cmd_effect = 'env'
        if (file_name is not None):
            cmd_effect = file_name
        cmd_effect += '_effect' + cmd_suffix

        # export path
        lines = ""
        for (key) in dict0["path+"]:
            if (plat == "Windows"):
                lines += (env_set + 'PATH=' + key + os.path.pathsep + '%PATH%' + cmd_return)
            else:
                lines += (env_set + 'PATH="' + key + '"' + os.path.pathsep + '$PATH' + cmd_return)

        # export var
        for (key, value) in dict0.items():
            if (key == 'path+'):
                continue
            if (plat == "Windows"):
                lines += (env_set + key + '=' + value + cmd_return)
            else:
                lines += (env_set + key + '=\"' + value + '\"' + cmd_return)

        with open(cmd_effect, 'w', encoding=cmd_codec) as f:
            f.write(cmd_header)
            f.write(lines)

        # export unset env
        cmd_unset = 'env'
        if (file_name is not None):
            cmd_unset = file_name
        cmd_unset += '_unset' + cmd_suffix

        # export unset path
        lines = ""
        for (key) in dict0["path+"]:
            if (plat == "Windows"):
                lines += (env_set + 'PATH=%PATH:' + key + ';=%' + cmd_return)
            else:
                lines += (env_set + 'PATH=$(' + 'echo ${PATH//' + key.replace('/', '\/') + ':/})' + cmd_return)

        # export unset var
        for (key, value) in dict0.items():
            if (key == 'path+'):
                continue
            if (plat == "Windows"):
                lines += ('set ' + key + '=' + cmd_return)
            else:
                lines += ('unset ' + key + cmd_return)
        with open(cmd_unset, 'w', encoding=cmd_codec) as f:
            f.write(cmd_header)
            f.write(lines)

        if (plat == "Windows"):
            ""
        else:
            os.system("chmod +x " + cmd_effect)
            os.system("chmod +x " + cmd_unset)

        # return file name
        return current_var, cmd_effect, cmd_unset

    # python [windows unix]
    def createCmdList05(env_name=None, local=True, list0=[], params0=[]):
        cmd_list = []

        if (env_name is None):
            env_name = rawconfig['environ']['current']

        name = uuid.uuid4().__str__()
        name = name.split('-')[0]
        # print (name)

        plat = getplatform()
        if (plat == "Windows"):
            cmd_status = "echo pymake-command-status:%ERRORLEVEL%"
            cmd_sep = '&'
            cmd_codec = "ansi"
            if (getplatform_release() == "XP"):
                cmd_codec = None
            # but windows, it is \r\n, python helpping me?
            cmd_return = "\n"
            cmd_exit = 'exit /b %ERRORLEVEL%'
            cmd_suffix = ".bat"
            cmd_header = "@echo off"
            cmd_call = "call "
            # window close echo, close promot
            cmd_list.append(cmd_header)
            # os.system("type env_effect.bat > cmd_exec.bat")
            cmd_list.append("call %s_effect.bat" % name)
        else:
            cmd_status = "echo pymake-command-status:$?"
            cmd_sep = ';'
            cmd_suffix = ".sh"
            cmd_exit = 'exit $?'
            cmd_codec = "utf8"
            cmd_return = "\n"
            cmd_header = "#!/usr/bin/env bash"
            cmd_call = "./"
            cmd_list.append(cmd_header)
            cmd_list.append("source %s_effect.sh" % name)

        cmd_suffix_python = '.py'
        cmd_codec_python = "utf8"
        cmd_return_python = "\n"

        params_string = ""
        for param in params0:
            if (str(param).__contains__(' ')):
                params_string += '"' + param + '"' + ' '
            else:
                params_string += param + ' '
        # print(params_string)

        pythonexecfile = ''
        if (local is True):
            # fixed
            pythonexecfile = name + '_exec' + cmd_suffix_python
            with open(pythonexecfile, 'w', encoding=cmd_codec_python) as f:
                for cmd in list0:
                    f.write(cmd + cmd_return_python)
                    # print(1, pythonexecfile)
        else:
            # actually now has only one command.
            for cmd in list0:
                # warning: now pymake is in user setted workroot.
                if (str(cmd).endswith(cmd_suffix_python)):
                    cmd_suffix_python = ''

                pythonexecfile = ""
                while (True):
                    # find in current path [+workroot]
                    pythonexecfile = os.getcwd() + os.path.sep + cmd + cmd_suffix_python
                    # print(2, pythonexecfile)
                    if (os.path.exists(pythonexecfile)):
                        break
                    pythonexecfile = startupworkdirectory + os.path.sep + cmd + cmd_suffix_python
                    # print(2, pythonexecfile)
                    if (os.path.exists(pythonexecfile)):
                        break

                    # find in .json environ
                    separateenvlistpath = os.path.pathsep.join(rawconfig['environ'][env_name]['path+'])
                    separateenvlistpath = separateenvlistpath.split(os.path.pathsep)
                    separateenvlistpath.reverse()
                    # for path0 in separateenvlistpath:
                    #    print(path0)
                    find_flag = 0
                    for path0 in separateenvlistpath:
                        pythonexecfile = path0 + os.path.sep + cmd + cmd_suffix_python
                        # print(2, pythonexecfile)
                        if (os.path.exists(pythonexecfile)):
                            find_flag = 1
                            break
                    if (find_flag == 1):
                        break

                    # find in basic environ [custom+]
                    env = os.environ
                    find_flag = 0
                    for path0 in env["PATH"].split(os.path.pathsep):
                        pythonexecfile = path0 + os.path.sep + cmd + cmd_suffix_python
                        # print(2, pythonexecfile)
                        if (os.path.exists(pythonexecfile)):
                            find_flag = 1
                            break
                    if (find_flag == 1):
                        break

                    # none? a python command, or python command-line.
                    pythonexecfile = cmd
                    break

                    # print(2, pythonexecfile)
                    # cmd_list.append(pythonexecfile + ' ' + params_string)

        # print params.
        # print(3, pythonexecfile)

        # get python command.
        pycmd = ''
        if (plat == "Windows"):
            pycmd = 'python.exe'
        else:
            pycmd = 'python3'

        pycmd = which_command(env_name, pycmd)

        # print(pycmd)
        if (plat == "Windows"):
            if (pycmd is None):
                pycmd = 'py'
            elif (pycmd != 'py'):
                pycmd = 'python'
        else:
            pycmd = 'python3'

        if (os.path.isfile(pythonexecfile)):
            if (plat == "Windows"):
                cmd_list.append("call %s \"%s\" %s" % (pycmd, pythonexecfile, '%*'))
            else:
                cmd_list.append("%s \"%s\" %s" % (pycmd, pythonexecfile, '"$@"'))
        else:
            if (plat == "Windows"):
                cmd_list.append("call %s -c \"%s\" %s" % (pycmd, pythonexecfile, '%*'))
            else:
                cmd_list.append("%s -c \"%s\" %s" % (pycmd, pythonexecfile, '"$@"'))

        # append exit 0
        cmd_list.append(cmd_exit)

        cmd_execute = name + "_exec" + cmd_suffix
        with open(cmd_execute, "w", encoding=cmd_codec) as f:
            for line in cmd_list:
                f.write(line + cmd_return)
        # print(cmd_execute)

        if (debugswitch == '1'):
            print("IN: execute file: %s" % cmd_execute)
            for cmd in cmd_list:
                print(cmd)
            print("---------------------------")

        if (plat == "Windows"):
            ""
        else:
            os.system("chmod +x " + cmd_execute)

        cmd_list.clear()
        if (plat == "Windows"):
            cmd_list.append(cmd_header + ' ' + cmd_sep + ' ' + cmd_status)
            # cmd_list.append("call " + cmd_execute + cmd_sep + ' ' + cmd_status)
            cmd_list.append("call " + cmd_execute + ' ' + params_string + cmd_sep + ' ' + cmd_status)
        else:
            # cmd_list.append(cmd_header + ' ' + cmd_sep + ' ' + cmd_status)
            # cmd_list.append("./" + cmd_execute + cmd_sep + ' ' + cmd_status)
            cmd_list.append("./" + cmd_execute + ' ' + params_string + cmd_sep + ' ' + cmd_status)

        cmd_list.append(cmd_exit)

        if (debugswitch == '1'):
            print("CMD: call execute file: %s" % cmd_execute)
            for cmd in cmd_list:
                print(cmd)
            print("---------------------------")

        return cmd_list, name

    # python command

    # python ccvp command

    # print(args)
    # print(args['custom'])
    # print(args['hh'])
    # custom command

    # system command

    # language export function
    def language_env_export(env_name=None, file_name=None):
        # select env
        current_var = rawconfig['environ']['current']
        if (env_name is not None):
            current_var = env_name
        dict0 = copy.deepcopy(rawconfig['environ'][current_var])

        plat = getplatform()
        if (plat == "Windows"):
            cmd_suffix = ".bat"
            cmd_codec = "ansi"
            if (getplatform_release() == "XP"):
                cmd_codec = None
            cmd_return = "\n"
            cmd_header = "@echo off" + cmd_return
            env_set = 'set '
        else:
            cmd_suffix = ".sh"
            cmd_codec = "utf8"
            cmd_return = "\n"
            cmd_header = "#!/usr/bin/env bash" + cmd_return
            env_set = 'export '

        # export effect env
        cmd_effect = 'env'
        if (file_name is not None):
            cmd_effect = file_name
        cmd_effect += '_effect' + cmd_suffix

        # export path
        lines = ""
        for (key) in dict0["path+"]:
            if (plat == "Windows"):
                lines += (env_set + 'PATH=' + key + os.path.pathsep + '%PATH%' + cmd_return)
            else:
                lines += (env_set + 'PATH="' + key + '"' + os.path.pathsep + '$PATH' + cmd_return)

        # export var
        for (key, value) in dict0.items():
            if (key == 'path+'):
                continue
            if (plat == "Windows"):
                lines += (env_set + key + '=' + value + cmd_return)
            else:
                lines += (env_set + key + '=\"' + value + '\"' + cmd_return)

        with open(cmd_effect, 'w', encoding=cmd_codec) as f:
            f.write(cmd_header)
            f.write(lines)

        # export unset env
        cmd_unset = 'env'
        if (file_name is not None):
            cmd_unset = file_name
        cmd_unset += '_unset' + cmd_suffix

        # export unset path
        lines = ""
        for (key) in dict0["path+"]:
            if (plat == "Windows"):
                lines += (env_set + 'PATH=%PATH:' + key + ';=%' + cmd_return)
            else:
                lines += (env_set + 'PATH=$(' + 'echo ${PATH//' + key.replace('/', '\/') + ':/})' + cmd_return)

        # export unset var
        for (key, value) in dict0.items():
            if (key == 'path+'):
                continue
            if (plat == "Windows"):
                lines += ('set ' + key + '=' + cmd_return)
            else:
                lines += ('unset ' + key + cmd_return)
        with open(cmd_unset, 'w', encoding=cmd_codec) as f:
            f.write(cmd_header)
            f.write(lines)

        if (plat == "Windows"):
            ""
        else:
            os.system("chmod +x " + cmd_effect)
            os.system("chmod +x " + cmd_unset)

        # return file name
        return current_var, cmd_effect, cmd_unset

    # language [ .bat .sh .ps1 .py ...] [windows unix] --suffix --encoding
    def createCmdList07(suffix=None, encoding=None, env_name=None, local=True, list0=[], params0=[]):
        cmd_list = []

        name = uuid.uuid4().__str__()
        name = name.split('-')[0]
        # print (name)

        plat = getplatform()
        if (plat == "Windows"):
            cmd_status = "echo pymake-command-status:%ERRORLEVEL%"
            cmd_sep = '&'
            cmd_codec = "ansi"
            if (getplatform_release() == "XP"):
                cmd_codec = None
            # but windows, it is \r\n, python helpping me?
            cmd_return = "\n"
            cmd_exit = 'exit /b %ERRORLEVEL%'
            cmd_suffix = ".bat"
            cmd_header = "@echo off"
            cmd_call = "call "
            # window close echo, close promot
            cmd_list.append(cmd_header)
            # os.system("type env_effect.bat > cmd_exec.bat")
            cmd_list.append("call %s_effect.bat" % name)
        else:
            cmd_status = "echo pymake-command-status:$?"
            cmd_sep = ';'
            cmd_suffix = ".sh"
            cmd_exit = 'exit $?'
            cmd_codec = "utf8"
            cmd_return = "\n"
            cmd_header = "#!/usr/bin/env bash"
            cmd_call = "./"
            cmd_list.append(cmd_header)
            cmd_list.append("source %s_effect.sh" % name)

        if (env_name is None):
            env_name = rawconfig['environ']['current']
        if (env_name == "current"):
            env_name = rawconfig['environ']['current']

        cmd_suffix_language = cmd_suffix
        cmd_codec_language = cmd_codec
        cmd_return_language = cmd_return

        if (suffix is not None):
            cmd_suffix_language = suffix
        if (encoding is not None):
            cmd_codec_language = encoding

        list1 = []
        # for current_var in str(args['<command-param>']).split():
        #    list1.append(current_var)
        if (params0.__len__() > 0):
            current_var = params0[0]
            list1.append(current_var)
            params0.pop(0)
        # print(list1)

        params_string = ""
        for param in params0:
            if (str(param).__contains__(' ')):
                params_string += '"' + param + '"' + ' '
            else:
                params_string += param + ' '
        # print(params_string)

        local2 = True
        if (list1.__len__() > 0):
            current_var = list1[0]
            if (current_var in rawconfig['command']):
                local2 = True
            else:
                local2 = False

        languageparams = ''
        # actually only one param.
        if (local2 is True):
            for param1 in list1:
                languageparams += param1
        else:
            for param1 in list1:
                # warning: now pymake is in user setted workroot.

                languageparams = ""
                while (True):
                    # find in current path [+--workroot]
                    languageparams = os.getcwd() + os.path.sep + param1
                    # print(2, languageparams)
                    if (os.path.exists(languageparams)):
                        break
                    languageparams = startupworkdirectory + os.path.sep + param1
                    # print(2, languageparams)
                    if (os.path.exists(languageparams)):
                        break

                    # find in .json environ
                    separateenvlistpath = os.path.pathsep.join(rawconfig['environ'][env_name]['path+'])
                    separateenvlistpath = separateenvlistpath.split(os.path.pathsep)
                    separateenvlistpath.reverse()
                    # for path0 in separateenvlistpath:
                    #    print(path0)
                    find_flag = 0
                    for path0 in separateenvlistpath:
                        languageparams = path0 + os.path.sep + param1
                        # print(2, languageparams)
                        if (os.path.exists(languageparams)):
                            find_flag = 1
                            break
                    if (find_flag == 1):
                        break

                    # find in basic environ [custom+]
                    env = os.environ
                    find_flag = 0
                    for path0 in env["PATH"].split(os.path.pathsep):
                        languageparams = path0 + os.path.sep + param1
                        # print(2, languageparams)
                        if (os.path.exists(languageparams)):
                            find_flag = 1
                            break
                    if (find_flag == 1):
                        break

                    # none? a language command, or language command-line.
                    languageparams = param1
                    break

                    # print(2, languageparams)
                    # cmd_list.append(languageparams + ' ' + params_string)

        if (list1.__len__() > 0):
            ''
            if (local2 is True):
                ''
                current_var = env_name
                local_command = raw_command(current_var)
                dict0 = copy.deepcopy(local_command)

                inside_name = name + '_2'
                languageparams = inside_name + '_exec' + cmd_suffix_language

                with open(languageparams, 'w', encoding=cmd_codec_language) as f:
                    for cmd1 in list1:
                        for cmd in dict0[cmd1]:
                            f.write(cmd + cmd_return_language)

                            # print(1, cmd_suffix_language)
                            # print(1, cmd_return_language)
                            # print(1, cmd_codec_language)
                            # print(1, languageparams)
            else:
                ''

        # print(3, languageparams)
        if (list1.__len__() > 0):
            if (local2 is True):
                params_string = languageparams + ' ' + params_string
            else:
                if (str(languageparams).__contains__(' ')):
                    params_string = '"' + languageparams + '"' + ' ' + params_string
                else:
                    params_string = languageparams + ' ' + params_string
        # print(params_string)

        languageexecfile = ''
        if (local is True):
            # fixed
            # inside_name = name
            # inside_name = hex( int( inside_name, 16 ) + 1).split('x')[1]
            inside_name = name + '_1'
            # print(inside_name)
            languageexecfile = inside_name + '_exec' + cmd_suffix
            with open(languageexecfile, 'w', encoding=cmd_codec) as f:
                for cmd in list0:
                    f.write(cmd + cmd_return)
                    # print(1, languageexecfile)
        else:
            languageexecfile = ''
            # actually now has only one command.
            for cmd in list0:
                # actually this is a command.
                if (str(cmd).__contains__(' ')):
                    pycmd = which_command(env_name, str(cmd))
                    # print(cmd)
                    # print(pycmd)
                    if (pycmd is not None and os.path.isfile(pycmd)):
                        languageexecfile = '"' + cmd + '"'
                    else:
                        languageexecfile = cmd
                else:
                    languageexecfile = cmd

        # print(3, languageexecfile)
        if (plat == "Windows"):
            cmd_list.append("call %s %s" % (languageexecfile, '%*'))
        else:
            if (languageexecfile.endswith('.sh')):
                cmd_list.append("sh %s %s" % (languageexecfile, '"$@"'))
            else:
                cmd_list.append("%s %s" % (languageexecfile, '"$@"'))

        # append exit 0
        cmd_list.append(cmd_exit)

        cmd_execute = name + "_exec" + cmd_suffix
        with open(cmd_execute, "w", encoding=cmd_codec) as f:
            for line in cmd_list:
                f.write(line + cmd_return)

        if (debugswitch == '1'):
            print("IN: execute file: %s" % cmd_execute)
            for cmd in cmd_list:
                print(cmd)
            print("---------------------------")

        if (plat == "Windows"):
            ""
        else:
            os.system("chmod +x " + cmd_execute)

        cmd_list.clear()
        if (plat == "Windows"):
            cmd_list.append(cmd_header + ' ' + cmd_sep + ' ' + cmd_status)
            # cmd_list.append("call " + cmd_execute + cmd_sep + ' ' + cmd_status)
            cmd_list.append("call " + cmd_execute + ' ' + params_string + cmd_sep + ' ' + cmd_status)
        else:
            # cmd_list.append(cmd_header + ' ' + cmd_sep + ' ' + cmd_status)
            # cmd_list.append("./" + cmd_execute + cmd_sep + ' ' + cmd_status)
            cmd_list.append("./" + cmd_execute + ' ' + params_string + cmd_sep + ' ' + cmd_status)

        cmd_list.append(cmd_exit)

        if (debugswitch == '1'):
            print("CMD: call execute file: %s" % cmd_execute)
            for cmd in cmd_list:
                print(cmd)
            print("---------------------------")

        return cmd_list, name, cmd_suffix, cmd_suffix_language

    # language command

    # ===============================================================================================
    # PYMAKE CCVP COMMAND INITIAL CODE
    # --------------------

    # cmd type2 function
    def cmd_type2(cmd_name=None, file_name=None, env_name=None):

        if (cmd_name is None):
            for (key, value) in rawconfig['command'].items():
                print(Fore.CYAN + "%s" % key)
            return

        if (rawconfig['command'].__contains__(cmd_name) is False):
            print("please check your command name")
            return

        if (env_name is None or env_name == rawconfig['environ']['current']):
            list0 = copy.deepcopy(rawconfig['command'][cmd_name])
        else:
            list0 = copy.deepcopy(raw_command(env_name)[cmd_name])

        # for cmd in list0:
        #    print(Fore.RED + "%s" % (cmd))

        temp_file_name = ""
        if (file_name is None):
            temp_file_name = "cmd"
        else:
            temp_file_name = "" + file_name

        cmd_header = ""
        cmd_codec = "utf8"
        # but windows, it is \r\n, python helpping me?
        cmd_return = "\n"
        cmd_suffix = ""
        if (getplatform() == "Windows"):
            cmd_codec = "ansi"
            if (getplatform_release() == "XP"):
                cmd_codec = None
            cmd_header = "@echo off"
            cmd_suffix = ".bat"
        else:
            cmd_codec = 'utf8'
            cmd_header = "#!/usr/bin/env bash"
            cmd_suffix = ".sh"

        suffix = args['--suffix']
        if (suffix is not None):
            cmd_suffix = str("%s" % suffix)

        encoding = args['--encoding']
        if (encoding is not None):
            cmd_codec = encoding

        cmd_exec = temp_file_name + cmd_suffix
        with open(cmd_exec, 'w', encoding=cmd_codec, errors='ignore') as f:
            # f.write(cmd_header + cmd_return)
            cmd = ''
            # add shebang line
            if (list(list0).__len__() > 0):
                cmd = list0[0]
            # print(".....")
            if (getplatform() == "Windows"):
                if (cmd_suffix == '.bat'):
                    if (cmd.startswith('@echo') is False):
                        f.write(cmd_header + cmd_return)
            else:
                if (cmd_suffix == '.sh'):
                    if (cmd.startswith('#!') is False):
                        f.write(cmd_header + cmd_return)

            for cmd in list0:
                f.write(cmd + cmd_return)

        if (plat == "Windows"):
            ""
        else:
            if (cmd_suffix == '.sh'):
                os.system("chmod +x " + cmd_exec)

        # print(cmd_codec)
        # print(cmd_suffix)
        # print(cmd_exec)

        return cmd_exec

    # type2 command

    # open command

    # clean *_effect *_unset *_exec .bat[.sh]

    # list show

    # have has

    # need return, set return, default break.
    # get

    # get

    # env environ

    # cmd_type function
    def cmd_type(cmd_name=None, file_name=None, env_name=None):
        if (cmd_name is None):
            for (key, value) in rawconfig['command'].items():
                print(Fore.CYAN + "%s" % key)
            return ""

        if (rawconfig['command'].__contains__(cmd_name) is False):
            print("please check your command name")
            return ""

        if (env_name is None or env_name == rawconfig['environ']['current']):
            list0 = copy.deepcopy(rawconfig['command'][cmd_name])
        else:
            list0 = copy.deepcopy(raw_command(env_name)[cmd_name])

        # for cmd in list0:
        #    print(Fore.RED + "%s" % (cmd))

        temp_file_name = ""
        if (file_name is None):
            temp_file_name = "cmd"
        else:
            temp_file_name = file_name

        if (getplatform() == "Windows"):
            cmd_header = "@echo off"
            cmd_suffix = "_exec.bat"
        else:
            cmd_header = "#!/usr/bin/env bash"
            cmd_suffix = "_exec.sh"

        cmd_exec = temp_file_name + cmd_suffix
        with open(cmd_exec, 'w', encoding=cmd_codec) as f:
            cmd = ''
            # add shebang line
            if (list(list0).__len__() > 0):
                cmd = list0[0]
            # print(".....")
            if (getplatform() == "Windows"):
                if (cmd.startswith('@echo') is False):
                    f.write(cmd_header + cmd_return)
            else:
                if (cmd.startswith('#!') is False):
                    f.write(cmd_header + cmd_return)
            for cmd in list0:
                f.write(cmd + cmd_return)

        if (plat == "Windows"):
            ""
        else:
            os.system("chmod +x " + cmd_exec)

        return cmd_exec

    # use - see/ss/cmd

    # see ss cmd

    # use env type command

    # type

    # export function
    def env_export(env_name=None, file_name=None):
        # select env
        current_var = rawconfig['environ']['current']
        if (env_name is not None):
            current_var = env_name
        dict0 = copy.deepcopy(rawconfig['environ'][current_var])

        plat = getplatform()
        if (plat == "Windows"):
            cmd_suffix = ".bat"
            cmd_codec = "ansi"
            if (getplatform_release() == "XP"):
                cmd_codec = None
            cmd_return = "\n"
            cmd_header = "@echo off" + cmd_return
            env_set = 'set '
        else:
            cmd_suffix = ".sh"
            cmd_codec = "utf8"
            cmd_return = "\n"
            cmd_header = "#!/usr/bin/env bash" + cmd_return
            env_set = 'export '

        # export effect env
        cmd_effect = 'env'
        if (file_name is not None):
            cmd_effect = file_name
        cmd_effect += '_effect' + cmd_suffix

        # export path
        lines = ""
        for (key) in dict0["path+"]:
            if (plat == "Windows"):
                lines += (env_set + 'PATH=' + key + os.path.pathsep + '%PATH%' + cmd_return)
            else:
                lines += (env_set + 'PATH="' + key + '"' + os.path.pathsep + '$PATH' + cmd_return)

        # export var
        for (key, value) in dict0.items():
            if (key == 'path+'):
                continue
            if (plat == "Windows"):
                lines += (env_set + key + '=' + value + cmd_return)
            else:
                lines += (env_set + key + '=\"' + value + '\"' + cmd_return)

        with open(cmd_effect, 'w', encoding=cmd_codec) as f:
            f.write(cmd_header)
            f.write(lines)

        # export unset env
        cmd_unset = 'env'
        if (file_name is not None):
            cmd_unset = file_name
        cmd_unset += '_unset' + cmd_suffix

        # export unset path
        lines = ""
        for (key) in dict0["path+"]:
            if (plat == "Windows"):
                lines += (env_set + 'PATH=%PATH:' + key + ';=%' + cmd_return)
            else:
                lines += (env_set + 'PATH=$(' + 'echo ${PATH//' + key.replace('/', '\/') + ':/})' + cmd_return)

        # export unset var
        for (key, value) in dict0.items():
            if (key == 'path+'):
                continue
            if (plat == "Windows"):
                lines += ('set ' + key + '=' + cmd_return)
            else:
                lines += ('unset ' + key + cmd_return)
        with open(cmd_unset, 'w', encoding=cmd_codec) as f:
            f.write(cmd_header)
            f.write(lines)

        if (plat == "Windows"):
            ""
        else:
            os.system("chmod +x " + cmd_effect)
            os.system("chmod +x " + cmd_unset)

        # return file name
        return current_var, cmd_effect, cmd_unset

    # export

    # get

    # .bat .sh, windows unix
    def createCmdList0(list0):

        cmd_list = []

        name = uuid.uuid4().__str__()
        name = name.split('-')[0]
        # print (name)

        plat = getplatform()
        if (plat == "Windows"):
            cmd_status = "echo pymake-command-status:%ERRORLEVEL%"
            cmd_sep = '&'
            cmd_codec = "ansi"
            if (getplatform_release() == "XP"):
                cmd_codec = None
            # but windows, it is \r\n, python helpping me?
            cmd_return = "\n"
            cmd_exit = 'exit /b %ERRORLEVEL%'
            cmd_suffix = ".bat"
            cmd_header = "@echo off"
            # window close echo, close promot
            cmd_list.append(cmd_header)
            # os.system("type env_effect.bat > cmd_exec.bat")
            cmd_list.append("call %s_effect.bat" % name)
        else:
            cmd_status = "echo pymake-command-status:$?"
            cmd_sep = ';'
            cmd_suffix = ".sh"
            cmd_exit = 'exit $?'
            cmd_codec = "utf8"
            cmd_return = "\n"
            cmd_header = "#!/usr/bin/env bash"
            cmd_list.append(cmd_header)
            cmd_list.append("source %s_effect.sh" % name)

        for cmd in list0:
            cmd_list.append(cmd)

        # append exit 0
        cmd_list.append(cmd_exit)
        # print( cmd_list )

        cmd_execute = name + "_exec" + cmd_suffix
        with open(cmd_execute, "w", encoding=cmd_codec) as f:
            for line in cmd_list:
                f.write(line + cmd_return)

        if (debugswitch == '1'):
            print("IN: execute file: %s" % cmd_execute)
            for cmd in cmd_list:
                print(cmd)
            print("---------------------------")

        if (plat == "Windows"):
            ""
        else:
            os.system("chmod +x " + cmd_execute)

        cmd_list.clear()
        if (plat == "Windows"):
            cmd_list.append(cmd_header + ' ' + cmd_sep + ' ' + cmd_status)
            cmd_list.append("call " + cmd_execute + ' ' + cmd_sep + ' ' + cmd_status)
        else:
            # cmd_list.append(cmd_header + ' ' + cmd_sep + ' ' + cmd_status)
            cmd_list.append("./" + cmd_execute + ' ' + cmd_sep + ' ' + cmd_status)
        cmd_list.append(cmd_exit)

        if (debugswitch == '1'):
            print("CMD: call execute file: %s" % cmd_execute)
            for cmd in cmd_list:
                print(cmd)
            print("---------------------------")

        # print (cmd_list)
        return cmd_list, name

    # .bat .sh, windows not compatibility, unix only [ignore]
    def createCmdList01(list0):

        cmd_list = []

        name = uuid.uuid4().__str__()
        name = name.split('-')[0]

        plat = getplatform()
        if (plat == "Windows"):
            cmd_status = "echo pymake-command-status:%errorlevel%"
            cmd_sep = '&'
            cmd_header = "@echo off"
            cmd_exit = 'exit /b %ERRORLEVEL%'
            # window close echo, close promot
            cmd_list.append(cmd_header + ' ' + cmd_sep + ' ' + cmd_status)
            cmd_list.append("call %s_effect.bat" % name + ' ' + cmd_sep + ' ' + cmd_status)
        else:
            cmd_status = "echo pymake-command-status:$?"
            cmd_sep = ';'
            cmd_exit = 'exit $?'
            cmd_header = "#!/usr/bin/env bash"
            cmd_list.append("source %s_effect.sh" % name + ' ' + cmd_sep + ' ' + cmd_status)

        for cmd in list0:
            cmd_list.append(cmd + ' ' + cmd_sep + ' ' + cmd_status)

        # append exit 0
        cmd_list.append(cmd_exit)
        # print( cmd_list )
        return cmd_list, name

    # use env exec command

    # cc exec

    # .bat .sh, windows, unix
    def createCmdList02(env_name=None, local=True, list0=[], params0=[]):

        cmd_list = []

        if (env_name is None):
            env_name = rawconfig['environ']['current']
        # print(env_name)

        name = uuid.uuid4().__str__()
        name = name.split('-')[0]
        # print (name)

        plat = getplatform()
        if (plat == "Windows"):
            cmd_status = "echo pymake-command-status:%ERRORLEVEL%"
            cmd_sep = '&'
            cmd_codec = "ansi"
            if (getplatform_release() == "XP"):
                cmd_codec = None
            # but windows, it is \r\n, python helpping me?
            cmd_return = "\n"
            cmd_exit = 'exit /b %ERRORLEVEL%'
            cmd_suffix = ".bat"
            cmd_header = "@echo off"
            cmd_call = "call "
            # window close echo, close promot
            cmd_list.append(cmd_header)
            # os.system("type env_effect.bat > cmd_exec.bat")
            cmd_list.append("call %s_effect.bat" % name)
        else:
            cmd_status = "echo pymake-command-status:$?"
            cmd_sep = ';'
            cmd_suffix = ".sh"
            cmd_exit = 'exit $?'
            cmd_codec = "utf8"
            cmd_return = "\n"
            cmd_header = "#!/usr/bin/env bash"
            cmd_call = "./"
            cmd_list.append(cmd_header)
            cmd_list.append("source %s_effect.sh" % name)

        # print(params0)
        params_string = ""
        for param in params0:
            # print(param)
            if (str(param).__contains__(' ')):
                params_string += '"' + param + '"' + ' '
            else:
                params_string += param + ' '
        # print(params_string)

        if (local is True):
            for cmd in list0:
                cmd_list.append(cmd)
        else:
            for cmd in list0:
                if (str(cmd).__contains__(' ')):
                    pycmd = which_command(env_name, str(cmd))
                    # print(str(cmd))
                    # print(Fore.RED + "which command:", pycmd)
                    if (pycmd is not None and os.path.isfile(pycmd)):
                        cmd_list.append('"' + cmd + '"' + ' ' + params_string)
                    else:
                        cmd_list.append(cmd + ' ' + params_string)
                else:
                    cmd_list.append(cmd + ' ' + params_string)

        # append exit 0
        cmd_list.append(cmd_exit)
        # print( cmd_list )

        cmd_execute = name + "_exec" + cmd_suffix
        with open(cmd_execute, "w", encoding=cmd_codec) as f:
            for line in cmd_list:
                f.write(line + cmd_return)
        # print(cmd_execute)

        if (debugswitch == '1'):
            print("IN: execute file: %s" % cmd_execute)
            for cmd in cmd_list:
                print(cmd)
            print("---------------------------")

        if (plat == "Windows"):
            ""
        else:
            os.system("chmod +x " + cmd_execute)

        cmd_list.clear()
        if (plat == "Windows"):
            cmd_list.append(cmd_header + ' ' + cmd_sep + ' ' + cmd_status)
            # cmd_list.append("call " + cmd_execute + cmd_sep + ' ' + cmd_status)
            cmd_list.append("call " + cmd_execute + ' ' + params_string + cmd_sep + ' ' + cmd_status)
        else:
            # cmd_list.append(cmd_header + ' ' + cmd_sep + ' ' + cmd_status)
            # cmd_list.append("./" + cmd_execute + cmd_sep + ' ' + cmd_status)
            cmd_list.append("./" + cmd_execute + ' ' + params_string + cmd_sep + ' ' + cmd_status)

        cmd_list.append(cmd_exit)

        # print (cmd_list)
        if (debugswitch == '1'):
            print("CMD: call execute file: %s" % cmd_execute)
            for cmd in cmd_list:
                print(cmd)
            print("---------------------------")

        return cmd_list, name

    # use env exec-with-params/execvp/ccvp command

    # exec-with-params ccvp execvp

    # here [False]

    # there [False]

    # default [False]

    # initialize

    # ===============================================================================================
    # pydependent work flow
    # -------------------------
    def format_size(size):
        if size < 1000:
            return '%i' % size + 'size'
        elif 1000 <= size < 1000000:
            return '%.1f' % float(size / 1000) + 'KB'
        elif 1000000 <= size < 1000000000:
            return '%.1f' % float(size / 1000000) + 'MB'
        elif 1000000000 <= size < 1000000000000:
            return '%.1f' % float(size / 1000000000) + 'GB'
        elif 1000000000000 <= size:
            return '%.1f' % float(size / 1000000000000) + 'TB'

    pymaketoolfileroot = pymakefileroot
    pymaketoolstartuproot = startupworkdirectory

    pydeprequiretxt = 'requirements.txt'
    pydep_cmdlinetxt = ''
    if(sys.argv.__len__()>1):
        ''
        pydep_cmdlinetxt = os.path.realpath(sys.argv[1])

    pydeprequiretxtlist = [
        os.path.join(pymaketoolfileroot, pydeprequiretxt),
        os.path.join(pymaketoolstartuproot, pydeprequiretxt),
        os.path.join(sourceroot, pydeprequiretxt),
        pydep_cmdlinetxt
    ]
    #print(pydeprequiretxtlist)

    pydeprequirepackagelist = []
    for file in pydeprequiretxtlist:
        if(file == ''):
            continue

        if(os.path.exists(file) and os.path.isfile(file)):
            datas = ""
            with open(file, 'r', encoding='utf8') as f:
                for line in f.readlines():
                    datas += line
            datas = datas.replace('\r', '')
            datalist = datas.split('\n')
            for package in datalist:
                if(package == ''):
                    continue
                if(pydeprequirepackagelist.__contains__(package)):
                    continue
                pydeprequirepackagelist.append(package)
    #print(pydeprequirepackagelist)

    pydepjson = os.path.join(sourceroot, 'dep.json')
    pydepconfig = {}

    d_dep = {
        "PyPi": {
            "官方源": "https://pypi.python.org/simple",
            "清华源": "https://pypi.tuna.tsinghua.edu.cn/simple",
            "中科大": "https://mirrors.ustc.edu.cn/pypi/web/simple",
            "阿里源": "https://mirrors.aliyun.com/pypi/simple",
            "网易源": "https://mirrors.163.com/pypi/simple",
            "腾讯源": "https://mirrors.cloud.tencent.com/pypi/simple",
            "豆瓣源": "https://pypi.doubanio.com/simple",
            "current": "清华源"
        },
        "Python": {
            "Py Launcher": "C:\\Windows\\py.exe",
            "-3.7-32": "C:\\Users\\Administrator\\AppData\\Local\\Programs\\Python\\Python37-32\\python.exe",
            "current": "Py Launcher"
        }
    }

    plat = getplatform()
    if(plat == 'Windows'):
        pass
    elif (plat == 'Darwin'):
        d_dep['Python'] = {
            "3.6": "/Library/Frameworks/Python.framework/Versions/3.6/bin/python3",
            "3.7": "/Library/Frameworks/Python.framework/Versions/3.7/bin/python3",
            "3.8": "/Library/Frameworks/Python.framework/Versions/3.8/bin/python3",
            "current": "3.6"
        }
    else:
        d_dep['Python'] = {
            "python 3": "/usr/bin/python3",
            "current": "python 3"
        }

    if(not os.path.exists(pydepjson)):
        writeJsonData(pydepjson, d_dep)
    if(not os.path.isfile(pydepjson)):
        os.removedirs(pydepjson)
        writeJsonData(pydepjson, d_dep)
    if(os.path.getsize(pydepjson) < 100):
        writeJsonData(pydepjson, d_dep)

    pydepconfig = readJsonData(pydepjson)

    def check_pydepconfig():
        # hard
        if (pydepconfig.__contains__("PyPi") is False):
            pydepconfig['PyPi'] = d_dep['PyPi']
            writeJsonData(pydepjson, pydepconfig)

        # soft num
        order_of_keys = pydepconfig['PyPi'].keys()
        list_of_tuples = [key for key in order_of_keys]
        if (list_of_tuples.__len__() < 2):
            pydepconfig['PyPi'] = d_dep['PyPi']
            writeJsonData(pydepjson, pydepconfig)

        # soft strip
        tempdict = {}
        for (k, v) in pydepconfig['PyPi'].items():
            current_var = str(k)
            current_var_key = current_var.strip()
            current_var_value = pydepconfig['PyPi'][current_var].strip()
            tempdict[current_var_key] = current_var_value
        pydepconfig['PyPi'] = tempdict

        # soft current
        if (not pydepconfig['PyPi'].__contains__('current')):
            ''
            order_of_keys = pydepconfig['PyPi'].keys()
            list_of_tuples = [key for key in order_of_keys]
            pydepconfig['PyPi']['current'] = list_of_tuples[0]
        if (pydepconfig['PyPi']['current'] == ''):
            ''
            order_of_keys = pydepconfig['PyPi'].keys()
            list_of_tuples = [key for key in order_of_keys]
            pydepconfig['PyPi']['current'] = list_of_tuples[0]

        # move 'current' to be last key
        order_of_keys = pydepconfig['PyPi'].keys()
        list_of_tuples = [key for key in order_of_keys]
        # print(order_of_keys)
        # print(list_of_tuples)
        # print(list_of_tuples[-1])
        if (list_of_tuples[-1] != 'current'):
            # print(".....")
            current_var = pydepconfig['PyPi']['current']
            pydepconfig['PyPi'].__delitem__('current')
            pydepconfig['PyPi']['current'] = current_var
            writeJsonData(pydepjson, pydepconfig)

        # -------------

        # hard
        if (pydepconfig.__contains__("Python") is False):
            pydepconfig['Python'] = d_dep['Python']
            writeJsonData(pydepjson, pydepconfig)

        # soft num
        order_of_keys = pydepconfig['Python'].keys()
        list_of_tuples = [key for key in order_of_keys]
        if (list_of_tuples.__len__() < 2):
            pydepconfig['Python'] = d_dep['Python']
            writeJsonData(pydepjson, pydepconfig)

        # soft strip
        tempdict = {}
        for (k, v) in pydepconfig['Python'].items():
            current_var = str(k)
            current_var_key = current_var.strip()
            current_var_value = pydepconfig['Python'][current_var].strip()
            tempdict[current_var_key] = current_var_value
        pydepconfig['Python'] = tempdict

        # soft current
        if (not pydepconfig['Python'].__contains__('current')):
            ''
            order_of_keys = pydepconfig['Python'].keys()
            list_of_tuples = [key for key in order_of_keys]
            pydepconfig['Python']['current'] = list_of_tuples[0]
        if (pydepconfig['Python']['current'] == ''):
            ''
            order_of_keys = pydepconfig['Python'].keys()
            list_of_tuples = [key for key in order_of_keys]
            pydepconfig['Python']['current'] = list_of_tuples[0]

        # move 'current' to be last key
        order_of_keys = pydepconfig['Python'].keys()
        list_of_tuples = [key for key in order_of_keys]
        # print(order_of_keys)
        # print(list_of_tuples)
        # print(list_of_tuples[-1])
        if (list_of_tuples[-1] != 'current'):
            # print(".....")
            current_var = pydepconfig['Python']['current']
            pydepconfig['Python'].__delitem__('current')
            pydepconfig['Python']['current'] = current_var
            writeJsonData(pydepjson, pydepconfig)

    check_pydepconfig()

    def format_pydepconfig():
        # move 'current' to be last key
        order_of_keys = pydepconfig['PyPi'].keys()
        list_of_tuples = [key for key in order_of_keys]
        # print(order_of_keys)
        # print(list_of_tuples)
        # print(list_of_tuples[-1])
        if (list_of_tuples[-1] != 'current'):
            # print(".....")
            current_var = pydepconfig['PyPi']['current']
            pydepconfig['PyPi'].__delitem__('current')
            pydepconfig['PyPi']['current'] = current_var
            writeJsonData(pydepjson, pydepconfig)

        # move 'current' to be last key
        order_of_keys = pydepconfig['Python'].keys()
        list_of_tuples = [key for key in order_of_keys]
        # print(order_of_keys)
        # print(list_of_tuples)
        # print(list_of_tuples[-1])
        if (list_of_tuples[-1] != 'current'):
            # print(".....")
            current_var = pydepconfig['Python']['current']
            pydepconfig['Python'].__delitem__('current')
            pydepconfig['Python']['current'] = current_var
            writeJsonData(pydepjson, pydepconfig)

    format_pydepconfig()

    # ===============================================================================================
    # pydependent gui work flow
    # -------------------------

    from PyQt5 import uic
    from PyQt5.QtCore import Qt
    from PyQt5.QtCore import QStringListModel, QModelIndex, QItemSelectionModel
    from PyQt5.QtCore import QEvent, QEventLoop, QTimer
    from PyQt5.QtCore import QRect
    from PyQt5.QtCore import QUrl
    from PyQt5.QtGui import QKeyEvent, QWheelEvent
    from PyQt5.QtGui import QIcon
    from PyQt5.QtGui import QPainter, QPen, QColor
    from PyQt5.QtGui import QTextOption, QTextCursor, QTextDocument, QFontMetrics, QFont
    from PyQt5.QtGui import QDrag, QDropEvent
    from PyQt5.QtWidgets import QApplication, QMainWindow, QStatusBar
    from PyQt5.QtWidgets import QListView, QTextEdit, QStyle
    from PyQt5.QtWidgets import QPushButton, QToolButton, QRadioButton, QCheckBox
    from PyQt5.QtWidgets import QWidget, QDialog, QFontDialog
    from PyQt5.QtWidgets import QTableWidget, QTableWidgetItem
    from PyQt5.QtWidgets import QAbstractItemView, QHeaderView
    from PyQt5.QtWidgets import QItemDelegate, QStyledItemDelegate
    # from PyQt5.Qsci import QsciScintilla
    
    class NoFocusDelegate(QStyledItemDelegate):
        def paint(self, QPainter, QStyleOptionViewItem, QModelIndex):
            if (QStyleOptionViewItem.state & QStyle.State_HasFocus):
                QStyleOptionViewItem.state = QStyleOptionViewItem.state ^ QStyle.State_HasFocus
            QStyledItemDelegate.paint(self, QPainter, QStyleOptionViewItem, QModelIndex)

    class Application(QApplication):
        def __init__(self, argv=[]):
            QApplication.setAttribute(Qt.AA_EnableHighDpiScaling)
            QApplication.__init__(self, argv)

    class MainWindow(QMainWindow):
        def __init__(self, parent=None):
            QMainWindow.__init__(self, parent)

            self.setupUi()
            self.setupData()
            self.setupPage()
            self.setupStarting()
            self.showMessage("Ready!GO!")

        # COMMON FUNCTION
        def eventFilter(self, watched, event):
            ''
            if (event.type() == QEvent.Paint):
                return QMainWindow.eventFilter(self, watched, event)

            if(watched == self.tableWidgetPyPi):
                if(event.type() == QEvent.KeyRelease):
                    if(event.key() == Qt.Key_S):
                        if(event.modifiers() & Qt.ControlModifier == Qt.ControlModifier):
                            #print(watched, event)

                            # CTRL+SHIFT+S
                            if (event.modifiers() & Qt.ShiftModifier == Qt.ShiftModifier):
                                self.onPushButtonSavePyPiForCurrentUserClicked()
                                return False

                            #CTRL+S
                            self.onPushButtonSavePyPiClicked()
                            return False
            elif (watched == self.tableWidgetPython):
                if (event.type() == QEvent.KeyRelease):
                    if (event.key() == Qt.Key_S):
                        if (event.modifiers() & Qt.ControlModifier == Qt.ControlModifier):
                            # print(watched, event)

                            # CTRL+S
                            self.onPushButtonSavePythonClicked()
                            return False
            elif (watched == self.textEditDependentPackage):
                if (event.type() == QEvent.Wheel):
                    #print(event.type(), event.key(), event.modifiers())
                    if (event.modifiers() & Qt.ControlModifier == Qt.ControlModifier):
                        # print(watched, event)

                        # CTRL+Wheel
                        #self.onPushButtonSavePythonClicked()
                        delta = event.angleDelta()
                        #delta1 = event.pixelDelta()
                        #print(delta, delta1)
                        if(delta.y() < 0):
                            self.textEditDependentPackage.zoomOut(2)
                        else:
                            self.textEditDependentPackage.zoomIn(2)
                        return False

            return QMainWindow.eventFilter(self, watched, event)

        def dragEnterEvent(self, event):
            #print('aaaa')
            if (event.mimeData().hasFormat("text/uri-list")):
                event.acceptProposedAction()
            #print('bbbbb')

        def dropEvent(self, event):
            #print('ccccc')

            if (event.mimeData().hasFormat("text/uri-list")):
                #print('ddddd')
                urls = event.mimeData().urls()
                #print('eeeee')
                if (urls.__len__() <= 0):
                    return
                #print('fffff')

                for url in urls:
                    fileName = url.toLocalFile()
                    #print('ggggg')
                    if (fileName.__len__() <= 0):
                        return

                    if(os.path.exists(fileName) and os.path.isfile(fileName)):
                        if(os.path.getsize(fileName) > 10*1024*1024):
                            self.showMessage(str('Dependent Package: %s is too large, cant parse.' % fileName))
                            return

                        # append
                        datas = ""
                        with open(fileName, 'r', encoding='utf8') as f:
                            for line in f.readlines():
                                datas += line
                        self.textEditDependentPackage.append(datas)
                        self.tabWidget.setCurrentIndex(self.indexDependentPackage)
                        self.showMessage(str('Dependent Package: append package in %s.' % fileName))

        def setupUi(self):
            pymaketooluipathfile = pymaketoolfileroot + os.path.sep + "mainwindow.ui"
            uic.loadUi(pymaketooluipathfile, self)

            self.pushButtonBackStep.clicked.connect(self.onPushButtonPreviousStepClicked)
            self.pushButtonForwardStep.clicked.connect(self.onPushButtonNextStepClicked)

            self.pushButtonRefreshPyPi.clicked.connect(self.onPushButtonRefreshPyPiClicked)
            self.pushButtonInitialPyPi.clicked.connect(self.onPushButtonInitialPyPiClicked)
            self.pushButtonAddPyPi.clicked.connect(self.onPushButtonAddPyPiClicked)
            self.pushButtonDelPyPi.clicked.connect(self.onPushButtonDelPyPiClicked)
            self.pushButtonSavePyPi.clicked.connect(self.onPushButtonSavePyPiClicked)

            self.pushButtonRefreshPython.clicked.connect(self.onPushButtonRefreshPythonClicked)
            self.pushButtonFindPython.clicked.connect(self.onPushButtonFindPythonClicked)
            self.pushButtonRefreshPyLauncher.clicked.connect(self.onPushButtonRefreshPyLauncherClicked)
            self.pushButtonInitialPython.clicked.connect(self.onPushButtonInitialPythonClicked)
            self.pushButtonAddPython.clicked.connect(self.onPushButtonAddPythonClicked)
            self.pushButtonDelPython.clicked.connect(self.onPushButtonDelPythonClicked)
            self.pushButtonSavePython.clicked.connect(self.onPushButtonSavePythonClicked)

            self.pushButtonRefreshSystem.clicked.connect(self.onPushButtonRefreshSystemClicked)

            self.pushButtonRefreshExistedPackage.clicked.connect(self.onPushButtonRefreshExistedPackageClicked)
            self.pushButtonSavePyPiForCurrentUser.clicked.connect(self.onPushButtonSavePyPiForCurrentUserClicked)

            self.pushButtonRefreshCommand.clicked.connect(self.onPushButtonRefreshCommandClicked)
            self.pushButtonStartInstall.clicked.connect(self.onPushButtonStartInstallClicked)

            self.pushButtonRefreshRequire.clicked.connect(self.onPushButtonRefreshRequireClicked)
            self.pushButtonSaveRequire.clicked.connect(self.onPushButtonSaveRequireClicked)

            self.tableWidgetPython.setColumnCount(2)
            self.tableWidgetPython.setRowCount(4)
            self.tableWidgetPython.setHorizontalHeaderLabels(('Python', 'Path'))
            self.tableWidgetPython.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeToContents)
            #self.tableWidgetPython.horizontalHeader().setSectionResizeMode(1, QHeaderView.Stretch)
            self.tableWidgetPython.horizontalHeader().setStretchLastSection(True)
            self.tableWidgetPython.setSelectionMode(QAbstractItemView.SingleSelection)
            self.tableWidgetPython.setSelectionBehavior(QAbstractItemView.SelectRows)
            #self.tableWidget.setEditTriggers(QAbstractItemView.NoEditTriggers)  # 禁止编辑
            #self.tableWidgetPython.horizontalHeader().setDefaultSectionSize(200)
            #self.tableWidgetPython.verticalHeader().setDefaultSectionSize(300)
            #self.tableWidgetPython.horizontalHeader().resizeSection(0, 300)
            #self.tableWidgetPython.verticalHeader().resizeSection(0, 500)
            #self.tableWidgetPython.verticalHeader().setVisible(False)
            #self.tableWidgetPython.horizontalHeader().setVisible(False)
            #self.tableWidgetPython.setItemDelegate(NoFocusDelegate())


            self.tableWidgetPyPi.setColumnCount(2)
            self.tableWidgetPyPi.setRowCount(4)
            self.tableWidgetPyPi.setHorizontalHeaderLabels(('PyPi', 'Url'))
            self.tableWidgetPyPi.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeToContents)
            self.tableWidgetPyPi.horizontalHeader().setStretchLastSection(True)
            self.tableWidgetPyPi.setSelectionMode(QAbstractItemView.SingleSelection)
            self.tableWidgetPyPi.setSelectionBehavior(QAbstractItemView.SelectRows)
            #self.tableWidgetPyPi.setItemDelegate(NoFocusDelegate())

            self.textEditExistedPackage.setReadOnly(True)
            self.textEditExistedPackage.setWordWrapMode(QTextOption.NoWrap)

            # self.textEditDependentPackage.setReadOnly(True)
            self.textEditDependentPackage.setWordWrapMode(QTextOption.NoWrap)
            self.textEditDependentPackage.setAcceptRichText(False)
            self.textEditDependentPackage.setAcceptDrops(False)
            self.setAcceptDrops(True)
            self.textEditDependentPackage.installEventFilter(self)

            self.textEditInstallPackage.setReadOnly(True)
            #self.textEditInstallPackage.setWordWrapMode(QTextOption.NoWrap)
            self.textEditInstallPackage.setWordWrapMode(QTextOption.WrapAnywhere)
            self.textEditInstallPackage.setLineWrapMode(QTextEdit.WidgetWidth)

            # self.tableWidgetPyPi.clicked.connect(self.onTableWidgetPyPiClicked)
            # self.tableWidgetPython.clicked.connect(self.onTableWidgetPythonClicked)
            self.tableWidgetPyPi.selectionModel().currentRowChanged.connect(self.onTableWidgetPyPiClicked)
            self.tableWidgetPython.selectionModel().currentRowChanged.connect(self.onTableWidgetPythonClicked)

            #self.widget_8.setHidden(True)

            while (False):
                ok = True
                nf, ok = QFontDialog.getFont()
                if (ok is True):
                    # self.textEditQMakeList.setFont(nf)
                    print("family:", nf.family(), " point:", nf.pointSize(), " weight:", nf.weight(), "italic:", nf.italic())

                nf = QApplication.font()
                print(nf.family(), nf.pointSize(), nf.weight(), nf.italic())
                break

            while (True):
                plat = getplatform()
                if(plat == 'Windows'):
                    ''
                elif(plat == 'Darwin'):
                    ''
                    nf = QFont('SimHei', 13)
                    self.textEditExistedPackage.setFont(nf)
                    #self.textEditDependentPackage.setFont(nf)
                    self.textEditInstallPackage.setFont(nf)
                    self.textEditPythonSummary.setFont(nf)
                else:
                    nf = QFont('AR PL UMing CN', 13)
                    self.textEditExistedPackage.setFont(nf)
                    #self.textEditDependentPackage.setFont(nf)
                    self.textEditInstallPackage.setFont(nf)
                    self.textEditPythonSummary.setFont(nf)
                break

        def setupData(self):
            ''
            for package in pydeprequirepackagelist:
                self.textEditDependentPackage.append(package)

            self.refreshtimer = QTimer(self)
            self.refreshtimer.timeout.connect(self.onRefreshTimer)
            self.refreshtimer.setSingleShot(True)
            self.refreshtimer.setInterval(800)
            # self.refreshtimer.start()

            self.refreshlanuchertimer = QTimer(self)
            self.refreshlanuchertimer.timeout.connect(self.onRefreshPyLauncherTimer)
            self.refreshlanuchertimer.setSingleShot(True)
            self.refreshlanuchertimer.setInterval(700)
            # self.refreshlanuchertimer.start()

            self.refreshpythontimer = QTimer(self)
            self.refreshpythontimer.timeout.connect(self.onRefreshPython3Timer)
            self.refreshpythontimer.timeout.connect(self.onRefreshPythonTimer)
            self.refreshpythontimer.setSingleShot(True)
            self.refreshpythontimer.setInterval(600)
            # self.refreshpythontimer.start()


            self.refreshsummarytimer = QTimer(self)
            self.refreshsummarytimer.timeout.connect(self.onRefreshSummaryTimer)
            self.refreshsummarytimer.setSingleShot(True)
            self.refreshsummarytimer.setInterval(400)
            # self.refreshsummarytimer.start()

            self.pythonDictToTableWidget()
            self.pypiDictToTableWidget()

        def setupPage(self):
            ''

            self.indexPyPi                  = 0
            self.indexPython                = 1
            self.indexSummary               = 2
            self.indexExistedPackage        = 3
            self.indexDependentPackage      = 4
            self.indexInstallPackage        = 4
            self.indexReadme                = 5

            self.tabWidget.removeTab(self.indexDependentPackage)

        def setupStarting(self):
            ''
            self.tabWidget.setCurrentIndex(self.indexDependentPackage)
            # self.tableWidgetPython.setFocus()

        def showMessage(self, message):
            ''
            self.statusBar.showMessage(message)

            sep = "%Y-%m-%d %H:%M:%S.%f"
            tuple_time = datetime.datetime.now()
            str_time = tuple_time.strftime(sep)
            str_time = str_time[0:23]
            self.textEditInstallPackage.append(str('[%-23s] %s' % (str_time, message)))
            end_value = self.textEditInstallPackage.verticalScrollBar().maximum()
            self.textEditInstallPackage.verticalScrollBar().setValue(end_value)

        def onPushButtonPreviousStepClicked(self):
            ''
            count = self.tabWidget.count()
            index = self.tabWidget.currentIndex()
            index = (index-1)%count
            self.tabWidget.setCurrentIndex(index)

        def onPushButtonNextStepClicked(self):
            ""
            count = self.tabWidget.count()
            index = self.tabWidget.currentIndex()
            index = (index+1)%count
            self.tabWidget.setCurrentIndex(index)


        # pypi configure
        def pypiDictToTableWidget(self):
            ''
            while(self.tableWidgetPyPi.rowCount()>0):
                self.tableWidgetPyPi.removeRow(0)
            self.tableWidgetPyPi.setRowCount(0)

            current_var = pydepconfig['PyPi']['current']
            self.labelCurrentPyPi.setText(pydepconfig['PyPi'][current_var])

            current_row = 0
            count = 0
            for (k, v) in pydepconfig['PyPi'].items():
                if(str(k) == 'current'):
                    continue

                if(str(k) == current_var):
                    current_row = count

                self.tableWidgetPyPi.insertRow(count)
                self.tableWidgetPyPi.setItem(count, 0, QTableWidgetItem(str(k)))
                self.tableWidgetPyPi.setItem(count, 1, QTableWidgetItem(str(v)))
                count = count + 1

            #A
            pythonIndex = QModelIndex()
            pythonIndex = self.tableWidgetPyPi.model().index( current_row, 0 )
            current_item = self.tableWidgetPyPi.itemFromIndex(pythonIndex)
            self.tableWidgetPyPi.setCurrentItem(current_item)
            #self.tableWidgetPyPi.selectionMode().setCurrentIndex(pythonIndex)
            #B
            self.tableWidgetPyPi.setCurrentItem(self.tableWidgetPyPi.item(current_row, 0))
            self.tableWidgetPyPi.update()
            #C
            self.tableWidgetPyPi.selectRow(current_row)

            current_row = self.tableWidgetPyPi.currentRow()
            if(current_row < 0):
                self.showMessage('Python PyPi: select item fail, row:%s' % current_row)
                self.showMessage('Python PyPi: refresh pydepconfig to pypi page fail.')
                return
            current_item_key = self.tableWidgetPyPi.item(current_row, 0).text()
            current_item_value = self.tableWidgetPyPi.item(current_row, 1).text()
            self.showMessage('Python PyPi: select item %s:%s' % (current_item_key,current_item_value))
            self.showMessage('Python PyPi: refresh pydepconfig to pypi page success.')

        def tableWidgetToPyPiDict(self):
            ''
            ''
            current_var = pydepconfig['PyPi']['current']
            current_python = pydepconfig['PyPi'][current_var]

            current_row = self.tableWidgetPyPi.currentRow()
            if(current_row < 0):
                self.showMessage('Python PyPi: pypi page is empty, save fail.')
                return
            current_item_key = self.tableWidgetPyPi.item(current_row, 0).text()
            current_item_value = self.tableWidgetPyPi.item(current_row, 1).text()

            current_python = current_item_value
            # print(current_item_key, current_item_value)

            if (current_item_key == 'current'):
                self.showMessage('Python PyPi: current item cant be "current", save fail.')
                return

            if (current_item_key == ''):
                self.showMessage('Python PyPi: current item key cant be empty, save fail.')
                return

            if (current_item_value == ''):
                self.showMessage('Python PyPi: current item value cant be empty, save fail.')
                return

            pydepconfig['PyPi'] = {}
            pydepconfig['PyPi']['current'] = current_item_key

            rowCount = self.tableWidgetPyPi.rowCount()
            count = 0
            while (count < rowCount):
                ''
                current_row = count
                current_item_key = self.tableWidgetPyPi.item(current_row, 0).text()
                current_item_value = self.tableWidgetPyPi.item(current_row, 1).text()

                current_python = current_item_value
                # print(current_item_key, current_item_value)

                count = count + 1

                if(current_item_key == 'current'):
                    continue

                if(current_item_key == ''):
                    continue

                if(current_item_value == ''):
                    continue

                pydepconfig['PyPi'][current_item_key] = current_item_value

            check_pydepconfig()

            writeJsonData(pydepjson, pydepconfig)

            self.showMessage('Python PyPi: save pypi page to pydepconfig and save to file success.')

        def onPushButtonInitialPyPiClicked(self):
            ''
            while(self.tableWidgetPyPi.rowCount()>0):
                self.tableWidgetPyPi.removeRow(0)
            self.tableWidgetPyPi.setRowCount(0)

            current_var = d_dep['PyPi']['current']
            self.labelCurrentPyPi.setText(d_dep['PyPi'][current_var])

            current_row = 0
            count = 0
            for (k, v) in d_dep['PyPi'].items():
                if(str(k) == 'current'):
                    continue

                if(str(k) == current_var):
                    current_row = count

                self.tableWidgetPyPi.insertRow(count)
                self.tableWidgetPyPi.setItem(count, 0, QTableWidgetItem(str(k)))
                self.tableWidgetPyPi.setItem(count, 1, QTableWidgetItem(str(v)))
                count = count + 1

            #A
            pythonIndex = QModelIndex()
            pythonIndex = self.tableWidgetPyPi.model().index( current_row, 0 )
            current_item = self.tableWidgetPyPi.itemFromIndex(pythonIndex)
            self.tableWidgetPyPi.setCurrentItem(current_item)
            #self.tableWidgetPyPi.selectionMode().setCurrentIndex(pythonIndex)
            #B
            self.tableWidgetPyPi.setCurrentItem(self.tableWidgetPyPi.item(current_row, 0))
            self.tableWidgetPyPi.update()
            #C
            self.tableWidgetPyPi.selectRow(current_row)

            self.showMessage('Python PyPi: initial pypi page success.')

        def onPushButtonAddPyPiClicked(self):
            ''

            current_row = self.tableWidgetPyPi.currentRow()
            # current_item_key = self.tableWidgetPyPi.item(current_row, 0).text()
            # current_item_value = self.tableWidgetPyPi.item(current_row, 1).text()

            # current_python = current_item_value
            # print(current_item_key, current_item_value)
            # print(current_row)
            current_row = current_row+1
            self.tableWidgetPyPi.insertRow(current_row)
            self.tableWidgetPyPi.setItem(current_row, 0, QTableWidgetItem(''))
            self.tableWidgetPyPi.setItem(current_row, 1, QTableWidgetItem(''))
            self.tableWidgetPyPi.selectRow(current_row)

            self.showMessage('Python PyPi: add one item, row:%s.' % current_row)

        def onPushButtonDelPyPiClicked(self):
            ''

            current_row = self.tableWidgetPyPi.currentRow()
            # current_item_key = self.tableWidgetPyPi.item(current_row, 0).text()
            # current_item_value = self.tableWidgetPyPi.item(current_row, 1).text()

            # current_python = current_item_value
            # print(current_item_key, current_item_value)
            # print(current_row)

            self.tableWidgetPyPi.removeRow(current_row)
            self.tableWidgetPyPi.selectRow(current_row-1)

            self.showMessage('Python PyPi: del one item, row:%s.' % current_row)

        def onPushButtonSavePyPiClicked(self):
            ''
            self.tableWidgetToPyPiDict()
            self.showMessage('Python PyPi: save pypi page to file finished.')

        def onPushButtonSavePyPiForCurrentUserClicked(self):
            ''
            self.onPushButtonSavePyPiClicked()

            current_row = self.tableWidgetPyPi.currentRow()
            if(current_row < 0):
                self.showMessage('Python PyPi: save pypi page for current user fail, empty pypi table.')
                return
            current_item_key = self.tableWidgetPyPi.item(current_row, 0).text()
            current_item_value = self.tableWidgetPyPi.item(current_row, 1).text()

            current_python = current_item_value
            # print(current_item_key, current_item_value)
            # print(current_row)


            pipini = ''
            plat = getplatform()
            if (plat == "Windows"):
                piproot = getuserroot() + os.path.sep + 'pip'
                if (not os.path.exists(piproot)):
                    os.mkdir(piproot)
                pipini = piproot + os.path.sep + 'pip.ini'
            else:
                piproot = getuserroot() + os.path.sep + '.pip'
                if (not os.path.exists(piproot)):
                    os.mkdir(piproot)
                pipini = piproot + os.path.sep + 'pip.conf'

            conf = MyConfigParser()
            conf.read(pipini)
            if (not conf.has_section('global')):
                conf.add_section('global')
                conf.write(open(pipini, 'w'))
            if (not conf.has_section('install')):
                conf.add_section('install')
                conf.write(open(pipini, 'w'))
            if (not conf.has_option('global', 'index-url')):
                conf.set('global', 'index-url', 'https://pypi.tuna.tsinghua.edu.cn/simple')
                conf.write(open(pipini, 'w'))
            if (not conf.has_option('install', 'trusted-host')):
                conf.set('install', 'trusted-host', 'mirrors.aliyun.com')
                conf.write(open(pipini, 'w'))

            conf.set('global', 'index-url', current_python)
            conf.set('install', 'trusted-host', 'mirrors.aliyun.com')
            conf.write(open(pipini, 'w'))

            self.refreshsummarytimer.start()
            self.showMessage('Python PyPi: save pypi page for current user finished.')

        def onTableWidgetPyPiClicked(self, index):
            ''
            if(not index.isValid()):
                return

            current_row = index.row()
            if(current_row < 0):
                self.showMessage('Python PyPi: select fail, row:%s.' % current_row)
                return
            current_item_key = self.tableWidgetPyPi.item(current_row, 0).text()
            current_item_value = self.tableWidgetPyPi.item(current_row, 1).text()

            current_python = current_item_value
            # print(current_item_key, current_item_value)
            # print(current_row)

            self.labelCurrentPyPi.setText(current_python)

            self.refreshsummarytimer.start()
            self.showMessage('Python PyPi: select pypi %s:%s.' % (current_item_key, current_item_value))

        def onPushButtonRefreshPyPiClicked(self):
            ''
            self.pypiDictToTableWidget()
            self.showMessage('Python PyPi: refresh pypi page from pydepconfig finished.')


        # python program
        def onRefreshPyLauncherTimer(self):
            ''
            plat = getplatform()
            if (plat == 'Windows'):
                pass
            else:
                return

            cmd_string = str('call "%s" %s' % ('py', '--list-paths'))
            plat = getplatform()
            if (plat == 'Windows'):
                pass
            else:
                cmd_string = str('"%s" %s' % ('which', 'python3'))
            # print("", cmd_string)
            result = subprocess.getoutput(cmd_string)
            # print("", result)

            pylauncher_dict = {}
            pylauncher_dict['Py Launcher'] = os.path.join(os.environ['WINDIR'], 'py.exe')

            count = 0
            for line in result.split('\n'):

                # filter line 0
                if(count == 0):
                    count = count + 1
                    continue

                count=count+1

                line = line.strip()
                if(line == ''):
                    continue

                # line = line + '\t*'
                line = line.replace('\t', ' ')

                # default python, named Py Launcher
                if(str(line).endswith('*')):
                    ''
                    # pyname = 'Py Launcher'
                    key = str(line).split(' ')[0].strip()
                    value = ' '.join(str(line).split(' ')[1:-1]).strip()
                    # print('Default:', key, value)
                    # pylauncher_dict[pyname] = value
                    pylauncher_dict[key] = value
                    continue

                key = str(line).split(' ')[0].strip()
                value = ' '.join(str(line).split(' ')[1:]).strip()
                # print(key, value)
                pylauncher_dict[key] = value

            def find_python(key, value):
                rowCount = self.tableWidgetPython.rowCount()
                count = 0
                while (count < rowCount):
                    ''
                    current_row = count
                    current_item_key = self.tableWidgetPython.item(current_row, 0).text()
                    current_item_value = self.tableWidgetPython.item(current_row, 1).text()

                    current_python = current_item_value
                    # print(current_item_key, current_item_value)

                    count = count + 1

                    if (current_item_key == 'current'):
                        continue

                    if (current_item_key == ''):
                        continue

                    if (current_item_value == ''):
                        continue

                    # pydepconfig['Python'][current_item_key] = current_item_value
                    if(current_item_key.strip() == key.strip()):
                        return current_row

                return -1

            # print(pylauncher_dict)

            for (k, v) in pylauncher_dict.items():
                # <0 add
                # >=0 replace
                current_row = find_python(k, v)
                # print(k, v, current_row)
                if(current_row >= 0):
                    self.tableWidgetPython.setItem(current_row, 0, QTableWidgetItem(str(k)))
                    self.tableWidgetPython.setItem(current_row, 1, QTableWidgetItem(str(v)))
                    continue

                current_row = self.tableWidgetPython.rowCount()
                self.tableWidgetPython.insertRow(current_row)
                self.tableWidgetPython.setItem(current_row, 0, QTableWidgetItem(str(k)))
                self.tableWidgetPython.setItem(current_row, 1, QTableWidgetItem(str(v)))

            self.showMessage('Python Program: refresh py launcher information to python page success.')

        def onRefreshPython3Timer(self):
            ''
            cmd_string = str('call "%s" %s' % ('where', 'python3'))
            plat = getplatform()
            if (plat == 'Windows'):
                pass
            else:
                cmd_string = str('"%s" %s' % ('which', 'python3'))
            # print("", cmd_string)
            result = subprocess.getoutput(cmd_string)
            # print("", result)

            pyname = 'Python3-'
            if (plat == 'Windows'):
                pass
            else:
                pyname = 'Python3-'
            pylauncher_dict = {}
            count = 0
            for line in result.split('\n'):
                count=count+1

                line = line.strip()
                if(line == ''):
                    continue

                # line = line + '\t*'

                key = pyname+str(count)
                value = line
                # print(key, value)
                pylauncher_dict[key] = value

            def find_python(key, value):
                rowCount = self.tableWidgetPython.rowCount()
                count = 0
                while (count < rowCount):
                    ''
                    current_row = count
                    current_item_key = self.tableWidgetPython.item(current_row, 0).text()
                    current_item_value = self.tableWidgetPython.item(current_row, 1).text()

                    current_python = current_item_value
                    # print(current_item_key, current_item_value)

                    count = count + 1

                    if (current_item_key == 'current'):
                        continue

                    if (current_item_key == ''):
                        continue

                    if (current_item_value == ''):
                        continue

                    # pydepconfig['Python'][current_item_key] = current_item_value
                    if(current_item_key.strip() == key.strip()):
                        return current_row

                return -1

            # print(pylauncher_dict)

            for (k, v) in pylauncher_dict.items():
                # <0 add
                # >=0 replace
                current_row = find_python(k, v)
                # print(k, v, current_row)
                if(current_row >= 0):
                    self.tableWidgetPython.setItem(current_row, 0, QTableWidgetItem(str(k)))
                    self.tableWidgetPython.setItem(current_row, 1, QTableWidgetItem(str(v)))
                    continue

                current_row = self.tableWidgetPython.rowCount()
                self.tableWidgetPython.insertRow(current_row)
                self.tableWidgetPython.setItem(current_row, 0, QTableWidgetItem(str(k)))
                self.tableWidgetPython.setItem(current_row, 1, QTableWidgetItem(str(v)))

            self.showMessage('Python Program: refresh python3 to python page success.')

        def onRefreshPythonTimer(self):
            ''
            cmd_string = str('call "%s" %s' % ('where', 'python'))
            plat = getplatform()
            if (plat == 'Windows'):
                pass
            else:
                cmd_string = str('"%s" %s' % ('which', 'python'))
            # print("", cmd_string)
            result = subprocess.getoutput(cmd_string)
            # print("", result)

            pyname = 'Python-'
            if (plat == 'Windows'):
                pass
            else:
                pyname = 'Python-'
            pylauncher_dict = {}
            count = 0
            for line in result.split('\n'):
                count = count + 1

                line = line.strip()
                if (line == ''):
                    continue

                # line = line + '\t*'

                key = pyname + str(count)
                value = line
                # print(key, value)
                pylauncher_dict[key] = value

            def find_python(key, value):
                rowCount = self.tableWidgetPython.rowCount()
                count = 0
                while (count < rowCount):
                    ''
                    current_row = count
                    current_item_key = self.tableWidgetPython.item(current_row, 0).text()
                    current_item_value = self.tableWidgetPython.item(current_row, 1).text()

                    current_python = current_item_value
                    # print(current_item_key, current_item_value)

                    count = count + 1

                    if (current_item_key == 'current'):
                        continue

                    if (current_item_key == ''):
                        continue

                    if (current_item_value == ''):
                        continue

                    # pydepconfig['Python'][current_item_key] = current_item_value
                    if (current_item_key.strip() == key.strip()):
                        return current_row

                return -1

            # print(pylauncher_dict)

            for (k, v) in pylauncher_dict.items():
                # <0 add
                # >=0 replace
                current_row = find_python(k, v)
                # print(k, v, current_row)
                if (current_row >= 0):
                    self.tableWidgetPython.setItem(current_row, 0, QTableWidgetItem(str(k)))
                    self.tableWidgetPython.setItem(current_row, 1, QTableWidgetItem(str(v)))
                    continue

                current_row = self.tableWidgetPython.rowCount()
                self.tableWidgetPython.insertRow(current_row)
                self.tableWidgetPython.setItem(current_row, 0, QTableWidgetItem(str(k)))
                self.tableWidgetPython.setItem(current_row, 1, QTableWidgetItem(str(v)))

            self.showMessage('Python Program: refresh python information to python program page success.')

        def pythonDictToTableWidget(self):
            ''
            while (self.tableWidgetPython.rowCount() > 0):
                self.tableWidgetPython.removeRow(0)
            self.tableWidgetPython.setRowCount(0)

            current_var = pydepconfig['Python']['current']
            self.labelCurrentPython.setText(pydepconfig['Python'][current_var])

            current_row = 0
            count = 0
            for (k, v) in pydepconfig['Python'].items():
                if (str(k) == 'current'):
                    continue

                if (str(k) == current_var):
                    current_row = count

                self.tableWidgetPython.insertRow(count)
                self.tableWidgetPython.setItem(count, 0, QTableWidgetItem(str(k)))
                self.tableWidgetPython.setItem(count, 1, QTableWidgetItem(str(v)))
                count = count + 1

            # A
            pythonIndex = QModelIndex()
            pythonIndex = self.tableWidgetPython.model().index(current_row, 0)
            current_item = self.tableWidgetPython.itemFromIndex(pythonIndex)
            self.tableWidgetPython.setCurrentItem(current_item)
            # self.tableWidgetPython.selectionMode().setCurrentIndex(pythonIndex)
            # B
            self.tableWidgetPython.setCurrentItem(self.tableWidgetPython.item(current_row, 0))
            self.tableWidgetPython.update()
            # C
            self.tableWidgetPython.selectRow(current_row)

            current_row = self.tableWidgetPython.currentRow()
            if(current_row < 0):
                self.showMessage('Python Program: select item fail, row:%s' % current_row)
                self.showMessage('Python Program: pydepconfig python is empty configure, please fix it manully.')
                return
            current_item_key = self.tableWidgetPython.item(current_row, 0).text()
            current_item_value = self.tableWidgetPython.item(current_row, 1).text()
            self.showMessage('Python Program: select item %s %s.' % (current_item_key,current_item_value))
            self.showMessage('Python Program: refresh pydepconfig to python program page success.')

        def tableWidgetToPythonDict(self):
            ''
            current_var = pydepconfig['Python']['current']
            current_python = pydepconfig['Python'][current_var]

            current_row = self.tableWidgetPython.currentRow()
            if (current_row < 0):
                self.showMessage('Python Program: python program is empty table, can save any item.')
                return
            current_item_key = self.tableWidgetPython.item(current_row, 0).text()
            current_item_value = self.tableWidgetPython.item(current_row, 1).text()

            current_python = current_item_value
            # print(current_item_key, current_item_value)

            if (current_item_key == 'current'):
                return

            if (current_item_key == ''):
                return

            if (current_item_value == ''):
                return

            pydepconfig['Python'] = {}
            pydepconfig['Python']['current'] = current_item_key

            rowCount = self.tableWidgetPython.rowCount()
            count = 0
            while (count < rowCount):
                ''
                current_row = count
                current_item_key = self.tableWidgetPython.item(current_row, 0).text()
                current_item_value = self.tableWidgetPython.item(current_row, 1).text()

                current_python = current_item_value
                # print(current_item_key, current_item_value)

                count = count + 1

                if (current_item_key == 'current'):
                    continue

                if (current_item_key == ''):
                    continue

                if (current_item_value == ''):
                    continue

                pydepconfig['Python'][current_item_key] = current_item_value

            check_pydepconfig()

            writeJsonData(pydepjson, pydepconfig)
            self.showMessage('Python Program: save python page to pydepconfig and save to file success.')

        def onPushButtonInitialPythonClicked(self):
            ''
            while(self.tableWidgetPython.rowCount()>0):
                self.tableWidgetPython.removeRow(0)
            self.tableWidgetPython.setRowCount(0)

            current_var = d_dep['Python']['current']
            self.labelCurrentPython.setText(d_dep['Python'][current_var])

            current_row = 0
            count = 0
            for (k, v) in d_dep['Python'].items():
                if(str(k) == 'current'):
                    continue

                if(str(k) == current_var):
                    current_row = count

                self.tableWidgetPython.insertRow(count)
                self.tableWidgetPython.setItem(count, 0, QTableWidgetItem(str(k)))
                self.tableWidgetPython.setItem(count, 1, QTableWidgetItem(str(v)))
                count = count + 1

            #A
            pythonIndex = QModelIndex()
            pythonIndex = self.tableWidgetPython.model().index( current_row, 0 )
            current_item = self.tableWidgetPython.itemFromIndex(pythonIndex)
            self.tableWidgetPython.setCurrentItem(current_item)
            #self.tableWidgetPython.selectionMode().setCurrentIndex(pythonIndex)
            #B
            self.tableWidgetPython.setCurrentItem(self.tableWidgetPython.item(current_row, 0))
            self.tableWidgetPython.update()
            #C
            self.tableWidgetPython.selectRow(current_row)
            self.showMessage('Python Program: initial python program to default configure success.')

        def onPushButtonAddPythonClicked(self):
            ''
            current_row = self.tableWidgetPython.currentRow()
            # current_item_key = self.tableWidgetPython.item(current_row, 0).text()
            # current_item_value = self.tableWidgetPython.item(current_row, 1).text()

            # current_python = current_item_value
            # print(current_item_key, current_item_value)
            # print(current_row)
            current_row = current_row+1
            self.tableWidgetPython.insertRow(current_row)
            self.tableWidgetPython.setItem(current_row, 0, QTableWidgetItem(''))
            self.tableWidgetPython.setItem(current_row, 1, QTableWidgetItem(''))
            self.tableWidgetPython.selectRow(current_row)
            self.showMessage('Python Program: add an item, row:%s.' % current_row)

        def onPushButtonDelPythonClicked(self):
            ''
            current_row = self.tableWidgetPython.currentRow()
            # current_item_key = self.tableWidgetPython.item(current_row, 0).text()
            # current_item_value = self.tableWidgetPython.item(current_row, 1).text()

            # current_python = current_item_value
            # print(current_item_key, current_item_value)
            # print(current_row)

            self.tableWidgetPython.removeRow(current_row)
            self.tableWidgetPython.selectRow(current_row-1)
            self.showMessage('Python Program: del an item, row:%s.' % current_row)

        def onPushButtonSavePythonClicked(self):
            ''
            self.tableWidgetToPythonDict()
            self.showMessage('Python Program: save python page to file finished.')

        def onTableWidgetPythonClicked(self, index):
            ''
            if(not index.isValid()):
                return

            current_row = index.row()
            if(current_row < 0):
                return
            current_item_key = self.tableWidgetPython.item(current_row, 0).text()
            current_item_value = self.tableWidgetPython.item(current_row, 1).text()

            current_python = current_item_value
            # print(current_item_key, current_item_value)
            # print(current_row)

            self.labelCurrentPython.setText(current_python)

            self.refreshtimer.start()
            self.refreshsummarytimer.start()
            self.showMessage('Python Program: select item %s : %s.' % (current_item_key, current_item_value))

        def onPushButtonRefreshPyLauncherClicked(self):
            ''
            self.refreshlanuchertimer.start()
            self.showMessage('Python Program: refresh py launcher finished.')

        def onPushButtonFindPythonClicked(self):
            ''
            self.refreshpythontimer.start()
            self.showMessage('Python Program: refresh find python finished.')

        def onPushButtonRefreshPythonClicked(self):
            ''
            self.pythonDictToTableWidget()
            self.showMessage('Python Program: refresh pydepconfig to python page finished.')


        # python summary
        def onRefreshSummaryTimer(self):
            ''
            self.textEditPythonSummary.clear()

            pipini = ''
            plat = getplatform()
            if (plat == "Windows"):
                piproot = getuserroot() + os.path.sep + 'pip'
                if (not os.path.exists(piproot)):
                    os.mkdir(piproot)
                pipini = piproot + os.path.sep + 'pip.ini'
            else:
                piproot = getuserroot() + os.path.sep + '.pip'
                if (not os.path.exists(piproot)):
                    os.mkdir(piproot)
                pipini = piproot + os.path.sep + 'pip.conf'

            sys_pypi_name = 'Official PyPi'
            sys_pypi = 'https://pypi.python.org/simple'
            conf = MyConfigParser()
            if(os.path.exists(pipini)):
                conf.read(pipini)
                if (conf.has_section('global')):
                    if (conf.has_option('global', 'index-url')):
                        sys_pypi_name = 'Custom PyPi'
                        sys_pypi = conf['global']['index-url']

            def get_pylauncher_python():
                plat = getplatform()
                if (plat == 'Windows'):
                    pass
                else:
                    return

                cmd_string = str('call "%s" %s' % ('py', '--list-paths'))
                plat = getplatform()
                if (plat == 'Windows'):
                    pass
                else:
                    cmd_string = str('"%s" %s' % ('which', 'python3'))
                # print("", cmd_string)
                result = subprocess.getoutput(cmd_string)
                # print("", result)

                pylauncher_dict = {}
                pylauncher_dict['Py Launcher'] = os.path.join(os.environ['WINDIR'], 'py.exe')

                count = 0
                for line in result.split('\n'):

                    # filter line 0
                    if (count == 0):
                        count = count + 1
                        continue

                    count = count + 1

                    line = line.strip()
                    if (line == ''):
                        continue

                    # line = line + '\t*'
                    line = line.replace('\t', ' ')

                    # default python, named Py Launcher
                    if (str(line).endswith('*')):
                        ''
                        # pyname = 'Py Launcher'
                        key = str(line).split(' ')[0].strip()
                        value = ' '.join(str(line).split(' ')[1:-1]).strip()
                        # print('Default:', key, value)
                        # pylauncher_dict[pyname] = value
                        pylauncher_dict[key] = value
                        continue

                    key = str(line).split(' ')[0].strip()
                    value = ' '.join(str(line).split(' ')[1:]).strip()
                    # print(key, value)
                    pylauncher_dict[key] = value
                return pylauncher_dict

            def get_where_python3():
                cmd_string = str('call "%s" %s' % ('where', 'python3'))
                plat = getplatform()
                if (plat == 'Windows'):
                    pass
                else:
                    cmd_string = str('"%s" %s' % ('which', 'python3'))
                # print("", cmd_string)
                result = subprocess.getoutput(cmd_string)
                # print("", result)

                pyname = 'Python3-'
                if (plat == 'Windows'):
                    pass
                else:
                    pyname = 'Python3-'
                pylauncher_dict = {}
                count = 0
                for line in result.split('\n'):
                    count = count + 1

                    line = line.strip()
                    if (line == ''):
                        continue

                    # line = line + '\t*'

                    key = pyname + str(count)
                    value = line
                    # print(key, value)
                    pylauncher_dict[key] = value
                return pylauncher_dict

            def get_where_python():
                cmd_string = str('call "%s" %s' % ('where', 'python'))
                plat = getplatform()
                if (plat == 'Windows'):
                    pass
                else:
                    cmd_string = str('"%s" %s' % ('which', 'python'))
                # print("", cmd_string)
                result = subprocess.getoutput(cmd_string)
                # print("", result)

                pyname = 'Python-'
                if (plat == 'Windows'):
                    pass
                else:
                    pyname = 'Python-'
                pylauncher_dict = {}
                count = 0
                for line in result.split('\n'):
                    count = count + 1

                    line = line.strip()
                    if (line == ''):
                        continue

                    # line = line + '\t*'

                    key = pyname + str(count)
                    value = line
                    # print(key, value)
                    pylauncher_dict[key] = value
                return pylauncher_dict


            str_format = '    %-20s : %s'
            self.textEditPythonSummary.append('System Python Configure:')
            self.textEditPythonSummary.append('  PyPi:')
            self.textEditPythonSummary.append(str_format%(sys_pypi_name, sys_pypi))
            self.textEditPythonSummary.append('  Python:')

            if(plat == 'Windows'):
                self.textEditPythonSummary.append('    Py Launcher:')
                for (k, v) in get_pylauncher_python().items():
                    ''
                    self.textEditPythonSummary.append(str_format%(k, v))
                self.textEditPythonSummary.append('')

            if (plat == "Windows"):
                self.textEditPythonSummary.append('    Where Python3:')
            else:
                self.textEditPythonSummary.append('    Which Python3:')

            for (k, v) in get_where_python3().items():
                ''
                self.textEditPythonSummary.append(str_format%(k, v))

            self.textEditPythonSummary.append('')
            if (plat == "Windows"):
                self.textEditPythonSummary.append('    Where Python:')
            else:
                self.textEditPythonSummary.append('    Which Python:')

            for (k, v) in get_where_python().items():
                ''
                self.textEditPythonSummary.append(str_format % (k, v))

            current_var = pydepconfig['PyPi']['current']
            current_python = pydepconfig['PyPi'][current_var]
            current_row = self.tableWidgetPyPi.currentRow()
            if(current_row < 0):
                current_var = 'None'
                current_python = 'None'
            else:
                current_var = self.tableWidgetPyPi.item(current_row, 0).text()
                current_python = self.tableWidgetPyPi.item(current_row, 1).text()
            current_pypi_name = current_var
            current_pypi = current_python

            current_var = pydepconfig['Python']['current']
            current_python = pydepconfig['Python'][current_var]
            current_row = self.tableWidgetPython.currentRow()
            if(current_row < 0):
                current_var = 'None'
                current_python = 'None'
            else:
                current_var = self.tableWidgetPython.item(current_row, 0).text()
                current_python = self.tableWidgetPython.item(current_row, 1).text()
            current_python_name = current_var
            current_python = current_python

            self.textEditPythonSummary.append('')

            self.textEditPythonSummary.append('Pydependent Python Configure:')
            self.textEditPythonSummary.append('  PyPi:')
            self.textEditPythonSummary.append(str(str_format+' [%s]')%('Custom PyPi', current_pypi, current_pypi_name))
            self.textEditPythonSummary.append('  Python:')
            self.textEditPythonSummary.append(str_format%(current_python_name, current_python))
            self.showMessage('Python Summary: list python and pypi summary success.')

        def onPushButtonRefreshSystemClicked(self):
            ''
            self.refreshsummarytimer.start()
            self.showMessage('Python Summary: list python and pypi information finished.')


        # exist package [pip list]
        def onRefreshTimer(self):
            ''
            current_var = pydepconfig['Python']['current']
            current_python = pydepconfig['Python'][current_var]

            current_row = self.tableWidgetPython.currentRow()
            if(current_row < 0):
                self.showMessage('Existed Package: python page is empty, can execute pip list.')
                return
            current_item_key = self.tableWidgetPython.item(current_row, 0).text()
            current_item_value = self.tableWidgetPython.item(current_row, 1).text()

            current_python = current_item_value
            # print(current_item_key, current_item_value)

            self.textEditExistedPackage.clear()

            if(not os.path.exists(current_python)):
                return
            if(not os.path.isfile(current_python)):
                return

            cmd_string = str('call "%s" %s' % (current_python, '-m pip list --format=columns'))
            plat = getplatform()
            if (plat == 'Windows'):
                pass
            else:
                cmd_string = str('"%s" %s' % (current_python, '-m pip list --format=columns'))
            # print("", cmd_string)
            result = subprocess.getoutput(cmd_string)
            # print("", result)

            p1 = str("Selected Python:\n %s %s" % (current_item_key, current_item_value))
            p2 = str("Command-line:\n %s" % (cmd_string))
            p3 = str("Result:\n %s" % (result))

            #self.textEditExistedPackage.append(p1)
            #self.textEditExistedPackage.append(p2)
            #self.textEditExistedPackage.append(p3)
            self.textEditExistedPackage.append(result)
            self.showMessage('Existed Package: pip list success, %s.' % cmd_string)

        def onPushButtonRefreshExistedPackageClicked(self):
            ''
            self.textEditExistedPackage.clear()
            self.refreshtimer.start()

        # dependent package
        def onPushButtonSaveRequireClicked(self):
            ''
            pydeprequire = os.path.join(sourceroot, pydeprequiretxt)
            with open(pydeprequire, 'w', encoding='utf8') as f:
                f.write(self.textEditDependentPackage.toPlainText())

        def onPushButtonRefreshRequireClicked(self):
            ''
            def refresh_pydeprequirepackagelist():
                pydeprequirepackagelist = []
                for file in pydeprequiretxtlist:
                    if (file == ''):
                        continue

                    if (os.path.exists(file) and os.path.isfile(file)):
                        datas = ""
                        with open(file, 'r', encoding='utf8') as f:
                            for line in f.readlines():
                                datas += line
                        datas = datas.replace('\r', '')
                        datalist = datas.split('\n')
                        for package in datalist:
                            if (package == ''):
                                continue
                            if (pydeprequirepackagelist.__contains__(package)):
                                continue
                            pydeprequirepackagelist.append(package)
                # print(pydeprequirepackagelist)
                return pydeprequirepackagelist
            pydeprequirepackagelist = refresh_pydeprequirepackagelist()
            self.textEditDependentPackage.clear()
            for package in pydeprequirepackagelist:
                self.textEditDependentPackage.append(package)


        # install package
        def getInstallCommand(self):

            # PyPi
            current_row = self.tableWidgetPyPi.currentRow()
            if(current_row < 0):
                self.showMessage('Install Package: pypi page is empty, cant create install command.')
                return
            current_item_key = self.tableWidgetPyPi.item(current_row, 0).text()
            current_item_value = self.tableWidgetPyPi.item(current_row, 1).text()

            current_pypi = current_item_value
            # print(current_item_key, current_item_value)
            # print(current_row)

            # For Python
            current_row = self.tableWidgetPython.currentRow()
            if(current_row < 0):
                self.showMessage('Install Package: python program page is empty, cant create install command.')
                return
            current_item_key = self.tableWidgetPython.item(current_row, 0).text()
            current_item_value = self.tableWidgetPython.item(current_row, 1).text()

            current_python = current_item_value
            # print(current_item_key, current_item_value)
            # print(current_row)

            # Install Package
            package_list = []
            datas = self.textEditDependentPackage.toPlainText()
            for data in datas.split('\n'):
                ''
                for item in data.split(' '):
                    item = item.strip()
                    if(item == ''):
                        continue
                    if(package_list.__contains__(item)):
                        continue
                    package_list.append(item)
            if(package_list.__len__() < 1):
                ''
                self.showMessage('Install Package: No package to install.')
                return None
            package_string = ' '.join(package_list)

            # print(json.dumps(package_list, indent=4, sort_keys=False, ensure_ascii=False))
            # print(package_list)
            # print(package_string)
            self.showMessage(str('Install Package: Install Package %s' % package_string))

            # PIP_INSTALL_COMMAND

            cmd_line2 = ''

            shell = ""
            plat = getplatform()
            if (plat == "Windows"):
                shell = os.environ.get('COMSPEC') + ' ' + "/k"
            else:
                shell = os.environ.get('SHELL')

            cmd_list2 = [
                'start',
                shell,
                '"',
                current_python,
                '-m pip install',
                package_string,
                str('-i %s' % current_pypi),
                '"'
            ]
            plat = getplatform()
            if(plat == 'Windows'):
                tmp_pkg_list = []
                for (k) in package_list:
                    tmp_pkg_list.append('"'+str(k)+'"')
                package_string = ' '.join(tmp_pkg_list)
                shell = os.environ.get('COMSPEC')
                cmd_str1 = str('start "call" "%s" "/k" "%s" "-m" "pip" "install" %s "-i" "%s"'
                               % (shell, current_python, package_string, current_pypi))
                cmd_list2 = [
                    cmd_str1
                ]
            elif (plat == 'Darwin'):
                tmp_pkg_list = []
                for (k) in package_list:
                    tmp_pkg_list.append('\\"'+str(k)+'\\"')
                package_string = ' '.join(tmp_pkg_list)
                cmd_str1 = str('osascript -e \' tell application "Terminal" to do script "cd %s;%s -m pip install %s -i %s" \' 1>/dev/null'
                               % (shellroot, current_python, package_string, current_pypi))
                cmd_list2 = [
                    cmd_str1
                ]
            else:
                tmp_pkg_list = []
                for (k) in package_list:
                    tmp_pkg_list.append('\\"'+str(k)+'\\"')
                package_string = ' '.join(tmp_pkg_list)
                cmd_str1 = str('gnome-terminal --window -- bash -c "cd %s;%s -m pip install %s -i %s;exec bash;"'
                               % (shellroot, current_python, package_string, current_pypi))
                cmd_list2 = [
                    cmd_str1
                ]

            cmd_line2 = ' '.join(cmd_list2)
            # print(cmd_line2)
            # return

            self.showMessage(str('Install Package: PIP_INSTALL_COMMAND %s' % cmd_line2))

            return cmd_line2

        def onPushButtonRefreshCommandClicked(self):
            ''
            self.getInstallCommand()
            self.showMessage('Install Package: take command success.')

        def onPushButtonStartInstallClicked(self):
            ''
            cmd_line2 = self.getInstallCommand()
            if(cmd_line2 is None):
                return
            os.system(cmd_line2)
            self.showMessage('Install Package: user can keep look out over install progress.')

    app = Application(sys.argv)
    win = MainWindow()
    win.show()
    sys.exit(app.exec())

    return


if __name__ == '__main__':
    ret = main_function()
    if (ret == None):
        ret = 0
    os._exit(ret)
