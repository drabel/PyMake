# -*- coding: utf-8 -*-
# !/usr/bin/env python

from __future__ import unicode_literals

import os
import sys
import copy


# local settings [private]
load_pth_debug_switch1 = False
def load_pth_debug_switch(switch=None):
    global load_pth_debug_switch1
    if(switch is None):
        return load_pth_debug_switch1
    if(isinstance(switch, bool)):
        load_pth_debug_switch1 = switch
    return load_pth_debug_switch1

# ----------------------------------------------------------------------
def load_pth_for_work_dir(pth_file=''):
    if (pth_file is None):
        return

    if(isinstance(pth_file, list)):
        ''
        for pth_file1 in pth_file:
            load_pth_for_work_dir(pth_file1)
        return

    # set into sys.path
    list_pth_path = []

    while (True):
        if(pth_file == ''):
            break

        # 1, relative to work dir.
        real_pth_file = os.path.realpath(pth_file)
        # print(real_pth_file)

        # 2, relative to self file. local or public load
        # pth_file1 = os.path.split(os.path.realpath(__file__))[0]
        # real_pth_file = os.path.join(pth_file1, pth_file)
        # real_pth_file = os.path.realpath(real_pth_file)
        # print(real_pth_file)

        # 3, relative to caller file. who need, who load.
        # caller
        # print(sys.argv[0])
        # file self
        # print(__file__)
        # pth_file1 = os.path.split(os.path.realpath(sys.argv[0]))[0]
        # real_pth_file = os.path.join(pth_file1, pth_file)
        # real_pth_file = os.path.realpath(real_pth_file)
        # print(real_pth_file)

        if(load_pth_debug_switch() is True):
            print(real_pth_file)

        if(not os.path.exists(real_pth_file)):
            break

        # read all
        local_pth_paths = []
        with open(real_pth_file, 'r', encoding='utf8') as f:
            for l in f.readlines():
                # important format
                # l = l.strip()
                while (l.endswith('\r') or l.endswith('\n') or l.endswith('\r\n')):
                    l = l.rstrip('\r\n')
                    l = l.rstrip('\n')
                    l = l.rstrip('\r')
                # if(l == ''):
                #    continue
                local_pth_paths.append(l)

        # format and write back

        # strip
        store_pth_paths = copy.deepcopy(local_pth_paths)
        for (i, l) in enumerate(store_pth_paths):
            # import format
            l = l.strip()
            store_pth_paths[i] = l

        # clean repeat path
        clean_list = []
        temp_list = []
        for l in store_pth_paths:
            # import ignore
            if (l == ''):
                continue
            if (os.path.isabs(l) is False):
                continue
            if (temp_list.__contains__(str(l).replace('\\', '/').lower())):
                clean_list.append(l)
                continue
            else:
                temp_list.append(str(l).replace('\\', '/').lower())
        # print(clean_list)

        store_pth_paths.reverse()
        for l in clean_list:
            if (store_pth_paths.__contains__(l) is True):
                store_pth_paths.remove(l)
        store_pth_paths.reverse()

        # write back
        if (local_pth_paths != store_pth_paths):
            with open(real_pth_file, 'w', encoding='utf8') as f:
                for l in store_pth_paths:
                    f.write(l + '\n')

        # raw

        # relative to caller file.
        pth_file1 = os.path.split(real_pth_file)[0]
        # print(pth_file1)

        sys_pth_paths = copy.deepcopy(store_pth_paths)
        for l in sys_pth_paths:
            if (l == ''):
                continue
            # print(os.path.isabs(l), l)
            if (os.path.isabs(l) is False):
                l = os.path.join(pth_file1, l)
                # print(l)
                l = os.path.realpath(l)
                # print(l)
            list_pth_path.insert(0, l)

        # clean illgal path
        clean_list = []
        for l in list_pth_path:
            if (l == ''):
                clean_list.append(l)
                continue
            if (os.path.isabs(l) is False):
                clean_list.append(l)
                continue
        # print(clean_list)

        for l in clean_list:
            if (list_pth_path.__contains__(l) is True):
                list_pth_path.remove(l)

        break

    if(load_pth_debug_switch()):
        print(list_pth_path)
    syspath1 = []
    for key in sys.path:
        temppath = key.replace('\\', '/')
        syspath1.append(temppath)
    for path in list_pth_path:
        module_path = path
        temppath = module_path.replace('\\', '/')
        if(syspath1.__contains__(temppath)):
            continue
        sys.path.insert(0, module_path)

    return

def load_pth_for_work_root(pth_file=''):
    load_pth_for_work_dir(pth_file)
    return

def load_pth_for_cwd(pth_file=''):
    load_pth_for_work_dir(pth_file)
    return

# ----------------------------------------------------------------------
def load_pth_for_self_file(pth_file=''):
    if (pth_file is None):
        return

    if (isinstance(pth_file, list)):
        ''
        for pth_file1 in pth_file:
            load_pth_for_self_file(pth_file1)
        return

    # set into sys.path
    list_pth_path = []

    while (True):
        if(pth_file == ''):
            break

        # 1, relative to work dir.
        # real_pth_file = os.path.realpath(pth_file)
        # print(real_pth_file)

        # 2, relative to self file. local or public load
        pth_file1 = os.path.split(os.path.realpath(__file__))[0]
        real_pth_file = os.path.join(pth_file1, pth_file)
        real_pth_file = os.path.realpath(real_pth_file)
        # print(real_pth_file)

        # 3, relative to caller file. who need, who load.
        # caller
        # print(sys.argv[0])
        # file self
        # print(__file__)
        # pth_file1 = os.path.split(os.path.realpath(sys.argv[0]))[0]
        # real_pth_file = os.path.join(pth_file1, pth_file)
        # real_pth_file = os.path.realpath(real_pth_file)
        # print(real_pth_file)

        if(load_pth_debug_switch() is True):
            print(real_pth_file)

        if(not os.path.exists(real_pth_file)):
            break

        # read all
        local_pth_paths = []
        with open(real_pth_file, 'r', encoding='utf8') as f:
            for l in f.readlines():
                # important format
                # l = l.strip()
                while (l.endswith('\r') or l.endswith('\n') or l.endswith('\r\n')):
                    l = l.rstrip('\r\n')
                    l = l.rstrip('\n')
                    l = l.rstrip('\r')
                # if(l == ''):
                #    continue
                local_pth_paths.append(l)

        # format and write back

        # strip
        store_pth_paths = copy.deepcopy(local_pth_paths)
        for (i, l) in enumerate(store_pth_paths):
            # import format
            l = l.strip()
            store_pth_paths[i] = l

        # clean repeat path
        clean_list = []
        temp_list = []
        for l in store_pth_paths:
            # import ignore
            if (l == ''):
                continue
            if (os.path.isabs(l) is False):
                continue
            if (temp_list.__contains__(str(l).replace('\\', '/').lower())):
                clean_list.append(l)
                continue
            else:
                temp_list.append(str(l).replace('\\', '/').lower())
        # print(clean_list)

        store_pth_paths.reverse()
        for l in clean_list:
            if (store_pth_paths.__contains__(l) is True):
                store_pth_paths.remove(l)
        store_pth_paths.reverse()

        # write back
        if (local_pth_paths != store_pth_paths):
            with open(real_pth_file, 'w', encoding='utf8') as f:
                for l in store_pth_paths:
                    f.write(l + '\n')

        # raw

        # relative to caller file.
        pth_file1 = os.path.split(real_pth_file)[0]
        # print(pth_file1)

        sys_pth_paths = copy.deepcopy(store_pth_paths)
        for l in sys_pth_paths:
            if (l == ''):
                continue
            # print(os.path.isabs(l), l)
            if (os.path.isabs(l) is False):
                l = os.path.join(pth_file1, l)
                # print(l)
                l = os.path.realpath(l)
                # print(l)
            list_pth_path.insert(0, l)

        # clean illgal path
        clean_list = []
        for l in list_pth_path:
            if (l == ''):
                clean_list.append(l)
                continue
            if (os.path.isabs(l) is False):
                clean_list.append(l)
                continue
        # print(clean_list)

        for l in clean_list:
            if (list_pth_path.__contains__(l) is True):
                list_pth_path.remove(l)

        break

    if(load_pth_debug_switch()):
        print(list_pth_path)
    syspath1 = []
    for key in sys.path:
        temppath = key.replace('\\', '/')
        syspath1.append(temppath)
    for path in list_pth_path:
        module_path = path
        temppath = module_path.replace('\\', '/')
        if(syspath1.__contains__(temppath)):
            continue
        sys.path.insert(0, module_path)

    return

def load_pth_for_local(pth_file=''):
    load_pth_for_self_file(pth_file)
    return

def load_pth_for_self(pth_file=''):
    load_pth_for_self_file(pth_file)
    return

# ----------------------------------------------------------------------
def load_pth_for_caller_file(pth_file=''):
    if (pth_file is None):
        return

    if (isinstance(pth_file, list)):
        ''
        for pth_file1 in pth_file:
            load_pth_for_caller_file(pth_file1)
        return

    # set into sys.path
    list_pth_path = []

    while (True):
        if(pth_file == ''):
            break

        # 1, relative to work dir.
        # real_pth_file = os.path.realpath(pth_file)
        # print(real_pth_file)

        # 2, relative to self file. local or public load
        # pth_file1 = os.path.split(os.path.realpath(__file__))[0]
        # real_pth_file = os.path.join(pth_file1, pth_file)
        # real_pth_file = os.path.realpath(real_pth_file)
        # print(real_pth_file)

        # 3, relative to caller file. who need, who load.
        # caller
        # print(sys.argv[0])
        # file self
        # print(__file__)
        pth_file1 = os.path.split(os.path.realpath(sys.argv[0]))[0]
        real_pth_file = os.path.join(pth_file1, pth_file)
        real_pth_file = os.path.realpath(real_pth_file)
        # print(real_pth_file)

        if(load_pth_debug_switch() is True):
            print(real_pth_file)

        if(not os.path.exists(real_pth_file)):
            break

        # read all
        local_pth_paths = []
        with open(real_pth_file, 'r', encoding='utf8') as f:
            for l in f.readlines():
                # important format
                # l = l.strip()
                while (l.endswith('\r') or l.endswith('\n') or l.endswith('\r\n')):
                    l = l.rstrip('\r\n')
                    l = l.rstrip('\n')
                    l = l.rstrip('\r')
                # if(l == ''):
                #    continue
                local_pth_paths.append(l)

        # format and write back

        # strip
        store_pth_paths = copy.deepcopy(local_pth_paths)
        for (i, l) in enumerate(store_pth_paths):
            # import format
            l = l.strip()
            store_pth_paths[i] = l

        # clean repeat path
        clean_list = []
        temp_list = []
        for l in store_pth_paths:
            # import ignore
            if (l == ''):
                continue
            if (os.path.isabs(l) is False):
                continue
            if (temp_list.__contains__(str(l).replace('\\', '/').lower())):
                clean_list.append(l)
                continue
            else:
                temp_list.append(str(l).replace('\\', '/').lower())
        # print(clean_list)

        store_pth_paths.reverse()
        for l in clean_list:
            if (store_pth_paths.__contains__(l) is True):
                store_pth_paths.remove(l)
        store_pth_paths.reverse()

        # write back
        if (local_pth_paths != store_pth_paths):
            with open(real_pth_file, 'w', encoding='utf8') as f:
                for l in store_pth_paths:
                    f.write(l + '\n')

        # raw

        # relative to caller file.
        pth_file1 = os.path.split(real_pth_file)[0]
        # print(pth_file1)

        sys_pth_paths = copy.deepcopy(store_pth_paths)
        for l in sys_pth_paths:
            if (l == ''):
                continue
            # print(os.path.isabs(l), l)
            if (os.path.isabs(l) is False):
                l = os.path.join(pth_file1, l)
                # print(l)
                l = os.path.realpath(l)
                # print(l)
            list_pth_path.insert(0, l)

        # clean illgal path
        clean_list = []
        for l in list_pth_path:
            if (l == ''):
                clean_list.append(l)
                continue
            if (os.path.isabs(l) is False):
                clean_list.append(l)
                continue
        # print(clean_list)

        for l in clean_list:
            if (list_pth_path.__contains__(l) is True):
                list_pth_path.remove(l)

        break

    if(load_pth_debug_switch()):
        print(list_pth_path)
    syspath1 = []
    for key in sys.path:
        temppath = key.replace('\\', '/')
        syspath1.append(temppath)
    for path in list_pth_path:
        module_path = path
        temppath = module_path.replace('\\', '/')
        if(syspath1.__contains__(temppath)):
            continue
        sys.path.insert(0, module_path)

    return

def load_pth_for_caller(pth_file=''):
    load_pth_for_caller_file(pth_file)
    return

# ----------------------------------------------------------------------
def load_pth_for_your_path(basic_path='', pth_file=''):
    if(basic_path is None):
        return

    if(not isinstance(basic_path, str)):
        return

    if (pth_file is None):
        return

    if (isinstance(pth_file, list)):
        ''
        for pth_file1 in pth_file:
            load_pth_for_caller_file(pth_file1)
        return

    # set into sys.path
    list_pth_path = []

    while (True):
        if(pth_file == ''):
            break

        # 1, relative to work dir.
        # real_pth_file = os.path.realpath(pth_file)
        # print(real_pth_file)

        # 2, relative to self file. local or public load
        # pth_file1 = os.path.split(os.path.realpath(__file__))[0]
        # real_pth_file = os.path.join(pth_file1, pth_file)
        # real_pth_file = os.path.realpath(real_pth_file)
        # print(real_pth_file)

        # 3, relative to caller file. who need, who load.
        # caller
        # print(sys.argv[0])
        # file self
        # print(__file__)
        # pth_file1 = os.path.split(os.path.realpath(sys.argv[0]))[0]
        # real_pth_file = os.path.join(pth_file1, pth_file)
        # real_pth_file = os.path.realpath(real_pth_file)
        # print(real_pth_file)

        # 4, relative to your custom path. [default: '', relative to os.getcwd()]
        pth_file1 = os.path.realpath(basic_path)
        real_pth_file = os.path.join(pth_file1, pth_file)
        real_pth_file = os.path.realpath(real_pth_file)
        # print(real_pth_file)

        if(load_pth_debug_switch() is True):
            print(real_pth_file)

        if(not os.path.exists(real_pth_file)):
            break

        # read all
        local_pth_paths = []
        with open(real_pth_file, 'r', encoding='utf8') as f:
            for l in f.readlines():
                # important format
                # l = l.strip()
                while (l.endswith('\r') or l.endswith('\n') or l.endswith('\r\n')):
                    l = l.rstrip('\r\n')
                    l = l.rstrip('\n')
                    l = l.rstrip('\r')
                # if(l == ''):
                #    continue
                local_pth_paths.append(l)

        # format and write back

        # strip
        store_pth_paths = copy.deepcopy(local_pth_paths)
        for (i, l) in enumerate(store_pth_paths):
            # import format
            l = l.strip()
            store_pth_paths[i] = l

        # clean repeat path
        clean_list = []
        temp_list = []
        for l in store_pth_paths:
            # import ignore
            if (l == ''):
                continue
            if (os.path.isabs(l) is False):
                continue
            if (temp_list.__contains__(str(l).replace('\\', '/').lower())):
                clean_list.append(l)
                continue
            else:
                temp_list.append(str(l).replace('\\', '/').lower())
        # print(clean_list)

        store_pth_paths.reverse()
        for l in clean_list:
            if (store_pth_paths.__contains__(l) is True):
                store_pth_paths.remove(l)
        store_pth_paths.reverse()

        # write back
        if (local_pth_paths != store_pth_paths):
            with open(real_pth_file, 'w', encoding='utf8') as f:
                for l in store_pth_paths:
                    f.write(l + '\n')

        # raw

        # relative to caller file.
        pth_file1 = os.path.split(real_pth_file)[0]
        # print(pth_file1)

        sys_pth_paths = copy.deepcopy(store_pth_paths)
        for l in sys_pth_paths:
            if (l == ''):
                continue
            # print(os.path.isabs(l), l)
            if (os.path.isabs(l) is False):
                l = os.path.join(pth_file1, l)
                # print(l)
                l = os.path.realpath(l)
                # print(l)
            list_pth_path.insert(0, l)

        # clean illgal path
        clean_list = []
        for l in list_pth_path:
            if (l == ''):
                clean_list.append(l)
                continue
            if (os.path.isabs(l) is False):
                clean_list.append(l)
                continue
        # print(clean_list)

        for l in clean_list:
            if (list_pth_path.__contains__(l) is True):
                list_pth_path.remove(l)

        break

    if(load_pth_debug_switch()):
        print(list_pth_path)
    syspath1 = []
    for key in sys.path:
        temppath = key.replace('\\', '/')
        syspath1.append(temppath)
    for path in list_pth_path:
        module_path = path
        temppath = module_path.replace('\\', '/')
        if(syspath1.__contains__(temppath)):
            continue
        sys.path.insert(0, module_path)

    return

def load_pth_for_you(basic_path='', pth_file=''):
    load_pth_for_your_path(basic_path, pth_file)
    return

# ----------------------------------------------------------------------
def load_pth(pth_file=''):
    load_pth_for_caller(pth_file)
    return

# ----------------------------------------------------------------------
#test code

'''
print(os.environ)
print('')
dict0 = {}
dict0['path+'] = []
for (key) in os.environ['PATH'].split(os.path.pathsep):
    # print(str(key))
    dict0['path+'].append(str(key))
for (key, value) in os.environ.items():
    if (key == 'path+'):
        continue
    if (str(key).lower() == "path"):
        continue
    dict0[key] = value
print(json.dumps(dict0, indent=4, sort_keys=False, ensure_ascii=False))

print(sys.argv[0])
print(sys.path)
print(os.environ['PYTHONPATH'])

os.environ['PYTHONPATH']='c:\\' + os.path.pathsep + os.environ['PYTHONPATH']
print(os.environ['PYTHONPATH'])
print(sys.path)
'''

if (load_pth_debug_switch() is True):
    print('===========================')
    load_pth_for_cwd('bb.pth.txt')
    load_pth_for_self('bb.pth.txt')
    # who need, who load! no relation ship.
    load_pth_for_caller('bb.pth.txt')
    load_pth('bb.pth.txt')
    print(sys.path)
    for i in sys.path:
       print(i)

def load_pth_debug(pth_file='test.pth'):
    ''
    load_pth_debug_switch(True)
    print('debug start ===========================')
    for i in sys.path:
       print(i)
    print('debug pth for os.getcwd() ===========================')
    load_pth_for_cwd(pth_file)
    for i in sys.path:
       print(i)
    print('debug pth for __file__ ===========================')
    load_pth_for_self(pth_file)
    for i in sys.path:
       print(i)
    print('debug pth for sys.argv[0] ===========================')
    # who need, who load! no relation ship.
    load_pth_for_caller(pth_file)
    for i in sys.path:
       print(i)
    print('debug pth for your path, "..." os.getcwd() ===========================')
    load_pth_for_you('', pth_file)
    for i in sys.path:
       print(i)
    print('debug pth for your path, "..." /Users/abel/Library ===========================')
    load_pth_for_you('/Users/abel/Library', pth_file)
    for i in sys.path:
       print(i)
    print('debug pth for your path, "..." exception ===========================')
    load_pth_for_you([], pth_file)
    for i in sys.path:
       print(i)
    print('debug pth default function ===========================')
    load_pth(pth_file)
    for i in sys.path:
       print(i)
    print('===========================')
    print(sys.path)
    for i in sys.path:
       print(i)
    print('debug end -----------------------------')
    load_pth_debug_switch(False)
