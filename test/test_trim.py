# -*- coding: utf-8 -*-
# !/usr/bin/env python

from __future__ import unicode_literals

import os
import re
import sys
import uuid
import shutil
import time
import json
import copy
import types
import datetime

# ===============================================================================================
# pymake relative path to pymake.py.
# relative to project root, relative to default UserSource.
# --------------------
pymakerelativepath = '..'


# ===============================================================================================
# load module helper
# --------------------

def load_rpath_for_local(path=''):
    # add rpath. [HEADER ROOT] [LIBRARY ROOT] [MODULE ROOT]
    # find module when execute.
    # 1, realpath, fake_file
    # 2, __file__
    # 3, sys.argv[0]
    basicpath = os.path.split(os.path.realpath(__file__))[0]
    paths = []
    if (isinstance(path, str)):
        paths.append(path)
    if (isinstance(path, list)):
        paths.extend(path)
    syspath1 = []
    for key in sys.path:
        temppath = key.replace('\\', '/')
        syspath1.append(temppath)
    for path in paths:
        module_path = os.path.join(basicpath, path)
        module_path = os.path.realpath(module_path)
        temppath = module_path.replace('\\', '/')
        if(syspath1.__contains__(temppath)):
            continue
        sys.path.insert(0, module_path)
    return

load_rpath_for_local(pymakerelativepath)
# print(sys.path)

# ===============================================================================================
# import module
# --------------------
# common
from pycore.pycore import *


# from pycore.docopt import docopt

# ===============================================================================================
# test module
# --------------------

print('-------------------------------------------------------------------------------------------')
str1 = '  abieogjgniid jgkg nnej ; \n jbguj \x0B 垂直制表符 \0 abd \t eee \r hhh \n gbrgrh    '
print('['+str1+']')
str2 = str_trim(str1)
print('['+str2+']')
str2 = str1.strip()
print('['+str2+']')

print('-------------------------------------------------------------------------------------------')
str1 = '  abieogjgniid jgkg nnej ;  hhh gbrgrh    '
print('['+str1+']')
str2 = str_trim(str1)
print('['+str2+']')
str2 = str1.strip()
print('['+str2+']')

print('-------------------------------------------------------------------------------------------')
str1 = '\r\n  abieogjgniid jgkg nnej ;  hhh gbrgrh    \r\n'
print('['+str1+']')
str2 = str_ltrim(str1, '\r\n')
print('['+str2+']')
str2 = str1.lstrip('\r\n')
print('['+str2+']')

print('-------------------------------------------------------------------------------------------')
str1 = '\r\n  abieogjgniid jgkg nnej ;  hhh gbrgrh    \r\n'
print('['+str1+']')
str2 = str_rtrim(str1, '\r\n')
print('['+str2+']')
str2 = str1.rstrip('\r\n')
print('['+str2+']')

print('-------------------------------------------------------------------------------------------')
str1 = '\r\n  abieogjgniid jgkg nnej ;  hhh gbrgrh    \r\n'
print('['+str1+']')
str2 = str_trim(str1, '\r\n')
print('['+str2+']')
str2 = str1.strip('\r\n')
print('['+str2+']')

