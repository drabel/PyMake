from __future__ import unicode_literals
# -*- coding: utf-8 -*-

import os
import re
import sys
import uuid
import shutil
import time
import json
import copy
import types

def load_rpath_for_local(path = ''):
    # add rpath. [HEADER ROOT] [LIBRARY ROOT] [MODULE ROOT]
    # find module when execute.
    # 1, realpath, fake_file
    # 2, __file__
    # 3, sys.argv[0]
    basicpath = os.path.split(os.path.realpath(__file__))[0]
    paths = []
    if(isinstance(path, str)):
        paths.append(path)
    if(isinstance(path, list)):
        paths.extend(path)
    syspath1 = []
    for key in sys.path:
        temppath = key.replace('\\', '/')
        syspath1.append(temppath)
    for path in paths:
        module_path = os.path.join(basicpath, path)
        module_path = os.path.realpath(module_path)
        temppath = module_path.replace('\\', '/')
        if(syspath1.__contains__(temppath)):
            continue
        sys.path.insert(0, module_path)
    return

load_rpath_for_local('..')
#you must load pycore to load function from there ...
#load_rpath_for_you(os.path.dirname(os.path.realpath(__file__)), '..')

print('-----------------------------')
print('sys.path')
for i in sys.path:
    print(i)
print('.')

from pycomm.pycomm import *

# os.chdir('C:\\Users\\Administrator\\Develop')
# load_pth('b0-toolkits\\a0-compiler/PyMake/STRUCTS.PTH', ['C:\\', 'D:\\', 'pydd'])
# print(os.environ['PYTHONPATH'])
# print(sys.path)
# return

load_pth_debug('a.pth.txt')

print('-----------------------------')
print('load_pth_for_your_path, local __file__')
load_pth_debug_switch(True)
load_pth_for_you(os.path.dirname(os.path.realpath(__file__)), 'a.pth.txt')
load_pth_debug_switch(False)
for i in sys.path:
    print(i)
print('.')

print('-----------------------------')
print('sys.path')
for i in sys.path:
    print(i)
print('.')

print('-----------------------------')
print('pyvariable')
#pyvariable()
print('.')

print('-----------------------------')
print('pyinitial')
#pyinitial()
print('.')

print('-----------------------------')
print('pymake8_initial')
# os.chdir('C:\\')
main_function()
# print(os.getcwd())
load_rpath_for_local('fack_path1')
#pyinitial()
# print

# mod & print

print('.')

print('-----------------------------')
print('sys.path')
for i in sys.path:
    print(i)
print('.')

print('-----------------------------')
print('where am I')
print(where_am_I())
print('.')

print('-----------------------------')
print('where is he')
print(where_is_he())
print('.')

print('-----------------------------')
print('where are you')
print(where_are_you())
print('.')

print('-----------------------------')
print('.')
