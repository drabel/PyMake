#!/usr/bin/env bash


echo System Path:

str=$PATH
i=1
while((1==1))
do
    splitchar=`echo $str | cut -d ":" -f $i`
    if [ "$splitchar" != "" ]
    then
        ((i++))
        echo "  $splitchar"
    else
        break
    fi
done

echo System Environ Variable:

names=$(env)

SAVEIFS=$IFS   # Save current IFS
IFS=$'\n'      # Change IFS to new line
names=($names) # split to array $names
IFS=$SAVEIFS   # Restore IFS

for (( i=0; i<${#names[@]}; i++ ))
do
    #echo "  ${names[$i]}"
    var=${names[$i]}
    name=${var%%=*}
    content=${var#*=}
    if [ "$name" = "PATH" ]; then
        continue
    fi
    printf "  %-30s %s\n" $name $content
done