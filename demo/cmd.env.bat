@echo off

setlocal enabledelayedexpansion

echo System Path:
::定义一个以分号作为分隔的字符串
set str=%path%
::str的副本
set remain=%str%
:loop
for /f "tokens=1* delims=;" %%a in ("%remain%") do (
	::输出第一个分段(令牌)
	echo   %%a
	rem 将截取剩下的部分赋给变量remain，其实这里可以使用延迟变量开关
	set remain=%%b
)
::如果还有剩余,则继续分割
if defined remain goto :loop

set str=
echo.

echo System Environ Variable:

::批处理对注释处理的非常不好，在for内禁用注释。系统找不到指定的驱动器。
for /f "delims=" %%i in ('set') do (
    set str=%%i
    set strorigin=!str!
    set ch1==
    set num=0
    rem echo !str!
    rem echo !strorigin!
    rem goto :eof
    set strname=
    set strcontent=

    call :findstrpair

    rem echo !num!
    set "var=                              "
    set strvar=
    rem echo !var:~0,20! !strname!
    call :emptyvar
    rem echo [!strvar!]
    if not "!strname!" == "Path" (
        echo   !strname! !strvar! !strcontent!
    )
    rem echo !strcontent!
    rem echo %num% !num!
    rem goto :eof

    set str=
    set strorigin=
    set ch1=
    set num=
    set var=
    set strvar=
)

set str=
echo.

::call py %~dp0python.env.py

exit /b 0

::输入的是个临时的、完整的str，会被废弃。
:findstrpair
rem echo %strorigin%
rem echo %str%
rem goto :eof
if not "%str%"=="" (
    set /a num+=1
    rem echo %str:~0,1%
    rem echo %ch1%
    rem goto :eof

    set "strname=!strorigin:~0,%num%!"
    set "strcontent=%str:~1%"
    rem echo %strname%
    rem echo %strcontent%
    rem goto :eof

    rem echo %%i
    rem 比较首字符是否为要求的字符，如果是则跳出循环
    if "%str:~0,1%"=="%ch1%" goto :eof

    set "str=%str:~1%"
    rem echo %str%
    goto findstrpair
)
set /a num=0
goto :eof

:emptyvar
    set /a pos=30-%num%
    if %pos% LSS 0 ( set /a pos=0 )
    set strvar=!var:~0,%pos%!
    set pos=
goto :eof