# -*- coding: utf-8 -*-
#!/usr/bin/env python

import os
import re
import sys
import uuid
import shutil
import time
import datetime
import json
import copy
import types
import platform

def main_function():
    #print(sys.executable)
    #print(platform.python_version())
    #print(__file__)
    #print(os.getcwd())
    #print(len(sys.argv))
    #print(sys.argv)
    #for path0 in sys.argv:
    #    print ("PARAMS:", path0)
    #print("")

    print('System Path:')
    for path0 in os.environ['PATH'].split(os.path.pathsep):
        print("  %s" % path0)

    print('System Environ Variable:')
    for (k ,v) in os.environ.items():
        if(str(k) == 'PATH'):
            continue
        print("  %-30s %s" % (k,v))
    return

if __name__ == '__main__':
    main_function()
