user_name=$1

while [ 1 ]
do

if [ "$user_name" = "" ]; then
    echo "Usage: $0 <user-name>"
	break
fi

id $user_name &>/dev/null
if [ $? -eq 0 ];then
    #echo "$user_name exists."
    echo. 2>/dev/null
else
    echo "$user_name does not exist."
    break
fi

if [ "$user_name" = "" ]; then
	break
fi

mk1 () {
    sudo echo create dir: $1
    sudo mkdir $1
    sudo echo chown: $1
    sudo chown -R $user_name:users $1
}

cd /var/services/homes/$user_name

sudo echo working ...

mk1 program
mk1 download
mk1 document
mk1 picture
mk1 favorite
mk1 photo
mk1 video
mk1 movie
mk1 music
mk1 www
mk1 rss
mk1 contact

break



done
