# PyMake Wiki  

*Around the user environment configuration file,*
*pymake series programs complete the management of various projects.*  
*The user environment description file is a file that the user configures independently.*
*It describes the environment on a computer, but these environment configurations are not necessarily effective.*
*The user must configure the environment on the computer according to the description before it can take effect.*  
*It is up to the user to decide whether to configure the computer environment according to the environment description or modify the description file according to the computer environment.*  

# Friend Program   
![..](z0.png)  

#### Program Link  

```bash
Python Project:
    PyMake    : https://gitee.com/drabel/PyMake
    PyQMake   : https://gitee.com/drabel/PyQMake
    PyCMake   : https://gitee.com/drabel/PyCMake
    PyAutoMake: https://gitee.com/drabel/PyAutoMake
    PyVCMake  : https://gitee.com/drabel/PyVCMake
    PyKeilMake: https://gitee.com/drabel/PyKeilMake
    PyBuilder : https://gitee.com/drabel/PyBuilder
    PyQQt     : https://gitee.com/drabel/PyQQt
    
Qt Project:
    LibQQt    : https://gitee.com/drabel/LibQQt
    Multi-link: https://gitee.com/drabel/multi-link
```

# PyMake Working

![..](z2.png)  


# Colleges and Universities (Gitee)   

*How to use the gitee repo in techning? This is the progress from gitee learning video.*  

![..](z1.png)  

