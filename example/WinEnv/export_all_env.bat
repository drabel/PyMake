@echo off

echo.
echo export all env to %CD%
echo.
echo export process will cost lots of time, please wait for some minutes.
echo Would you like to do this really?
pause

for /f "delims=" %%i in ('pymake env') do (
    echo pymake export2 here %%i to %%i -lc
    call pymake export2 here %%i to %%i -lc
    echo.
    echo pymake vc export2 here %%i to %%i -lc
    call pymake vc export2 here %%i to %%i -lc
    echo.
)