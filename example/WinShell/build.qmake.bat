@echo off
echo You must use pymake ccvp command to execute this command.
if ""%QTDIR%"" == """" ( echo Error: env QTDIR is empty. & exit /b 0)
if ""%QTVERSION%"" == """" ( echo Error: env QTVERSION is empty. & exit /b 0)
if ""%QSYS%"" == """" ( echo Error: env QSYS is empty. & exit /b 0)

rem echo.
echo Work root: %CD%
if not exist "*.pro" ( echo has any .pro? please add here command to restrict. & exit /b 0 )

set src_path=%CD%
set profilename=%1
set prod=V:/Develop/d0-product/%profilename%/%QSYS%
set sdk=V:/Develop/d1-sdk/%profilename%/%QSYS%
set src=%src_path%/%profilename%.pro
if not exist "%src%" ( echo %profilename%.pro is not exist. & exit /b 0)

set build=V:/Develop/c0-buildstation/%profilename%/%QSYS%/%QTVERSION%/Debug
md %build:/=\%
cd /d %build%
rem del /f /q demo examples src test

echo src file: %src:\=/%
echo src path: %src_path:\=/%
echo build at: %build:\=/%
echo sdk at  : %sdk:\=/%
echo prod at : %prod:\=/%
echo build inf %QTSPEC% %QTCONFIG%
call :start_time
qmake %src% %QTSPEC% CONFIG+=debug CONFIG+=qml_debug %QTCONFIG% && %make0% qmake_all
%make0%
call :stop_time
call :diff_time
echo build inf %QTSPEC% %QTCONFIG%
echo src file: %src:\=/%
echo src path: %src_path:\=/%
echo build at: %build:\=/%
echo sdk at  : %sdk:\=/%
echo prod at : %prod:\=/%
rem echo target  : %build%/debug/bin/%profilename%.exe
cd /d %src_path%
goto :eof

:start_time
set a=%time%
echo .
echo 开始时间：%a%
goto :eof

:stop_time
set b=%time%
echo .
echo 结束时间：%b%
echo .
goto :eof

:diff_time
set /a h1=%a:~0,2%
set /a m1=1%a:~3,2%-100
set /a s1=1%a:~6,2%-100
set /a h2=%b:~0,2%
set /a m2=1%b:~3,2%-100
set /a s2=1%b:~6,2%-100
if %h2% LSS %h1% set /a h2=%h2%+24
set /a ts1=%h1%*3600+%m1%*60+%s1%
set /a ts2=%h2%*3600+%m2%*60+%s2%
set /a ts=%ts2%-%ts1%
set /a h=%ts%/3600
set /a m=(%ts%-%h%*3600)/60
set /a s=%ts%%%60
echo 耗时%h%小时%m%分%s%秒
goto :eof



